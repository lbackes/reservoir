
FT_Simulator                                     [v1.0,  28 October   2007]
:::::   The <FT_Simulator> main program (calls all the high-level routines)

Core_Memory_Allocation                           [v1.0,   29 May      2008]
:::::   Allocate memory to most arrays based on size provided by input parameters

EOS_Defaults                                     [v1.0,   7 April     2006]
:::::   Default parameters of the equation of state for a pure gas system

EOS_Primary_Variable_List                        [v1.0,  01 June 2008]
:::::   Determines the list of primary variables for the GAS equation of state

Write_Headings                                   [v1.0,  23 April     2007]
:::::   Routine for printing the headings in the standard <FTSim> output file

Open_External_IO_Files                           [v1.0,  14 January   2006]
:::::   Open files <VERS>, <MESH>, <INCON>, <GENER>, <SAVE>, <LINEQ>, and <TABLE>

CPU_Timing_Routine                               [v1.0,  10 March     2006]
:::::   CPU time computation: Uses FORTRAN95 intrinsic timing functions

Allocate_Temp_Input_Memory                       [v1.0,   29 May      2008]
:::::   Allocate memory to to temporary input arrays

Read_Main_Input_File                             [v1.0,  29 September 2007]
:::::   Read all data provided in the main FTSim input file

Initializations_and_Defaults                     [v1.0,  30 September 2007]
:::::   Initialize important parameters and variables 

Read_Gas_Specific_Data                           [v1.0,   29 May      2008]
:::::   Read data specific to the Air-H2O system from the <Air+H2O> data block of the input data file

Read_Geologic_Media_Properties                   [v1.0,   1 June   2008]
:::::   Read the rock type data from the <ROCK> block of the FTSim input data file

Read_Time_Discretization_Data                    [v1.0,  01 June 2008]
:::::   Read the time discretization data from the TIME_DISCRETIZATION_DATA block of the FTSim input data file

Read_Computational_Parameters                    [v1.0,  21 September 2007]
:::::   Read the computational parameter data from the NRI block of the FTSim input data file

Read_Output_Options                              [v1.0,  21 September 2007]
:::::   Read the data from the OUTPUT_Options block of the FTSim input data file

Read_Computational_Parameters                    [v1.0,  21 September 2007]
:::::   Read the computational parameter data from the PARAM block of the FTSim input data file

Read_Initial_Conditions                          [v1.0,   04 June      2008]
:::::   Read data specific to the <INITIAL CONDITIONS> data block of the input data file

Read_Generation_Data                             [v1.0,  11 September 2007]
:::::   Read the source/sink data from the <GENER> block of the FTSim input data file

Significant_Digits                               [v1.0,  11 March     2006]
:::::   Calculate number of significant digits for floating point arithmetic

Read_External_Input_Files                        [v1.0,  12 January   2007]
:::::   Initialize data from files <MESH> or <MINC>, <GENER> and <INCON>
        Also initializes permeability modifiers and coordinate arrays and optionally reads tables with flowing wellbore pressures

Read_Elements_From_MESH                          [v1.0,  19 August    2007]
:::::   Read the element data from the <ELEME> block of the file <MESH>

Read_Connections_From_MESH                       [v1.0,  20 August    2007]
:::::   Read the connection data from the <CONNE> block of the file <MESH>

Read_Generic_File_GENER                          [v1.0,   8 March      2007]
:::::   Read the sink/source data from file GENER (generic)

Read_Initial_Conditions_File                     [v1.0,   1 October   2007]
:::::   Read the initial condition data directly from the file <INCON>

Solver_Memory_Allocation                         [v1.0,   29 May      2008]
:::::   Allocate memory to the Jacobian  and solution matrix arrays

Print_Input_Data                                 [v1.0,  22 November  2006]
:::::   Provide printout of most data provided through the <FTSim> main input file

Deallocate_Unneeded_Arrays                       [v1.0,   29 May      2008]
:::::   Deallocation of memory from all temporary and unnecessary arrays

Write_More_Generic_Info                          [v1.0,  23 April     2007]
:::::   Routine for printing additional information in the output file

Write_EOS_Specific_Info                          [v1.0,   29 May      2008]
:::::   Routine for printing additional EOS-specific information in the standard <FTSim> output file

Simulation_Cycle                                 [v1.0,  18 August    2007]
:::::   Executive routine that performs the simulation while marching in time

Equation_of_State                                [v1.0,   01 June      2008]
:::::   Selection of the appropriate state in the Gas system

EOS_Initial_Assignment                           [v1.0,   01 June      2008]
:::::   Initial assignment of thermophysical properties in a Gas system

Single_Phase_Gas                                 [v1.0,   01 June      2008]
:::::   Computes the secondary variables under 1-phase (gas) conditions in a 100% gas system

Compute_Gas_Density                              [v1.0,  20 June      2008]
:::::   Computation of the compressibility factor Z and density of a real gas

Compute_Gas_Viscosity                            [v1.0,  12 June 2008]
:::::   Calculate the gas viscosity (scalar version)

Hydraulic_Property_Changes                       [v1.0,   01 June      2008]
:::::     Transient porosity and permeability as a function of P, T, stress and strain

Mass_and_Energy_Balance                          [v1.0,   29 May      2008]
:::::   Perform summary balances for volume, mass, and energy

Adjust_Timestep_for_Printout                     [v1.0,   4 November  2007]
:::::   Adjust time steps to conform with user-defined times for a print-out

JACOBIAN_SetUp                                   [v1.0,   01 June      2008]
:::::   Assemble all accumulation and flow terms; Populate the Jacobian matrix - Gas system

Upon_Convergence                                 [v1.0,  29 September 2006]
:::::   Update primary variables and select timestep after convergence of the NI iterations (generic)

Write_File_SAVE                                  [v1.0,  14 January   2007]
:::::   At the completion of a FTSim run, write primary variables into file "SAVE"

Update_Simulation_Parameters                     [v1.0,  15 February  2007]
:::::   Update simulation contol parameters without interrupting execution

Print_Standard_Output                            [v1.0,   29 May      2008]
:::::   Print results for elements, connections, and sinks & sources in the standard <FTSim> output file
