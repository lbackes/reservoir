!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>          T_Main.f95: Code unit including the main program           >
!>                      and all related routines                       >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
      PROGRAM TOUGH_Plus
! 
! ...... Modules to be used 
! 
         USE Cycling_Process_Selector
! 
         USE Basic_Parameters
         USE General_External_File_Units
! 
         USE General_Control_Parameters
         USE MPICOM
         USE MADIM
!
!***********************************************************************
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      TOUGH+MP Main Program: Calls several lower-level routines      *     
!*                    which execute computations                       *     
!*                                                                     *
!*                   Version 1.1 - March 24, 2008                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
      INCLUDE "mpif.h"
! 
! -------
! ... Character allocatable arrays     
! -------
! 
      CHARACTER(LEN = 5), ALLOCATABLE, DIMENSION(:) :: VV
! 
! -------
! ... Double Precision Variables    
! -------
! 
      REAL(KIND = 8) :: ELT,ELT1,ELTC
! 
! -------
! ... Integer Variables    
! -------
! 
      INTEGER :: i,ierG
! 
! -------
! ... Logical Variables    
! -------
! 
      LOGICAL :: Flag_MINC,EX
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of TOUGH+
!
!
      call MPI_INIT(ierr)
      call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierr)
      iMaster=0

      if (myid .eq. 0) then
! read constants from file 'PARAL.prm'
       PartReady=0
       MNEL=500000
       MNCON=2300000
       INQUIRE(FILE='PARAL.prm',EXIST=EX)
       IF(EX) then
        open(56,file='PARAL.prm', status='old')
        read(56,*) MNEL,MNCON,PartReady, iMaster
        close(56)
        if ((iMaster>=nprocs) .or. (iMaster<0)) iMaster=0
       END IF 
      end if

       call MPI_BCAST(iMaster,1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr)

       N_Times=2                ! N_Times is a constant, can be adjusted based on needed.
       MAXPROCS=2048            ! Maximum number of processors can be used by this program, it can be increased
       MINPROCS=2

       if (myid ==iMaster) then
       finput = 'INFILE'
       foutput = 'OUTPUT'
       foutput_data='OUTPUT_DATA'
       OPEN(UNIT=35,FILE=finput,FORM='FORMATTED',STATUS='OLD')
       OPEN(UNIT=36,FILE=foutput,FORM='FORMATTED',STATUS='UNKNOWN')
       OPEN(UNIT=37,FILE=foutput_data,FORM='FORMATTED',STATUS='UNKNOWN')
! -------
! ... Open file VERS to record subroutine use
! -------
! 
      INQUIRE(FILE = 'VERS',EXIST = file_VERS_exists)
! 
      IF_VERS: IF(file_VERS_exists) THEN
                  OPEN(VERS_Unit, FILE = 'VERS', STATUS = 'OLD')
               ELSE
                  OPEN(VERS_Unit, FILE = 'VERS', STATUS = 'NEW')
                  ENDFILE (UNIT = VERS_Unit)
               END IF IF_VERS
! 
      REWIND (UNIT = VERS_Unit)
! 
      WRITE(UNIT = VERS_Unit, FMT = 6000)
! 
! -------
! ... Reading the heading/title of the input data file  
! -------
!
      READ (35, FMT = 6002) title
      WRITE(36, FMT = 6003) title
      end if                      !myid==iMaster
! 
! 
!***********************************************************************
!*                                                                     *
!*                Allocate memory for most arrays                      *
!*                                                                     *
!***********************************************************************
! 
! 
      CALL Core_Memory_Allocation_1

! 
       if (myid==iMaster) then  
! 
!***********************************************************************
!*                                                                     *
!*          Print headings and important general information           *
!*                                                                     *
!***********************************************************************
! 
!
       CALL Write_Headings
! 
! 
!***********************************************************************
!*                                                                     *
!*               Check the directory for various files                 *
!*                                                                     *
!***********************************************************************
! 
!
      CALL Open_External_IO_Files
! 
! 
!***********************************************************************
!*                                                                     *
!*                       Initialize the clock                          *
!*                                                                     *
!***********************************************************************
! 
! 
      CALL CPU_Timing_Routine(CPU_InitialTime)
! 
! 
!***********************************************************************
!*                                                                     *
!*             Allocate memory to temporary input arrays               *
!*                                                                     *
!***********************************************************************
! 
! 
      CALL Allocate_Temp_Input_Memory
! 
! 
!***********************************************************************
!*                                                                     *
!*             Read data from the main input data file                 *
!*                                                                     *
!***********************************************************************
! 
!
      CALL Read_Main_Input_File(Flag_MINC)

!
      IF(NoFlowSimulation .EQV. .TRUE.) THEN
         ELT1 = 0.0d0
         GO TO 800
      END IF
! 
! -------
! ... Determine the floating point accuracy of the processor 
! -------
! 
      CALL Significant_Digits
!                                                       
! 
!***********************************************************************
!*                                                                     *
!*      Read input data from the various files, both pre-existing      *
!*                  and created by the INPUT routine                   *
!*                                                                     *
!***********************************************************************
! 
      CALL Read_External_Input_Files(Flag_MINC)
! 
      END IF                     !myid==iMaster
!
  800   CALL MPI_BCAST(NoFlowSimulation,1,MPI_LOGICAL,iMaster,MPI_COMM_WORLD,ierr)
! 
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!*                                                                     *
!*                      CONDUCT THE SIMULATION                         *
!*                                                                     *
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! 
!
      IF_NoFloSimul:  IF(NoFlowSimulation .EQV. .FALSE.) THEN   ! Check if the simulation ...
!                                                               ! ... is to be bypassed
! ----------
! ... Print additional information
! ----------
!
      IF(myid==iMaster) then
        WRITE(36,6004) NumElemTot, NumElem, NumConx, NumSS
!
      IF(PermModType /= 'NONE') WRITE(36,6005)
! ----------
! ...... Select solver options 
! ----------
! 
         CALL Set_Solver_Options
! 
! 
!***********************************************************************
!*                                                                     *
!*             Allocate memory to matrix solver arrays                 *
!*                                                                     *
!***********************************************************************
! 
!
!kz         CALL Solver_Memory_Allocation    
! 
! ----------
!
! ----------
! ...... Print input data (when Flag_PrintInputs == .TRUE.)
! ----------
!
         IF(Flag_PrintInputs) CALL Print_Input_Data
! 
! ----------
! ...... Deallocate unnecessary memory (Temporary input arrays)  
! ----------
! 
         CALL Deallocate_Unneeded_Arrays       
! 
! ----------
! ...... Print additional information 
! ----------
!
         CALL Write_More_Generic_Info
! 
         CALL Write_EOS_Specific_Info
! 
! 
!***********************************************************************
!*                                                                     *
!*     Perform simulation by solving the mass and heat equations       *
!*                                                                     *
!***********************************************************************
! 
!
        END IF    !myid==iMaster
        CALL DO_parallel
!
! ----------
! ...... Deterine elapsed time for data input
! ----------
!
         CALL CPU_Timing_Routine(ELT1)
         ELT1 = ELT1 - CPU_InitialTime
!
          IF(myid==iMaster) WRITE(36,6006) ELT1

        CALL Simulation_Cycle
!
! <<<                      
! <<< End of the "IF_NoFloSimul" construct         
! <<<
!
      END IF IF_NoFloSimul
! 
! 
!***********************************************************************
!*                                                                     *
!*                   Print-out of version information                  *
!*                                                                     *
!***********************************************************************
! 
! 
 1000 IF_NoVersion: IF(NoVersion .EQV. .FALSE.) THEN  ! Check whether to bypass
! 
! ----------
! ...... Print information 
! ----------
!
       IF(myid==iMaster) THEN 
         WRITE(36, FMT = 6008)  
!
! ...... Close the VERS file
!
         ENDFILE (UNIT = VERS_Unit)
!
! ...... Go to the top of the VERS file
!
         REWIND (UNIT = VERS_Unit)
! 
! ----------
! ...... Allocate memory to the VV local array 
! ----------
! 
         ALLOCATE(VV(26), STAT=ierG)
! 
! ...... Check if properly allocated 
! 
         IF(ierG /= 0) THEN   ! Unsuccesful allocation
            WRITE(36, FMT = 6101)         
            CALL STOP_ALL
         END IF
! 
! ----------
! ...... Initialization of the VV local array 
! ----------
! 
         DO_Version: DO 
! 
! ......... Initialization of the VV local array 
! 
            VV = '     '     !  CAREFUL - Whole array operation
!
! ......... Read version information
!
            READ(UNIT = VERS_Unit, FMT = 6010, IOSTAT = ierG) (VV(i),i=1,26)
!
! ......... For EOF/EOR, exit the loop
!
            IF(ierG /= 0) EXIT DO_Version            
!
! ......... Write version information
!
            WRITE(36, FMT = 6010) (VV(i),I=1,26)
!
         END DO DO_Version 
! 
! ----------
! ...... Allocate memory from the VV local array 
! ----------
! 
         DEALLOCATE (VV, STAT=ierG)
! 
! ...... Check if properly deallocated 
! 
         IF(ierG /= 0) THEN   ! Unsuccesful deallocation
            WRITE(36, FMT = 6102)         
            CALL STOP_ALL
         END IF
!
       END IF                 !myid==iMaster
      END IF IF_NoVersion
! 
! 
!***********************************************************************
!*                                                                     *
!*               Compute execution timing information                  *
!*                                                                     *
!***********************************************************************
! 
! 
      IF(myid==iMaster) WRITE(36, FMT = 6015)
!
      CALL CPU_Timing_Routine(ELT)
!
      ELT  = ELT-CPU_InitialTime
      ELTC = ELT-ELT1
!
      IF(myid==iMaster) WRITE(36, FMT = 6020) ELT, ELT1, ELTC, CPU_MatrixSolTime   !  Print execution time data
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'TOUGH+',T50,'[v1.0,  18 April     2004]',/,   &
     &         ':::::   The TOUGH+ main program (calls all the high-level routines)') 
!
 6002 FORMAT(A120)
 6003 FORMAT(/1X,131('=')//5X,'PROBLEM TITLE:  ',A120//)


 6004 FORMAT(/' MESH HAS',I7,' ELEMENTS (',I7,' ACTIVE) AND',I7,' CONNECTIONS (INTERFACES)',   &
     &        ' BETWEEN THEM'/' GENER HAS',I6,' SINKS/SOURCES')
 6005 format(' BLOCK-BY-BLOCK PERMEABILITY MODIFICATION IS IN EFFECT')
!
 6006 FORMAT(//,' END OF TOUGH+ INPUT JOB: Elapsed Time = ',1pe15.8,' seconds'/)
!
 6008 FORMAT(/,   &
     &       132('*'),/,'*',130X,'*',/,                                   &
     &       '*',50X,'SUMMARY OF PROGRAM UNITS USED',51X,'*',/,           &
     &       '*',130X,'*',/,132('*'),/)
!
 6010 FORMAT(26A5)
 6015 FORMAT(//,132('*'))
 6020 FORMAT(//, ' END OF TOUGH+ SIMULATION RUN:',   &
     &       T35,' Elapsed Time             = ',1pe15.8,' sec',/,   &
     &       T35,' Data Input Time          = ',1pe15.8,' sec',/,   &
     &       T35,' Calculation Time         = ',1pe15.8,' sec',/,   &
     &       T40,' Matrix Solving Time = ',1pe15.8,' sec')
!
 6101 FORMAT(//,20('ERROR-'),//,   &
     &       T2,'Memory allocation of array VV in the main program TOUGH+ was unsuccessful',//,   &
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',   &
     &       //,20('ERROR-'))
 6102 FORMAT(//,20('ERROR-'),//,   &
     &       T2,'Memory deallocation of array VV in TOUGH+ was unsuccessful',//,   &
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',   &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of TOUGH+
!
!

       CALL MPI_FINALIZE(ierG)       
       STOP
      END PROGRAM TOUGH_Plus
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************

