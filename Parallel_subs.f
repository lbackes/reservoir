! Subroutines for main parallelization
!DO_parallelCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC!     
      Subroutine DO_parallel
      USE KindData
      USE MADIM
      USE MPICOM
      USE UPDATEINFO
      USE temp_arrays
      USE AZ_ME_para
      USE Solution_Matrix_Arrays
      USE Basic_Parameters
      USE General_External_File_Units
      USE General_Control_Parameters
      USE Connection_Attributes
      USE Grid_Geometry
      USE VBR_Matrix
      USE CONN_INDX
      USE Element_Attributes
      USE Sources_and_Sinks
      USE Time_Series_Parameters
      USE Subdomain_Definitions
!
      IMPLICIT NONE
      include 'mpif.h'
      include 'az_aztecf.h'
     
      integer iI,iJ,i,j,itmp,NULL
      parameter(NULL = 0)
      REAL(KIND = 8) :: ELT
           
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!  Declarations for use of AZTEC and METIS

      integer DEBUG
      integer nblkrow, nblkcol, nblk
      integer stat(MPI_STATUS_SIZE)
!  Declarations required for using METIS for graph partitioning

      integer  N_update, N_external

!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

!    Declarations of allocatable vectors and related scalars
!     ADJ and XADJ for holding adjecency matrix
      integer(iK4), dimension(:), allocatable:: ADJ, XADJ

      integer(iK4), dimension(:), allocatable:: part
      integer(iK4),allocatable,dimension(:)::lxlen
!     Vectors for holding matrix structure for global matrix
      integer lgrpntr, lgcpntr, lgbpntr, lgbindx, lgindx
      integer(iK4), dimension(:), allocatable:: grpntr, gcpntr, gbpntr
      integer(iK4), dimension(:), allocatable:: gbindx, gindx

      integer lrpntr, lcpntr, lbpntr, lbindx, lindx
!     Vectors for connections
      integer(iK8), dimension(:), allocatable:: gnex1, gnex2
      integer(iK8), dimension(:), allocatable:: gnexind

!     work arrays
      real(iKindN), dimension(:), allocatable:: rwork
      integer(iK8), dimension(:), allocatable:: iwork
      character*8, dimension(:), allocatable:: cwork

     
!     End of declarations for allocatable vectors
      LOGICAL EX

! end of dada declaration      
!   ******************************************************************

      DEBUG=0
      Flag_SemianalyticHeatExchange = .FALSE.      !mop(15)>0.0 is not ready for the parallel version.
      IF(myid==iMaster) WRITE(VERS_Unit,6000)
      
      call BCAST_CONST
!     get constants for defining the array sizes for all PEs
      if (nprocs<2) then
       if (myid==iMaster) then
        write(36,*) 'nprocs=',nprocs
        write(36,*) 'TOUGH+MP REQUIRES RUNNING ON AT LEAST 2 PROCESSES.'
        write(*,*) 'nprocs=',nprocs
        write(*,*) 'TOUGH+MP REQUIRES RUNNING ON AT LEAST 2 PROCESSES.'
       end if
      CALL STOP_ALL
      end if
!
      iSlave=0
      if (iMaster==0) iSlave=1
      if (myid==iSlave) then
       ALLOCATE(ELEM0(MNEL))
       ALLOCATE(MATX0(MNEL))
       ALLOCATE(por0(MNEL))
       ALLOCATE(indx0(MNEL))
       ALLOCATE(X(MNEL*NumComPlus1))
       ALLOCATE(perm0(3,MNEL))
!
       call MPI_RECV(ELEM0,MNEL*8,MPI_CHARACTER,iMaster,11, &
     & MPI_COMM_WORLD,stat,ierr)
       call MPI_RECV(MATX0,MNEL,MPI_INTEGER,iMaster,12, &
     & MPI_COMM_WORLD,stat,ierr)
       call MPI_RECV(por0,MNEL,MPI_DOUBLE_PRECISION,iMaster,13, &
     & MPI_COMM_WORLD,stat,ierr)
       call MPI_RECV(indx0,MNEL,MPI_INTEGER,iMaster,14, &
     & MPI_COMM_WORLD,stat,ierr)
       call MPI_RECV(X,MNEL*NumComPlus1,MPI_DOUBLE_PRECISION,iMaster,15, &
     & MPI_COMM_WORLD,stat,ierr)
       call MPI_RECV(perm0,MNEL*3,MPI_DOUBLE_PRECISION,iMaster,16, &
     & MPI_COMM_WORLD,stat,ierr)
      end if
!
      if (myid==iMaster) then
       DEALLOCATE(xx0,yy0,zz0)
       call MPI_SEND(ELEM0,MNEL*8,MPI_CHARACTER,iSlave,11, &
     & MPI_COMM_WORLD,ierr)
       call MPI_SEND(MATX0,MNEL,MPI_INTEGER,iSlave,12, &
     & MPI_COMM_WORLD,ierr)
       call MPI_SEND(por0,MNEL,MPI_DOUBLE_PRECISION,iSlave,13, &
     & MPI_COMM_WORLD,ierr)
       call MPI_SEND(indx0,MNEL,MPI_INTEGER,iSlave,14, &
     & MPI_COMM_WORLD,ierr)
       call MPI_SEND(X,MNEL*NumComPlus1,MPI_DOUBLE_PRECISION,iSlave,15, &
     & MPI_COMM_WORLD,ierr)

       call MPI_SEND(perm0,MNEL*3,MPI_DOUBLE_PRECISION,iSlave,16, &
     & MPI_COMM_WORLD,ierr)
!
       DEALLOCATE(por0,X,ELEM0,MATX0,indx0,perm0)
       deallocate(ELEMINDEX)
      end if

! make an estimation for LNEL and LNCON
      LNEL=MNEL/nprocs*N_Times 
      LNCON=MNCON/nprocs*N_Times    
      N_unknowns=LNEL*NumEqu
      MAXXPART = N_unknowns
      N_needed = N_unknowns/2+5          ! AZTEC specific
      N_send_to_others = N_needed        ! AZTEC specific
      
      ALLOCATE(lxlen(MAXPROCS))
      ALLOCATE(update(N_unknowns))
      ALLOCATE(req(2*MAXPROCS))
      ALLOCATE(stat_no_B(MPI_STATUS_SIZE,2*MAXPROCS))
      ALLOCATE(proc_config(AZ_PROC_SIZE))

!     Read parameters for Aztec solver and METIS choice etc
      call rsolveinfo
      if (EE_output==666888) DEBUG=1
!
!     allocate small arrays that need to be distributed.
      if (myid/=iMaster) then
!      call AllocGlobalArray1
      ALLOCATE(NEX1(MNCON))
      ALLOCATE(NEX2(MNCON))
      end if
!     Ditribute the full nex1 and nex2 and all smaller matrices and
!     scalars that are to be fully replicated on all processors.
      call allreplicom

      IF(Flag_MonitorSubdomains .EQV. .TRUE.) THEN
       ALLOCATE(SubDomain_Data(NumSubdomains,11,1000))
      END IF
!
      if (myid==iSlave) then
      allocate(ADJ(MNCON*2))
      allocate(XADJ(MNEL+1))
      call MPI_RECV(ADJ,NumConx*2,MPI_INTEGER,iMaster,16,MPI_COMM_WORLD,stat,ierr)
      call MPI_RECV(XADJ,NumElem+1,MPI_INTEGER,iMaster,17,MPI_COMM_WORLD,stat,ierr)
      end if 
      if (myid .EQ. iMaster) then
!     Flush the OUTPUT file after all intial I/O
         call flush(36)

!  Start by initializing ADJ and XAJD, which are two vectors used
!  to Store all information about connections in the graph.
!  These arrays are used by METIS to partition the graph.

         allocate(adj(MNCON*2))
         allocate(xadj(MNEL+1))
         adj=0
         xadj=0

         call nex2adj(ADJ,xadj)
         call MPI_SEND(ADJ,NumConx*2,MPI_INTEGER,iSlave,16, MPI_COMM_WORLD,ierr)
! following lines transform ADJ back to adjecency matrix format
         do i=1,NumConx*2
          if (ADJ(i)<0) then
            ADJ(i)=NEX2(-ADJ(i))
          else
            ADJ(i)=NEX1(ADJ(i))
          end if 
         end do 

!     Partition the graph through calling an appropriate 
!     METIS routine.

         allocate(part(MNEL))

      INQUIRE(FILE='part.dat',EXIST=EX)
      IF(EX .EQV. .FALSE.) THEN
       if (PartReady==1) then
        write(*,*) 'File part.dat does not exist,use automatic domain partitioning.'
        write(36,*) 'File part.dat does not exist,use automatic domain partitioning.'
        PartReady=0
       end if
      END IF 

        if (PartReady==0) then
         nparts = nprocs
         if (EE_partitioner .EQ. 'METIS_Kway') then
            call METIS_PartGraphKway(NumElem, XADJ, ADJ, &
     &           NULL, NULL, 0, 1, nparts, 0, edgecut, part)
         elseif (EE_partitioner .EQ. 'METIS_VKway') then
            call METIS_PartGraphVKway(NumElem, XADJ, ADJ, &
     &        NULL, NULL, 0, 1, nparts, 0, edgecut, part)
         elseif (EE_partitioner .EQ. 'METIS_Recursive') then
            call METIS_PartGraphRecursive(NumElem, XADJ, ADJ, &
     &        NULL, NULL, 0, 1, nparts, 0, edgecut, part)
         end if
       else 
        open (unit=75,file='part.dat',form='formatted',status='old')
        read(75,133) nparts,edgecut,i
        if (nparts .ne. nprocs) then
         write(*,*) 'nparts does  not equal to CPU number used, ', &
     & 'repartition the domain to nprocs(',nprocs,') parts or ', &
     & 'run the program use ',nparts,' Processors'
         write(36,*) 'nparts does  not equal to CPU number used, ', &
     & 'repartition the domain to nprocs(',nprocs,') parts or ', &
     & 'run the program use ',nparts,' Processors'

        call STOP_ALL
        end if
        if (i .ne. NumElem) then
       write(*,*) 'The girdblock number in the partitioned domain does',&
     & ' not match the gridblock number in MESH file'
       write(36,*) 'the girdblock number in the partitioned domain does',&
     & ' not match the gridblock number in MESH file'
         call STOP_ALL
        end if
        read(75,144) (part(iI),iI=1,NumElem)
        close(75)
       end if    !finish partitition of the domain

 133   format(3I10)
 144   format(10I8)

      if (DEBUG .GT. 0) then
         write(*,*)'pass partition'
       end if
!     Adjust part for numbering processors from 0

            part = part - 1

         if (DEBUG .GT. 0) then
            write(*,*)'Edgecut = ',edgecut
         end if
      
!ee Initialize the VBR data structure with adaquate pointers for
!ee the current Jacobian.

!     Allocate vectors to hold the structure of the global matrix
  
         lgrpntr = NumElem+1
         lgcpntr = lgrpntr
         lgbpntr = lgrpntr
         lgbindx = NumElem + 2*NumConx
         lgindx = lgbindx+1
 
         allocate(grpntr(lgrpntr))
         allocate(gcpntr(lgcpntr))
         allocate(gbpntr(lgbpntr))
         allocate(gbindx(lgbindx))
!         allocate(gindx(lgindx))
         call initjac(ADJ, XADJ, NumElem, NumElem, NumEqu, grpntr, gcpntr, gbpntr, gbindx)
 
        call MPI_SEND(XADJ,NumElem+1,MPI_INTEGER,iSlave,17,MPI_COMM_WORLD,ierr)
        
        deallocate(ADJ,XADJ)
      end if 
! End of Section, only PE0 goes through this section.
 
!     Distribute local parts of the VBR matrix structures, 
!     and appropriate "update" parts of common blocks from processor 0
!     to all other processors.
      ierr = 0

!     Allocate vectors to hold the structure for the local part of 
!     the matrix.
      lrpntr = LNEL+1
      lcpntr = NumElem+1
      lbpntr = lrpntr
      lbindx = LNEL + 2*LNCON
      lindx = lbindx+1

 
      allocate(rpntr(lrpntr))
      allocate(cpntr(lcpntr))
      allocate(bpntr(lbpntr))
      allocate(bindx(lbindx))
      allocate(indx(lindx))
      i=0
      if (myid==iMaster) then
      allocate(IUpdateN(0:nprocs-1))
      allocate(iUpdateIdx(0:nprocs-1,0:N_unknowns-1))
      end if
!
      ierr=iMaster
      call distidx(myid, part, NumElem, nblkrow, nblkcol,nblk, nprocs, lxlen, NumComPlus1, & 
     &  update, N_unknowns, rpntr, lrpntr, cpntr, lcpntr, bpntr, lbpntr, &
     &  bindx, lbindx, indx, lindx, grpntr, lgrpntr,gcpntr, lgcpntr,  gbpntr, lgbpntr, &
     &  gbindx, lgbindx, lgindx, ierr)

!
!     If the problem setup is not OK, then
!     tell other processors to exit
      if (ierr .ne. 0) then
         write(36,*)'*** EXECUTION STOPPED ***'
         write(36,*)'Try using more or fewer processor '
         write(36,*)'or to increase N_times'
         call STOP_ALL
      end if
!
!     Deallocate vectors for holding structure of global matrix.
      if (myid==iMaster) then
         if (DEBUG .GT. 0) then
          write(*,*) 'pass distdx'
         end if
         deallocate(grpntr)
         deallocate(gcpntr)
         deallocate(gbpntr)
         deallocate(gbindx)
         deallocate(part)
      end if

!      Allocate the vector aval for holding the matrix elements.
!      bpntr(nblkrow+1) is the last element in bpntr.
!      indx(bpntr(nblkrow+1)+1) is the last element in indx.
!     Transform the local VBR-structure (with global indices) into
!     the order Aztec requires with local indices.
!     This will give a new order of all local elements. This new
!     order is specified by the arrays update and update_index.
      call AZ_set_proc_config(proc_config,MPI_COMM_WORLD)
      N_update = nblkrow
      laval =  indx(bpntr(nblkrow+1)+1)+1
      allocate(aval(laval))
      ALLOCATE(update_index(N_unknowns))
      ALLOCATE(external(N_needed))
      ALLOCATE(extern_index(N_needed))

      ALLOCATE(data_org(AZ_COMM_SIZE+AZ_send_list+N_send_to_others))

      call AZ_transform(proc_config, external, bindx, &
     &  aval, update, update_index, extern_index, data_org, &
     &  N_update, indx, bpntr, rpntr, cpntr, AZ_VBR_MATRIX)

      if (DEBUG .GT. 0) then
       if (myid==0) write(*,*) 'pass AZtransform'
      end if
!     Deallocate aval in order to save some memory during the rest of
!     the initialization procedure. aval is reallocated later.

      deallocate(aval)
      N_external = data_org(AZ_N_external+1)/NumEqu
      
      itmp=N_update+N_external
      LNEL=itmp
      call MPI_Allreduce(itmp,LMNEL,1,MPI_INTEGER,MPI_MAX, &
     &MPI_COMM_WORLD,ierr)
    
!     Construct local versions of nex1 and nex2 on each processor,
!     so that the local versions only include connections where elements
!     in the local processors update list are included (sometimes with
!     a neighboring element in the "external" list).
!     First the global versions are stored in gnex1 and gnex2.
!     Then nex1 and nex2 are assigned the local indices.

      LMNCON=MNCON/nprocs*N_Times*2   !estimates the max number of connections at PEs

       if(myid .ne. iSlave) then
        allocate(adj(MNCON*2))
        allocate(xadj(MNEL+1))
       end if
       call MPI_BCAST(adj, NumConx*2, MPI_INTEGER, iSlave,MPI_COMM_WORLD, ierr)
       call MPI_BCAST(xadj,NumElem+1, MPI_INTEGER, iSlave,MPI_COMM_WORLD, ierr)
   
      allocate(gnex1(LMNCON))
      allocate(gnex2(LMNCON))
      allocate(gnexind(LMNCON))
      call getlnex(adj,xadj,gnex1,gnex2, update(1:N_update), &
     &      update_index, N_update, external, extern_index, &
     &      N_external, NumConx, lncon, gnexind)
!
      if (DEBUG .GT. 0) then
       if (myid==iMaster) write(*,*) 'pass getlnex '
      end if
      deallocate(nex1,nex2,adj,xadj)

      itmp = lncon
      call MPI_Allreduce(itmp, LMNCON, 1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierr)
      if (DEBUG .GT. 0) then
       if (myid .eq.iMaster) then
         write(*,*)'Maximum number of connections at any proc:', LMNCON
       end if
      end if

      nbmxc=LNEL*NumEqu
      if(myid==iMaster) then
       i=LNCON 
       j=LNEL
       LNCON=LMNCON
       LNEL=LMNEL
      end if
      allocate(conx(LNCON),STAT = iI)
      conx(1:LNCON)%n1=gnex1(1:LNCON)
      conx(1:LNCON)%n2=gnex2(1:LNCON)
      deallocate(gnex1,gnex2)
      allocate(nexind(lncon),STAT = iI)

!     The following fix (to renumber the indices in nexind from 0)
!     is only to be able to reuse the distribution software used
!     for vectors relating to the update vector. If it will be kept
!     this way, this fix should be incorporated into the construction
!     of nexind in the routine getlnex.

      nexind(1:lncon) = gnexind(1:lncon) - 1
!     End of nexind fix
      deallocate(gnexind)
      allocate(elem(LNEL),STAT = iI)
      allocate(AHT(LNEL),STAT = iI)
      allocate(emissivity(lncon),STAT = iI)
      allocate(ElemState(LNEL),ElemMedia(LNEL, -2:NumEqu))
      if(myid==iMaster) then
       LNCON=i
       LNEL=j
      end if
!     all vectors in modules that has to do with
!     connections and elements. They are sent with elements in appropriate order.

      call DistrData(nexind)

      if (DEBUG .GT. 0) then
      if (myid==0) write(*,*) 'pass DistrDat'
      end if
      CALL Core_Memory_Allocation_2 

!
      call MPI_BCAST(PermModType, 4, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      ALLOCATE(b(nbmxc))
      ALLOCATE(xr(nbmxc))
!
      allocate(rwork(N_update*NumComPlus1))
      allocate(iwork(N_update))
      allocate(cwork(N_update))

!     Transform vectors (relating to "update" of elements) in 
!     common blocks that has to do with grid elements, so that 
!     their order correspond to the reorder made by AZ_transform.
!
      IGOOD_Needed=0
      DO iI=1,NumSS
        IF(SS(iI)%Table_Length>1) IGOOD_Needed=1 
        IF(SS(iI)%TypeIndx==(NumComPlus1+2)) IGOOD_Needed=1 
      END DO
      call transcom(update_index, N_update, rwork, iwork, cwork)

      call locnogn(update, update_index, N_update, rwork,iwork, cwork)
      deallocate(rwork)
      deallocate(iwork)
      deallocate(cwork)

!     Determine the actual number of border elements to send
!     to each processor, and the total number of elements 
!     to send.

      totbrdlen = 0
      do i = 1, data_org(AZ_N_neigh+1)
         totbrdlen =  totbrdlen + data_org(AZ_send_length+(i-1)+1)
      end do
      totbrdlen = totbrdlen / NumEqu

      allocate(rwork(totbrdlen*(NumEqu+1)))
      allocate(iwork(totbrdlen))
      allocate(cwork(totbrdlen))

      Do i=1,N_update
        IF((elem(i)%activity=='I') .OR. (elem(i)%activity=='V')) THEN 
         elem(i)%vol=1.00d+50
        END IF
      END DO
!     Exchange the "external" parts of all vectors in common blocks
!     that has to do with grid elements.

      call exchallext(myid, data_org, update, update_index,rwork, iwork, cwork)            

      deallocate(rwork)
      deallocate(iwork)
      deallocate(cwork)
!     Allocate aval and working space to be used throughout the 
!     whole computation.
      allocate(aval(laval))
      call MPI_Allreduce(itmp, LMNCON, 1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierr)
      itmp=N_update
      call MPI_Allreduce(itmp, MaxNEL, 1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierr)
      call MPI_Allreduce(itmp, MinNEL, 1, MPI_INTEGER, MPI_MIN, MPI_COMM_WORLD, ierr)
       NumElem = N_update
       NumElemTot=LNEL 
       NumConx = lncon
       NumUnknowns=LNEL*NumEqu
       iDoSave=0
       iDoConxT=0
       iDoSST=0
       iDoSubDomain=0
       Conx_Data=0.0
       SS_DATA=0.0 
       G_ObsElem_Flag=ObsElem_Flag
       G_ObsConx_Flag=ObsConx_Flag
       G_ObsSS_Flag=ObsSS_Flag
       call RFILEWRK
       if (myid==iMaster) then 
        CALL CPU_Timing_Routine(ELT)
        write(*,*) 'Time for preprocessing ---- including data input, &
     &  partition, distribution:',ELT-CPU_InitialTime
       end if
 6000 FORMAT('Main Subroutine for Parallelization, [v1.0,  04 April     2007]')

      RETURN
      END Subroutine DO_parallel

!Core_Memory_Allocation_1CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE Core_Memory_Allocation_1
! 
         USE EOS_Parameters
         USE EOS_Default_Parameters
! 
         USE Basic_Parameters
         USE General_External_File_Units
! 
         USE General_Control_Parameters
         USE Diffusion_Parameters
! 
         USE Grid_Geometry
         USE Element_Attributes
         USE Connection_Attributes
! 
         USE Solution_Matrix_Arrays
         USE Sources_and_Sinks
         USE Geologic_Media_Properties

         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      ROUTINE FOR READING IN MEMORY SIZE RELATED PARAMETER           *
!*                                                                     *
!***********************************************************************
!***********************************************************************
! 
         IMPLICIT NONE
         INCLUDE 'mpif.h'
! 
! ----------
! ...... Integer Arrays    
! ----------
! 
         INTEGER, DIMENSION(0:30) :: ierror = 99999
! 
! ----------
! ...... Integer variables     
! ----------
! 
         INTEGER :: ii,i,j
         INTEGER :: number_of_components, number_of_equations, characters_in_element_name
         INTEGER :: Max_number_of_elements, Max_number_of_connections, Max_number_of_sources_and_sinks, Max_number_of_geologic_media
! 
! ----------
! ...... Character variables     
! ----------
! 
         CHARACTER(LEN = 15) :: B_name = '               '
         CHARACTER(LEN = 26) :: HEADR
         CHARACTER(LEN =  9) :: Memory_header
         CHARACTER(LEN =  1) :: Option_property_update
! 
! ----------
! ...... Logical variables     
! ----------
! 
         LOGICAL exists, NewInputFormat
!
         LOGICAL Flag_binary_diffusion, Flag_coupled_geochemistry, Flag_coupled_geomechanics, Flag_active_connections

! 
! ----------
! ...... Namelists
! ----------
!
         NAMELIST/Basic_Parameter_Definitions/ EOS_Name,                      &  ! The name of the EOS segment compiled with TOUGH+
     &                                         number_of_components,          &  ! Number of mass components
     &                                         number_of_equations,           &  ! Number of equations per gridblock
     &                                         number_geomech_parameters,     &  ! Number of geomechanical parameters to store
     &                                         Flag_binary_diffusion,         &  ! Logical variable determining whether diffusion is to be considered
     &                                         element_by_element_properties, &  ! Flag indicating if flow properties are to be read element by element
     &                                         porosity_perm_dependence,      &  ! Flag indicating if permeability is to change as a function of porosity
     &                                         scaled_capillary_pressure,     &  ! Flag indicating if the capillary pressure is to be scaled
     &                                         Flag_coupled_geochemistry,     &  ! Flag indicating coupled geochemistry is considered
     &                                         Flag_coupled_geomechanics,     &  ! Flag indicating coupled geomechanics is considered
     &                                         Option_property_update            ! Option determining the type of property updating
!
         NAMELIST/System_Specifications/ coordinate_system,                   &  ! Coordinate system
     &                                   Max_number_of_elements,              &  ! Maximum number of elements (cells)
     &                                   Max_number_of_connections,           &  ! Maximum number of connections (cells)
     &                                   Max_number_of_sources_and_sinks,     &  ! Maximum number of sources and sinks
     &                                   Max_number_of_geologic_media,        &  ! Maximum number of geological media
     &                                   characters_in_element_name,          &  ! Number of characters in the element names
     &                                   Flag_active_connections                 ! Flag indicating whether the run is intended to be limited to determining the active connections
 
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Core_Memory_Allocation
!
!
         if (myid==iMaster) then

         WRITE(VERS_Unit,6000)
! 
! 
!***********************************************************************
!*                                                                     *
!*              OPEN FILE TO DOCUMENT MEMORY ALLOCATION                *
!*                                                                     *
!***********************************************************************
! 
! 
         INQUIRE(FILE='ALLOC',EXIST=exists)
         IF(exists) THEN
            OPEN(UNIT = MemoryAlloc_Unit, FILE = 'ALLOC', STATUS = 'OLD')
            REWIND (UNIT = MemoryAlloc_Unit)
         ELSE      
            OPEN(UNIT = MemoryAlloc_Unit, FILE = 'ALLOC', STATUS = 'NEW')
         END IF     
! 
! 
!***********************************************************************
!*                                                                     *
!*              READ NUMBERS OF ELEMENTS AND CONNECTIONS               *
!*                                                                     *
!***********************************************************************
! 
! 
         READ(35,*) HEADR  
         B_name        = TRIM(ADJUSTL(HEADR))
         Memory_header = B_name(1:9)
!
         IF( Memory_header /= '>>>memory'   .AND. Memory_header /= '>>>Memory'   .AND. Memory_header /= '>>>MEMORY' .AND.   &
    &        Memory_header(1:6) /= 'memory' .AND. Memory_header(1:6) /= 'Memory' .AND. Memory_header(1:6) /= 'MEMORY'   )   &
    &    THEN
                     WRITE(*, 6002)
                     WRITE(36, 6002)
                     WRITE(MemoryAlloc_Unit,6002)
                     CALL STOP_ALL
         END IF
!
! ...... Determine the input format style
!
         IF( Memory_header == '>>>memory' .OR. Memory_header == '>>>Memory' .OR. Memory_header == '>>>MEMORY' ) THEN
            NewInputFormat = .TRUE.
         ELSE
            NewInputFormat = .FALSE.
         END IF
!
!
!***********************************************************************
!*                                                                     *
!*  READ THE NUMBER OF COMPONENTS, EQUATIONS, PHASES, SEC. VARIABLES   *
!*                                                                     *
!***********************************************************************
!
!
         IF_InputFormat1: IF( NewInputFormat .EQV. .FALSE. ) THEN
!
! ......... Reading data using the old input format
!
            READ(35,*) B_name
            EOS_Name = TRIM(ADJUSTL(B_name))
!
            READ(35,*) NumCom, NumEqu ,NumPhases, Flag_BinaryDiffusion
!
         ELSE
!
! ......... Reading data using the new input format (NAMELIST-based)
!
            Flag_coupled_geochemistry = .FALSE. !... Some initializations of NAMELIST variables
            Flag_coupled_geomechanics = .FALSE.
!
            Option_property_update = 'Continuous'
!
            number_geomech_parameters = 0
!
            READ(35, NML = Basic_Parameter_Definitions, IOSTAT = ierror(0))
!
            NumCom = number_of_components
            NumEqu = number_of_equations
!
            Option_PropertyUpdate = Option_property_update
!
            Flag_BinaryDiffusion     = Flag_binary_diffusion
            Flag_CoupledGeochemistry = Flag_coupled_geochemistry
            Flag_CoupledGeomechanics = Flag_coupled_geomechanics
!
! ......... Stop if there is a problem reading the fundamental parameters
!
            IF (ierror(0) /= 0) THEN
               WRITE (*, FMT = 6100)
               WRITE (36, FMT = 6100)
               CALL STOP_ALL
            END IF
!
         END IF IF_InputFormat1
!
!
       END IF       !myid=iMaster
! 
! 
!***********************************************************************
!*                                                                     *
!*   DETERMINE DEFAULT PARAMETERS FOR THE VARIOUS EQUATIONS OF STATE   *
!*                                                                     *
!***********************************************************************
! 
! 
        call MPI_BCAST(NumCom,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(NumEqu,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(NumPhases,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(EOS_Name, 15, MPI_CHARACTER,iMaster,MPI_COMM_WORLD, ierr)
        call MPI_BCAST(Flag_BinaryDiffusion,1,MPI_LOGICAL,iMaster,MPI_COMM_WORLD, ierr)

         CALL EOS_Defaults(ADJUSTL(EOS_Name), NumCom, NumEqu, Flag_BinaryDiffusion,                                    &
     &                                        Max_NumMassComp, Min_NumMassComp, Max_NumEquations, Min_NumEquations,    &
     &                                        Max_NumPhases, Max_NumMobPhases, NumStateParam, NumAuxParam,             &
     &                                        VERS_Unit, MemoryAlloc_Unit )

!
! ...... Determine the number of mobile phases 
!
         NumPhases    = Max_NumPhases
         NumMobPhases = Max_NumMobPhases
!
! ...... Define some additional parameters 
!
         NumEquPlus1  = NumEqu + 1
         NumEquSquare = NumEqu*NumEqu
         NumPerturb   = 2*NumEqu + 1
         NumComPlus1  = NumCom + 1
!
! ...... Define whether the simulation is conducted isothermally 
!
         IF(NumEqu == NumCom) THEN
            nonisothermal_conditions = .FALSE.
         ELSE
            nonisothermal_conditions = .TRUE.
         END IF
!
! ----------
! ...... Determine the primary variable list (to be included in the INCON and SAVE files) 
! ----------
!
         CALL EOS_Primary_Variable_List( ADJUSTL(EOS_Name), NumCom, NumEqu, Number_of_states, &
     &                                   EOS_variables, VERS_Unit, MemoryAlloc_Unit )
!
! ----------
! ...... Number of secondary parameters (to be stored in the DP array)
! ----------
!
         IF(Flag_BinaryDiffusion) THEN
            NumSecondaryVar = 10*Max_NumPhases + 1 + NumAuxParam
         ELSE
            NumSecondaryVar = 8*Max_NumPhases + 1 + NumAuxParam
         END IF
!
!
!***********************************************************************
!*                                                                     *
!*       SYSTEM DESCRIPTION: GRID, SOURCES & SINKS, MEDIA, ETC.        *
!*                                                                     *
!***********************************************************************
!
!
!
         if (myid==iMaster) then
         IF_InputFormat2: IF( NewInputFormat .EQV. .FALSE. ) THEN
!
! -------------
! ......... Old Input Style: Read the number of elements, connections and number of characters in the element name
! -------------
!
            READ(35,*) coordinate_system, Max_NumElem, Max_NumConx, ElemNameLength, Flag_ActiveConnectionsOnly
            READ(35,*) Max_NumSS
            READ(35,*) Max_NumMedia
!
            number_geomech_parameters = 0
!
            READ(35,*,err=10) element_by_element_properties, porosity_perm_dependence, scaled_capillary_pressure
            READ(35,*) Flag_CoupledGeochemistry
   10       BACKSPACE 35
            READ(35,*) Flag_CoupledGeochemistry, Option_PropertyUpdate
            READ(35,*) Flag_CoupledGeomechanics, Option_PropertyUpdate, number_geomech_parameters
!
         ELSE
!
! -------------
! ......... New Input Style: Read the number of elements, connections and number of characters in the element name
! -------------
!
            characters_in_element_name = 5    ! Initializations of some of the NAMELIST variables
!
            Flag_active_connections       = .FALSE.
            element_by_element_properties = .FALSE.
            porosity_perm_dependence      = .FALSE.
            scaled_capillary_pressure     = .FALSE.
!
            READ(35, NML = System_Specifications, IOSTAT = ierror(0))
!
! ......... Stop if there is a problem reading the fundamental parameters
!
            IF (ierror(0) /= 0) THEN
               WRITE (36, FMT = 6105)
               CALL STOP_ALL
            END IF
!
            Max_NumElem  = Max_number_of_elements
            Max_NumConx  = Max_number_of_connections
            Max_NumSS    = Max_number_of_sources_and_sinks
            Max_NumMedia = Max_number_of_geologic_media
!
            ElemNameLength = characters_in_element_name
!
            Flag_ActiveConnectionsOnly = Flag_active_connections
!
         END IF IF_InputFormat2
         END IF      !iMaster=myid

        call MPI_BCAST(Max_NumElem,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(Max_NumConx,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(ElemNameLength,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(coordinate_system, 3, MPI_CHARACTER,iMaster,MPI_COMM_WORLD, ierr)
        call MPI_BCAST(Flag_ActiveConnectionsOnly,1,MPI_LOGICAL,iMaster,MPI_COMM_WORLD, ierr)
        call MPI_BCAST(Max_NumSS,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(Max_NumMedia,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(number_geomech_parameters,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
        call MPI_BCAST(Option_PropertyUpdate, 1, MPI_CHARACTER,iMaster,MPI_COMM_WORLD, ierr)
        call MPI_BCAST(Flag_CoupledGeochemistry,1,MPI_LOGICAL,iMaster,MPI_COMM_WORLD, ierr)
        call MPI_BCAST(Flag_CoupledGeomechanics,1,MPI_LOGICAL,iMaster,MPI_COMM_WORLD, ierr)
        call MPI_BCAST(element_by_element_properties,1,MPI_LOGICAL,iMaster,MPI_COMM_WORLD, ierr)
        call MPI_BCAST(porosity_perm_dependence,1,MPI_LOGICAL,iMaster,MPI_COMM_WORLD, ierr)
        call MPI_BCAST(scaled_capillary_pressure,1,MPI_LOGICAL,iMaster,MPI_COMM_WORLD, ierr)
!
! ...... Ensure that the selected type of coordinate system is a valid option
!
         SELECT CASE(coordinate_system(1:3))
         CASE('Car', 'car', 'CAR')
            coordinate_system(1:3) = 'Car'
         CASE( 'Cyl', 'cyl', 'CYL' )
            coordinate_system(1:3) = 'Cyl'
         CASE DEFAULT
            WRITE(36,6010) coordinate_system
         END SELECT
!
! ...... Add a couple of extra elements and connections (to avoid array boundary errors)
!
         Max_NumElem = Max_NumElem + 2
         Max_NumConx = Max_NumConx + 2
!
! ......         
! ...... NumAboveMain: The total number of connections of elements with other elements of larger element numbers 
! ......         
! ...... NumBelowMain: The total number of connections of elements with other elements of smaller element numbers
! ......          
!
         NumAboveMain = 11*Max_NumConx/10
         NumBelowMain = NumAboveMain
!
!***********************************************************************
!*                                                                     *
!*          MEMORY ALLOCATION TO SOURCE/SINK-RELATED ARRAYS            *
!*                                                                     *
!***********************************************************************
!
!
! ...... Add a couple of extra sources/sinks (to avoid array boundary errors)
!
         Max_NumSS = Max_NumSS + 2
! 
! ----------
! ...... Allocate memory to derived-type     
! ----------
! 
         ALLOCATE(SS(Max_NumSS), STAT = ierror(20))
! 
         IF(Max_NumPhases > 1) THEN
            DO_SS : DO i = 1,Max_NumSS
               ALLOCATE(SS(i)%PhaseFracFlow(Max_NumMobPhases), STAT = ierror(21))
               ALLOCATE(SS(i)%PhaseVolRate(Max_NumMobPhases),  STAT = ierror(22))
            END DO DO_SS
         END IF
! 
         ALLOCATE(WDel(Max_NumSS), STAT = ierror(23))
         WDel%NumG=0
         WDel%NumH=0
         WDel%idTab=0
!
!
!***********************************************************************
!*                                                                     *
!*           MEMORY ALLOCATION TO ARRAYS OF ROCK PROPERTIES            *
!*                                                                     *
!***********************************************************************
!
!
! ...... Add a couple of extra media (to avoid array boundary errors)
!
         Max_NumMedia = Max_NumMedia + 2
! 
! ----------
! ...... Allocate memory to derived-type allocatable arrays     
! ----------
! 
         ALLOCATE(PoMed(Max_NumMedia), STAT=ierror(24))
! 
         ALLOCATE(media(Max_NumMedia), STAT=ierror(25))

!

! ----------
! ...... Allocate memory for diffusion flux arrays (binary_diffusion = .TRUE.) and diffusivities 
! ----------
! 
! 
            ALLOCATE(diffusivity(Max_NumMobPhases, Max_NumMassComp), STAT = ierror(13))

! 
!
!***********************************************************************
!*                                                                     *
!*        PARAMETERSS RELATED To GEOMECHANICAL/GEOCHEMICAL EFFECTS     *
!*                                                                     *
!***********************************************************************
!
!
!
         IF(Flag_CoupledGeomechanics .EQV. .FALSE.) number_geomech_parameters = 0
! 
! 
!***********************************************************************
!*                                                                     *
!*         OPEN FILE FOR DATA EXCHANGE WITH GEOMECHANICAL CODE         *
!*                                                                     *
!***********************************************************************
! 

        if (myid==iMaster) then    
         IF(Flag_CoupledGeomechanics) THEN
!
            INQUIRE(FILE='To_GMech',EXIST=exists)
            IF(exists) THEN
               OPEN(UNIT = To_Geomechanics_Unit, FILE = 'To_GMech', STATUS = 'OLD')
               REWIND (UNIT = To_Geomechanics_Unit)
            ELSE
               OPEN(UNIT = To_Geomechanics_Unit, FILE = 'To_GMech', STATUS = 'NEW')
            END IF
!
            INQUIRE(FILE='Fr_GMech',EXIST=exists)
            IF(exists) THEN
               OPEN(UNIT = From_Geomechanics_Unit, FILE = 'Fr_GMech', STATUS = 'OLD')
               REWIND (UNIT = From_Geomechanics_Unit)
            ELSE
               OPEN(UNIT = From_Geomechanics_Unit, FILE = 'Fr_GMech', STATUS = 'NEW')
            END IF
!
         END IF
!
!***********************************************************************
!*                                                                     *
!*         ENSURE PROPER MEMORY ALLOCATION - STOP IF PROBEMS           *
!*                                                                     *
!***********************************************************************
!
!
         DO_InitCheck: DO ii=0,27
!
            IF(ierror(ii) == 0) THEN
               WRITE(UNIT = MemoryAlloc_Unit, FMT = 6015) ii
            ELSE IF(ierror(ii) == 99999) THEN
               CONTINUE
            ELSE
               WRITE(36, FMT = 6020)  ii
               WRITE(UNIT = MemoryAlloc_Unit, FMT = 6020) ii
               CALL STOP_ALL
            END IF
!
         END DO DO_InitCheck
!
!
!***********************************************************************
!*                                                                     *
!*                      END OF THE DATA BLOCK                          *
!*                                                                     *
!***********************************************************************
!
!
      IF_FormatStyle: IF( NewInputFormat ) THEN
!
         READ( 35, FMT = '(A3)', IOSTAT = ierror(0) ) DataBlockEndSpecifier
!
!-----------
! ...... Stop if there is a problem reading the data block end specifies
!-----------
!
         IF( (ierror(0) == 0) .AND. (DataBlockEndSpecifier == '<<<') ) THEN
            RETURN
         ELSE
            WRITE (36, FMT = 6500)
            CALL STOP_ALL
         END IF
!
      END IF IF_FormatStyle

      END IF   !myid=iMaster

        call MPI_BCAST(DataBlockEndSpecifier, 3, MPI_CHARACTER,iMaster,MPI_COMM_WORLD, ierr)

        RETURN
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Core_Memory_Allocation',T50,'[v1.0,  29 September 2006]',/,        &
     &         ':::::   Allocate memory to most arrays based on size provided by input parameters')
! 
 6002 FORMAT(//,20('ERROR-'),//,T33,                                              &
     &             '       S I M U L A T I O N   A B O R T E D',/,                &
     &         T30,'The header <MEMORY> is missing at the top of the input file', &
     &       /,T32,'               CORRECT AND TRY AGAIN',                        &    
     &       //,20('ERROR-'))
 6010 FORMAT(//,20('ERROR-'),//,   &
     &       T5,'The coordinate system (variable <coordinate_system> = ',a11, &
     &          ' in subroutine <Core_Memory_Allocation>) is unavailable',//, &
     &       T5,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',   &
     &       //,20('ERROR-'))
!
 6015 FORMAT(T2,' Memory allocation at point ',i3,' in subroutine "Core_Memory_Allocation" was successful')
 6020 FORMAT(//,20('ERROR-'),//,                                          &
     &       T2,' Memory allocation at point ',i3,' in subroutine "Core_Memory_Allocation" was unsuccessful',//,   &
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',        &
     &       //,20('ERROR-'))
!
!
 6100 FORMAT(//,22('ERROR-'),//,T33,  &
     &             '       S I M U L A T I O N   A B O R T E D',/,                              &
     &         T12,'There is a problem reading the namelist <Basic_Parameter_Definitions> in the <Memory_Allocation> data block', &
     &       /,T32,'               CORRECT AND TRY AGAIN',                                      &
     &       //,22('ERROR-'))
!
 6105 FORMAT(//,22('ERROR-'),//,T33,  &
     &             '       S I M U L A T I O N   A B O R T E D',/,                              &
     &         T18,'There is a problem reading the namelist <System_Specifications> in the <Memory_Allocation> data block', &
     &       /,T32,'               CORRECT AND TRY AGAIN',                                      &
     &       //,22('ERROR-'))
!
 6500 FORMAT(//,22('ERROR-'),//,T33,  &
     &             '       S I M U L A T I O N   A B O R T E D',/,                                     &
     &         T10,'There is a problem reading the data block end specifier <DataBlockEndSpecifier> ', &
     &             'in the <Memory_Allocation> block',                                                 &
     &       /,T32,'               CORRECT AND TRY AGAIN',                                             &
     &       //,22('ERROR-'))
!
      END SUBROUTINE Core_Memory_Allocation_1

!STOP_ALLCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine STOP_ALL
       include "mpif.h"
       call flush(36)
       call mpi_abort(mpi_comm_world,1,ierr)
      end subroutine STOP_ALL

!ALLOC_MESSAGECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine Alloc_Message(iI)
       INTEGER::iI
       if (iI/=0) then
      !memory allocation error
       WRITE(36,*) 'Computer memory allocation error!'
       call stop_all
       end if
      end subroutine Alloc_Message

!DETERMINE_Connection_Elements_1CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
         SUBROUTINE Determine_Connection_Elements_1( NumElemTot, NumConx )
!
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        Routine for determining the neigbors of each element         *
!*                                                                     *
!*                  Version 1.0 - March 28, 2007                       *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
            USE Grid_Geometry
            USE temp_arrays
            IMPLICIT NONE
!
! -------------
! ......... Integer input variables
! -------------
!
            INTEGER, INTENT(IN) :: NumElemTot, NumConx
!
! -------------
! ......... Integer variables
! -------------
!
            INTEGER :: N_invalid_elem, n,iI
            character*8::EL1,EL2
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of <Determine_Connection_Elements>
!`
            ALLOCATE(ELEMINDEX(NumElemTot),STAT=iI) 
!
            if (iI/=0) then
             write(36,*) 'Something is wrong during memory allocation for ELEMINDEX'
             CALL STOP_ALL
            end if

            DO n = 1,NumElemTot
             ELEMINDEX(n)=n
            END DO
            CALL QCKSRTONEKEY(NumElemTot,ELEM0,ELEMINDEX) 
!
! ......... Determine the element numbers of the connections
!
            DO iI=1,NumConx
            EL1=conx(iI)%name1
            EL2=conx(iI)%name2
            CALL BSEARCH(NumElemTot, ELEM0, ELEMINDEX, EL1, N)
            IF(N.GT.0) THEN
              conx(iI)%n1 = N
            ELSE
              conx(iI)%n1 = 0
            END IF
            CALL BSEARCH(NumElemTot, ELEM0, ELEMINDEX, EL2, N)
            IF(N.GT.0) THEN
             conx(iI)%n2 = N
            ELSE
             conx(iI)%n2 = 0
            END IF
            END DO
!
! ......... Determine the total # of invalid elements in connections
!
            N_invalid_elem = COUNT(conx(1:NumConx)%n1 == 0) + COUNT(conx(1:NumConx)%n2 == 0)  ! Total # of invalid elements in connections
!
! ......... Print a warning
!
            IF(N_invalid_elem /= 0) THEN
               DO n = 1,NumConx
                  IF(conx(n)%n1 == 0) WRITE(36, FMT = 6008) conx(n)%name1, n
                  IF(conx(n)%n2 == 0) WRITE(36, FMT = 6008) conx(n)%name2, n
               END DO

            END IF
            deallocate(ELEMINDEX)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6008 FORMAT(' Reference to unknown element "',A8,'" at connection number ',I8,' ==> Will ignore connection')
!
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of <Determine_Connection_Elements>
!
!
            RETURN
!
         END SUBROUTINE Determine_Connection_Elements_1
!
!RMESH1CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! read in MESHB data
      subroutine RMesh1(iJ)
       USE temp_arrays
       USE MADIM
       USE Basic_Parameters
       implicit none
       integer::iI,iJ

       open (unit=70,file='MESHA',form='unformatted',status='old')
       open (unit=80,file='MESHB',form='unformatted',status='old')
       read(80) MNCON,MNEL
       IF (iJ>1) THEN
        ALLOCATE(ELEM0(MNEL),STAT=iI)
        ALLOCATE(MATX0(MNEL),STAT=iI)
        ALLOCATE(NEX1(MNCON),STAT=iI)
        ALLOCATE(NEX2(MNCON),STAT=iI)
        call Alloc_Message(iI)
        allocate (xx0(MNEL),STAT=iI)
        allocate (yy0(MNEL),STAT=iI)
        allocate (zz0(MNEL),STAT=iI)
       call Alloc_Message(iI)
       END IF

       read(80) (ELEM0(iI),iI=1,MNEL)
       read(80) (MATX0(iI),iI=1,MNEL)
       read(80) (NEX1(iI),iI=1,MNCON)
       read(80) (NEX2(iI),iI=1,MNCON)
       read(70) iI
       read(70) (xx0(iI),iI=1,MNEL)
       read(70) (yy0(iI),iI=1,MNEL)
       read(70) (zz0(iI),iI=1,MNEL)

       close(80)
       close(70)
 
       return
      end

!ALLOCGLOBALARRAY1CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

  ! allocate the public arrays for storing initial data that are not be allocated
       subroutine AllocGlobalArray1
       USE MADIM 
       USE Basic_Parameters
       USE Sources_and_Sinks
       USE Geologic_Media_Properties
       implicit none
       integer::iI,I

! ...... Add a couple of extra sources/sinks (to avoid array boundary errors)
!
         Max_NumSS = NumSS + 2
! 
! ----------
! ...... Allocate memory to derived-type     
! ----------
! 
         ALLOCATE(SS(Max_NumSS), STAT = iI)
! 
         IF(NumPhases > 1) THEN
            DO_SS : DO i = 1,Max_NumSS
               ALLOCATE(SS(i)%PhaseFracFlow(NumMobPhases), STAT = iI)
               ALLOCATE(SS(i)%PhaseVolRate(NumMobPhases),  STAT = iI)
            END DO DO_SS
         END IF
! 
         ALLOCATE(WDel(Max_NumSS), STAT = iI)
         call Alloc_Message(iI)
!
!
!***********************************************************************
!*                                                                     *
!*           MEMORY ALLOCATION TO ARRAYS OF ROCK PROPERTIES            *
!*                                                                     *
!***********************************************************************
!
!
!
! ...... Add a couple of extra media (to avoid array boundary errors)
!
         Max_NumMedia = NumMedia + 2
! 
! ----------
! ...... Allocate memory to derived-type allocatable arrays     
! ----------
! 
         IF(EOS_Name(1:7) == 'HYDRATE') THEN 
          ALLOCATE(PoMed(Max_NumMedia), STAT=iI)         
         END IF
! 
         ALLOCATE(media(Max_NumMedia), STAT=iI)
         call Alloc_Message(iI)
      end

!Core_Memory_Allocation_2CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE Core_Memory_Allocation_2
! 
         USE EOS_Parameters
         USE EOS_Default_Parameters
! 
         USE Basic_Parameters
         USE General_External_File_Units
! 
         USE General_Control_Parameters
         USE Diffusion_Parameters
! 
         USE Grid_Geometry
         USE Element_Attributes
         USE Connection_Attributes
! 
         USE Solution_Matrix_Arrays
         USE Sources_and_Sinks
         USE Geologic_Media_Properties
!
         USE temp_arrays
         USE MADIM  
         USE MPICOM 
         IMPLICIT NONE
! 
! ----------
! ...... Integer Arrays    
! ----------
! 
         INTEGER, DIMENSION(0:30) :: ierror = 99999
! 
! ----------
! ...... Integer variables     
! ----------
! 
         INTEGER :: ii,i,j,i1,j1
! 
! ----------
! ...... Logical variables     
! ----------
! 
         LOGICAL exists
! 
         SAVE 

        if(myid==iMaster) then
         i1=LNCON 
         j1=LNEL
         LNCON=LMNCON
         LNEL=LMNEL
        end if
!
! ----------
! ...... Allocate memory for arrays within the variable <ElemState> (derived type)  
! ----------
! 
         DO i = 1,LNEL
! 
           ALLOCATE(ElemState(i)%pres(-1:NumEqu), ElemState(i)%temp(-1:NumEqu), ElemState(i)%param(NumStateParam), STAT = ierror(2))
! 
         END DO
! 
! ----------
! ...... Allocate memory to derived-type arrays - Secondary variables   
! ----------
!  
         ALLOCATE(ElemProp(LNEL,0:NumEqu), STAT=ierror(3))
! 
! ----------
! ...... Allocate memory for arrays within the variable <ElemProp> (derived type)  
! ----------
! 
         DO_j1 : DO j = 0,NumEqu
            DO_i1 : DO i = 1,LNEL
! 
               ALLOCATE(ElemProp(i,j)%satur(Max_NumPhases),                      & 
     &                  ElemProp(i,j)%MassFrac(Max_NumMassComp, Max_NumPhases),  & 
     &                  ElemProp(i,j)%density(Max_NumPhases),                    & 
     &                  ElemProp(i,j)%enthalpy(Max_NumPhases),                   & 
     &                  ElemProp(i,j)%IntEnergy(Max_NumPhases),                  & 
     &                  ElemProp(i,j)%RelPerm(Max_NumMobPhases),                 & 
     &                  ElemProp(i,j)%viscosity(Max_NumMobPhases),               & 
     &                  ElemProp(i,j)%CapPres(Max_NumMobPhases-1),               &            
     &                  STAT = ierror(4))
!
                  do ii=1,Max_NumMobPhases
                     ElemProp(i,j)%RelPerm(ii)=0.d0
                  end do
 
            END DO DO_i1
         END DO DO_j1
! 
! ----------
! ...... Allocate memory for diffusion coefficients in the derived-type arrays (binary_diffusion = .TRUE.)  
! ----------
! 
         IF_BinDif1: IF( (Flag_BinaryDiffusion .EQV. .TRUE.) .AND. Max_NumMassComp >= 2 ) THEN
! 
            DO_j2 : DO j = 0,NumEqu
               DO_i2 : DO i = 1,LNEL
                  ALLOCATE(ElemProp(i,j)%DiffCoA(Max_NumMobPhases),   & 
     &                     ElemProp(i,j)%DiffCoB(Max_NumMobPhases),   & 
     &                     STAT = ierror(5))
               END DO DO_i2
            END DO DO_j2
! 
         END IF IF_BinDif1
! 
! ...... Allocate memory for additional data within the variable <ElemProp> (derived type)  
! 
         IF_More: IF(NumAuxParam > 0) THEN
! 
            DO_j3 : DO j = 0,NumEqu
               DO_i3 : DO i = 1,LNEL
                  ALLOCATE(ElemProp(i,j)%MoreData(NumAuxParam), STAT = ierror(6))
               END DO DO_i3
            END DO DO_j3
! 
         END IF IF_More
! 
! ----------
! ...... Allocate memory to double precision allocatable arrays     
! ----------
! 
         ALLOCATE(AI(LNEL), STAT=ierror(7))
! 
! ----------
! ...... Allocate memory to the integer arrays that provide the element starting locations in the arrays of the unknowns    
! ----------
! 
         ALLOCATE(Loc(LNEL), Locp(LNEL), STAT=ierror(8))
!
! ...... Define the locator arrays
!
         FORALL (i = 1 : LNEL)
            Loc(i)  = (i-1)*NumEqu
            Locp(i) = (i-1)*NumComPlus1
         END FORALL
!
!
!***********************************************************************
!*                                                                     *
!*          MEMORY ALLOCATION TO CONNECTION-RELATED ARRAYS             *
!*                                                                     *
!***********************************************************************
! 
! ----------
! ...... Allocate memory to derived-type arrays  
! ----------
! 
         ALLOCATE(ConxFlow(LNCON), STAT=ierror(9) )
! 
! ----------
! ...... Allocate memory to arrays within the derived type: ConxFlow  
! ----------
! 
         DO_Conx1 : DO i = 1,LNCON
! 
            ALLOCATE(ConxFlow(i)%rate(Max_NumMobPhases),     STAT = ierror(10))
            ALLOCATE(ConxFlow(i)%DarcyVel(Max_NumMobPhases), ConxFlow(i)%PoreVel(Max_NumMobPhases),  STAT = ierror(11))
! 
            ALLOCATE(ConxFlow(i)%CompInPhase(Max_NumMassComp, Max_NumMobPhases),  STAT = ierror(12))
! 
         END DO DO_Conx1 
! 
! ----------
! ...... Allocate memory for diffusion flux arrays (binary_diffusion = .TRUE.) and diffusivities 
! ----------
! 
         IF_BinDif2: IF( (Flag_BinaryDiffusion .EQV. .TRUE.) .AND. Max_NumMassComp >= 2 ) THEN
! 
            ALLOCATE( DiffusiveFlow(LNCON, Max_NumMobPhases, Max_NumMassComp), &
     &                STAT = ierror(13))
! 
         END IF IF_BinDif2
! 
!
!***********************************************************************
!*                                                                     *
!*            MEMORY ALLOCATION TO JACOBIAN MATRIX ARRAYS              *
!*                                                                     *
!***********************************************************************
!
!
         Max_NumPrimaryVar = NumComPlus1*LNEL
         MatrixOrder       =  NumEqu*LNEL
!  
! ----------
! ...... Allocate memory to double precision allocatable arrays     
! ----------
! 
            ALLOCATE(DX(Max_NumPrimaryVar),    STAT=ierror(14))
            ALLOCATE(DELX(Max_NumPrimaryVar),  STAT=ierror(15))
            ALLOCATE(old_accum(MatrixOrder),   STAT=ierror(16))
!  
            ALLOCATE(R(MatrixOrder + 1), STAT = ierror(17))
            ALLOCATE(rWorkA(MatrixOrder + 1), STAT = ierror(21))
            ALLOCATE(CO(NumEquPlus1*LNEL+2*LNEL), STAT = ierror(20))
            
!
!
!
!***********************************************************************
!*                                                                     *
!*        ARRAYS RELATED To GEOMECHANICAL/GEOCHEMICAL EFFECTS          *
!*                                                                     *
!***********************************************************************
!
! ...... Allocate memory for arrays related to geomechanical effects
!
! 
         IF(Flag_CoupledGeomechanics) THEN
            ALLOCATE(GeoMech(LNEL,0:NumEqu), STAT=ierror(18)) ! ... Stress and strain
! 
            IF_Geomech: IF( number_geomech_parameters > 0 ) THEN
! 
               DO_equ1 : DO j = 0,NumEqu
                  DO_elem1 : DO i = 1,LNEL
                   ALLOCATE(GeoMech(i,j)%param(number_geomech_parameters), STAT = ierror(19)) ! ... Additional parameters
                  END DO DO_elem1 
               END DO DO_equ1
! 
            END IF IF_Geomech
! 
         END IF
! 
!***********************************************************************
!*                                                                     *
!*         ENSURE PROPER MEMORY ALLOCATION - STOP IF PROBEMS           *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_InitCheck: DO ii=2,20
! 
            IF(ierror(ii) == 0) THEN
               IF(myid==iMaster) WRITE(MemoryAlloc_Unit,6015) ii
            ELSE IF(ierror(ii) == 99999) THEN
               CONTINUE
            ELSE
               WRITE(36,6020)  ii
               WRITE(MemoryAlloc_Unit,6020) ii
               CALL STOP_ALL
            END IF

            if(myid==iMaster) then
             LNCON=i1
             LNEL=j1
            end if
! 
         END DO DO_InitCheck
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
!
 6015 FORMAT(T2,' Memory allocation at point ',i3,' in subroutine "Core_Memory_Allocation" was successful')
!
 6020 FORMAT(//,20('ERROR-'),//,                                          &
     &       T2,' Memory allocation at point ',i3,' in subroutine "Core_Memory_Allocation" was unsuccessful',//,   &
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',        &
     &       //,20('ERROR-'))
!
! =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Core_Memory_Allocation
!
      END SUBROUTINE Core_Memory_Allocation_2
!
!
!RFILEWRKCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE RFILEWRK
! Some work which was done in original RFILE subroutine
!
      USE MADIM
      USE MPICOM
      USE Basic_Parameters
      USE Grid_Geometry
      USE Connection_Attributes
      USE Diffusion_Parameters
      USE Element_Attributes
      USE Geologic_Media_Properties
      USE General_Control_Parameters
      USE General_External_File_Units
      USE EOS_External_File_Units
      USE Subdomain_Definitions
      USE UPDATEINFO
   
      IMPLICIT none
      integer::iI,i,N,N0,iJ,j

       IF(Flag_SemianalyticHeatExchange .EQV. .FALSE.) THEN
        DEALLOCATE(AI, STAT=iI)
        DEALLOCATE(AHT, STAT=iI)
       END IF       
! ... Initialize fluxes: CAREFUL! Whole array operation
!
      IF((Flag_BinaryDiffusion) .AND. NumMobPhases > 1) THEN
        DiffusiveFlow = 0.0d0
      END IF

      ConxFlow%heat  = 0.0d0

      FORALL (n=1:NumConx) 
         ConxFlow(n)%rate(1:NumMobPhases)     = 0.0d0
         ConxFlow(n)%DarcyVel(1:NumMobPhases) = 0.0d0
         ConxFlow(n)%PoreVel(1:NumMobPhases)  = 0.0d0
         ConxFlow(n)%CompinPhase(1:NumCom,1:NumMobPhases) = 0.0d0
      END FORALL 
      IF (RadiativeHeat .EQV. .FALSE.) THEN
       DEALLOCATE (emissivity, STAT=iI)    
      END IF  
!
      DO i = 1,3
         DO ij=1,LNEL
          if (ElemMedia(ij,current)%perm(i)==0.0) then
           ElemMedia(ij,current)%perm(i) = media(elem(ij)%MatNum)%Perm(i)   ! Assign permeabilities
          end if
         END DO
      END DO

      IF(PermModType /= 'NONE') THEN
        FORALL (i=1:3) 
            ElemMedia(1:LNEL,current)%perm(i) = ElemMedia(1:LNEL,current)%perm(i) * elem(1:LNEL)%pm     ! Modify permeabilities if modifiers are provided 
        END FORALL
      END IF

!***********************************************************************
!*                                                                     *
!*           Store the initial/original state of the system            *
!*                                                                     *
!***********************************************************************
! 
! 
       ElemState%index(previous) = ElemState%index(current)   ! CAREFUL! Whole array operation
!
       ElemMedia(1:LNEL,original)%porosity = ElemMedia(1:LNEL,current)%porosity
       ElemMedia(1:LNEL,previous)%porosity = ElemMedia(1:LNEL,current)%porosity    
!
      FORALL (i=1:3)      
         ElemMedia(1:LNEL,original)%perm(i) = ElemMedia(1:LNEL,current)%perm(i)
         ElemMedia(1:LNEL,previous)%perm(i) = ElemMedia(1:LNEL,current)%perm(i)             
      END FORALL
!
      DO iI = 1,NumEqu
         ElemMedia(1:LNEL,iI)%porosity = ElemMedia(1:LNEL,current)%porosity  ! Initialize the incremented-state porosity
         FORALL (i=1:3) ElemMedia(1:LNEL,iI)%perm(i) = ElemMedia(1:LNEL,current)%perm(i)   ! Initialize the incremented-state permeability
      END DO

!
!***********************************************************************
!*                                                                     *
!*           DETERMINE NUMBER OF FLAG ELEMENT (FOR PRINT-OUT)          *
!*                                                                     *
!***********************************************************************
!
      TrackElemNum = 0
!
      IF_ActEl2: IF(TrackedElemName(1:5) /= '     ') THEN
!
         DO_NumEleB: DO n = 1,NumElem
!
            IF(elem(n)%name == TrackedElemName) THEN
               TrackElemNum = n
               EXIT
            END IF
!
         END DO DO_NumEleB
!
      END IF IF_ActEl2

      if(TrackElemNum>0) then
        if (myid/=iMaster) then
           call OPFILE(myid,'Track_Eleme_Info_',Track_Unit)
         end if
      end if 
!
!***********************************************************************
!*                                                                     *
!*           Find local index for subdomain elements                   *
!*                                                                     *
!***********************************************************************
!
       DO_OUT: DO i=1,NumSubdomains
                iJ=0
       DO_MID:  DO j=1, subdomain(i)%NumElements
       DO_IN:     DO n = 1,NumElem
                   iI=update(n)+1
                   IF(iI==subdomain(i)%ElemNum(j)) THEN
                    iJ=iJ+1
                    subdomain(i)%ElemNum(iJ)=n
                    EXIT DO_IN
                   END IF
                  END DO  DO_IN
                END DO    DO_MID
                subdomain(i)%NumElements=iJ
              END DO DO_OUT  
!
      CALL Time_Series_Printouts('Elem_')
      CALL Time_Series_Printouts('Conx_')
      CALL Time_Series_Printouts('SS_Ti')

      IF_PrintPmax: IF(Flag_MaxP_OutputFile .EQV. .TRUE.) THEN
!
         IF(myid==islave) WRITE(MaxP_Info_Unit,6105)
!
      END IF IF_PrintPmax

!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6105 FORMAT(T4,'Time(d)    Cell name     Maximum Pressure (Pa)      X(m)          Y(m)         Z(m)')
!
      RETURN
      END

!SAVE_HYD_INFOCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE SAVE_HYD_INFO
! Save time series data in the "Hydrate_Info" file
!
      USE EOS_External_File_Units
      USE temp_arrays
      USE MPICOM
      USE MADIM
!
      IMPLICIT NONE
      INCLUDE 'mpif.h'
      integer::N
      Real(KIND = 8),dimension(5,1000)::rTem 

          call MPI_Reduce(Hyd_Data(2:6,1:iDoSave), rTem(1:5,1:iDoSave), 5*iDoSave, &
     &    MPI_DOUBLE_PRECISION,MPI_SUM, iMaster, MPI_COMM_WORLD, ierr)
!
          IF(myid==iMaster) THEN
           DO n=1,iDoSave
           WRITE(Hydrate_Info_Unit,6200)  Hyd_Data(1,n),       &  ! Time in days
     &                                  rTem(1,n),             &  ! Cumulative CH4 mass release rate (kg/s)
     &                                  rTem(1,n)*1.47724d0,   &  ! Cumulative CH4 volume release rate (ST m^3/s)
     &                                  rTem(2,n),             &  ! Cumulative CH4 released/reacted mass (kg)
     &                                  rTem(3,n),             &  ! Cumulative CH4 released/reacted volume (ST m^3)
     &                                  rTem(4,n),             &  ! Total CH4 in the reservoir (ST m^3)
     &                                  rTem(5,n)                 ! Total mass of remaining hydrate in the reservoir (kg)
!     &                                  rTem(6,n),             &  ! Average pressure (Pa)
!     &                                  rTem(7,n)                 ! Average temperature (C) 
!
           END DO
          END IF
          iDoSave=0
          Hyd_Data=0.0

 6200 FORMAT(8(1pe14.7,3x),1pe14.7)
       RETURN
      END SUBROUTINE  SAVE_HYD_INFO
!
!SAVE_Conx_Time_SeriesCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      SUBROUTINE SAVE_Conx_Time_Series
       USE General_External_File_Units
       USE temp_arrays
       USE MPICOM
       USE MADIM
!
       IMPLICIT NONE
       INCLUDE 'mpif.h'
       integer::N
       Real(KIND = 8),dimension(9,1000)::rTem
! 
          call MPI_Reduce(Conx_Data(2:9,1:iDoConxT), rTem(1:8,1:iDoConxT), 8*iDoConxT, &
     &    MPI_DOUBLE_PRECISION,MPI_SUM, iMaster, MPI_COMM_WORLD, ierr)

          call MPI_Reduce(Conx_Data(1,1:iDoConxT), rTem(9,1:iDoConxT), iDoConxT, &
     &    MPI_DOUBLE_PRECISION,MPI_MAX, iMaster, MPI_COMM_WORLD, ierr)

!
          IF(myid==iMaster) THEN
           DO n=1,iDoConxT
            WRITE(UNIT = Conx_TimeSeries_Unit, FMT = 6010) rTem(9,n), rTem(1,n),rTem(2,n),rTem(3,n),rTem(4,n),rTem(5,n), &
     &      rTem(6,n),rTem(7,n),rTem(8,n) 
           END DO
          END IF
          iDoConxT=0
          Conx_Data=0.0

 6010 FORMAT(1pE12.5,2x,9(1pE14.7,1x))

       RETURN
      END SUBROUTINE SAVE_Conx_Time_Series

!SAVE_SS_SeriesCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 !
      SUBROUTINE SAVE_SS_Time_Series
       USE General_External_File_Units
       USE temp_arrays
       USE MPICOM
       USE MADIM
!
       IMPLICIT NONE
       INCLUDE 'mpif.h'
       integer::N
       Real(KIND = 8),dimension(6,1000)::rTem
!
          call MPI_Reduce(SS_Data(2:6,1:iDoSST), rTem(1:5,1:iDoSST), 5*iDoSST, &
     &    MPI_DOUBLE_PRECISION,MPI_SUM, iMaster, MPI_COMM_WORLD, ierr)

          call MPI_Reduce(SS_Data(1,1:iDoSST), rTem(6,1:iDoSST), iDoSST, &
     &    MPI_DOUBLE_PRECISION,MPI_MAX, iMaster, MPI_COMM_WORLD, ierr)
!
          IF(myid==iMaster) THEN
           DO n=1,iDoSST
            WRITE(UNIT = SS_TimeSeries_Unit, FMT = 6010) rTem(6,n), rTem(1,n),rTem(1,n)*1.47724d0,rTem(2,n), &
     & rTem(2,n)*1.47724d0,rTem(3,n),rTem(4,n),rTem(5,n) 
           END DO
          END IF
          iDoSST=0
          SS_Data=0.0
 6010 FORMAT(1pE12.5,2x,9(1pE14.7,1x))

       RETURN
      END SUBROUTINE SAVE_SS_Time_Series
!
!
!SAVE_SubDomain_Time_SeriesCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      SUBROUTINE SAVE_SubDomain_Time_Series
       USE Subdomain_Definitions
       USE temp_arrays
       USE MPICOM
       USE MADIM
!
       IMPLICIT NONE
       INCLUDE 'mpif.h'
       integer::N,i,iI
       Real(KIND = 8),dimension(11,1000)::rTem
!
       DO  n = 1,NumSubdomains
          rTem=0.0
          call MPI_Reduce(SubDomain_Data(n,1:11,1:iDoSubDomain), rTem(1:11,1:iDoSubDomain), 11*iDoSubDomain, &
     &    MPI_DOUBLE_PRECISION,MPI_SUM, iMaster, MPI_COMM_WORLD, ierr)
 
!
          IF(myid==iMaster) THEN
! ----------------
! ............ Write time series data for the pore-volume-averaged properties of the subdomain
! ----------------
!
               DO iI=1,iDoSubDomain
                SubDomain_Data(n,1:10,iI)=SubDomain_Data(n,1:10,iI)/(SubDomain_Data(n,11,iI)+1.0d-50)
                WRITE(UNIT = subdomain(n)%PrintUnitNumber, FMT = 6012) SubDomain_T(iI), (rTem(i,iI), i=1,11)
               END DO
          END IF
       END DO
!
          iDoSubDomain=0
          SubDomain_Data=0.0
 6012 FORMAT(11(1pE12.5,1x),1pE15.8) 
 
       RETURN
      END SUBROUTINE SAVE_SubDomain_Time_Series
!
! Print_Parallel_InfoCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE Print_Parallel_Info
       USE MPICOM
       USE MADIM
       USE AZ_ME_para
       USE General_Control_Parameters 
       USE Basic_Parameters

       IMPLICIT NONE
       INCLUDE 'mpif.h' 
       include 'az_aztecf.h'
       integer::gtotbrdlen,gN_external, N_external,maxN_external, minN_external,gnghbrs,maxnghbrs
       integer::gNEL, minnghbrs, NNCON, maxNCON,minNCON, irpntr,icpntr,ibpntr,ibindx,iindx
       integer::myamem,mymatvecmem,maxamem,maxmatvecmem,nghbrs
       N_external= NumElemTot-NumElem
       eetime2=MPI_WTIME()
       eetime2=eetime2-eetime1
!     Collect some information for output on processor 0:
      call MPI_ALLREDUCE(totbrdlen, gtotbrdlen, 1, MPI_INTEGER,MPI_SUM, MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(N_external, gN_external, 1, MPI_INTEGER,MPI_SUM, MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(N_external, maxN_external, 1, MPI_INTEGER,  MPI_MAX, MPI_COMM_WORLD, ierr) 
      call MPI_ALLREDUCE(N_external, minN_external, 1, MPI_INTEGER,  MPI_MIN, MPI_COMM_WORLD, ierr)
       nghbrs = data_org(AZ_N_Neigh+1)
      call MPI_ALLREDUCE(nghbrs, gnghbrs, 1, MPI_INTEGER,MPI_SUM, MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(nghbrs, maxnghbrs, 1, MPI_INTEGER,MPI_MAX,MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(NumElem, minNEL, 1, MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(NumElem, gNEL, 1, MPI_INTEGER,MPI_SUM, MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(NumElem, maxNEL, 1, MPI_INTEGER,MPI_MAX,MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(nghbrs, minnghbrs, 1, MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(NumConx, NNCON, 1, MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(NumConx, maxNCON, 1, MPI_INTEGER,MPI_MAX,MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(NumConx, minNCON, 1, MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD, ierr)

!   Use some temporary variables to calculate the size actually
!   used for storing the matrix. (This is typically smaller than the
!   allocated matrix.)
      irpntr = NumElem+1
      icpntr = gNEL+1
      ibpntr = irpntr
      ibindx = NumElem + 2*NumConx
      iindx = ibindx+1
      myamem = (laval*8+(irpntr+icpntr+ibpntr+ibindx+iindx)*4)/1024
      mymatvecmem = myamem + (2*NumElem+N_external)*NumEqu*8/1024
      call MPI_ALLREDUCE(myamem, maxamem, 1, MPI_INTEGER,MPI_MAX, MPI_COMM_WORLD, ierr)
      call MPI_ALLREDUCE(mymatvecmem, maxmatvecmem, 1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierr)

      if (myid .eq. iMaster) then
         write(36,*)'ZZZ  '
         write(36,*)'ZZZ  Number of processors = ',nprocs
         write(36,*)'ZZZ  '
 
         write(36,*)'ZZZ Total Number of time steps for this run = ', NumTimeSteps
         write(36,*)'ZZZ  '
         write(36,*)'ZZZ Average time in Aztec per time step = ',&
     &               CPU_MatrixSolTime/(1.e0*NumTimeSteps)
         write(36,*)'ZZZ Average time spent on other per time step = ',&
     &               (eetime2 - CPU_MatrixSolTime)/(1.e0*NumTimeSteps)
         write(36,*)'ZZZ  '
 
         write(36,*)'ZZZ Total number Newton steps = ',numlinsolve
         write(36,*)'ZZZ Average number of Newton steps per time step = ',&
     &               (1.e0*numlinsolve)/(1.e0*NumTimeSteps)
         write(36,*)'ZZZ Average time per Newton step = ',&
     &               CPU_MatrixSolTime/(1.e0*numlinsolve)
         write(36,*)'ZZZ Average time spent on other per Newton st = ',&
     &               (eetime2-CPU_MatrixSolTime)/(1.e0*numlinsolve)
         write(36,*)'ZZZ  '
                                 
         write(36,*)'ZZZ Total number of iter in solving linear Eq = ',itotitr
         write(36,*)'ZZZ Average iter num per call to solve linear Eq = ',&
     &               (1.e0*itotitr)/(1.e0*numlinsolve)
         write(36,*)'ZZZ Average time per iter in solving linear Eq = ',&
     &               CPU_MatrixSolTime/(1.e0*itotitr)
         write(36,*)'ZZZ  '
 
         write(36,*)'ZZZ Partitioning algorithm used: ',EE_partitioner
         write(36,*)'ZZZ Number of edges cut = ',edgecut
         write(36,*)'ZZZ  '
         write(36,*)'ZZZ Average number elements per proc = ',&
     &              (1.e0*gNEL)/nprocs
         write(36,*)'ZZZ Maximum number elements at any proc = ',&
     &              maxNEL
         write(36,*)'ZZZ Minimum number elements at any proc = ',&
     &              minNEL
         write(36,*)'ZZZ Allocated LNEL = ',NumElemTot 
         write(36,*)'ZZZ Average number connections per proc = ',&
     &              (1.e0*NNCON)/nprocs
         write(36,*)'ZZZ Minimum number connections at any proc = ',&
     &              minNCON
         write(36,*)'ZZZ Maximum number connections at any proc = ',&
     &              maxNCON
         write(36,*)'ZZZ Allocated LMNCON = ', LMNCON
 
         write(36,*)'ZZZ  '
         write(36,*)'ZZZ Average number of neighbors per proc = ',&
     &              (1.e0*gnghbrs)/nprocs
         write(36,*)'ZZZ Maximum number of neighbors at any proc = ',&
     &              maxnghbrs
         write(36,*)'ZZZ Minimum number of neighbors at any proc = ',&
     &              minnghbrs
 
         write(36,*)'ZZZ  '
         write(36,*)'ZZZ Average number of external elem. per proc = ',&
     &              (1.e0*gN_external)/nprocs
         write(36,*)'ZZZ Maximum number of external elem. per proc = ',&
     &              maxN_external
         write(36,*)'ZZZ Minimum number of external elem. per proc = ',&
     &              minN_external
         write(36,*)'ZZZ  '
         write(36,*)'ZZZ Maximum size for local matrix (in Kbyte) = ',&
     &              maxamem
         write(36,*)'ZZZ Maximum size data in matvec (in Kbyte) = ',&
     &              maxmatvecmem
         write(36,*)'ZZZ  '
         write(36,*)'ZZZ  ===='
         write(36,*)'ZZZ  '
         call flush(36)
         call solveinfo(36)
         call solveinfo(6)
         write(36,*)'ZZZ  ============================================='
         write(36,*)'ZZZ  '
         write(36,*)'ZZZ  '
         END IF 
         RETURN
         END SUBROUTINE Print_Parallel_Info

!DISTRDATACCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

! This subroutine was written by Keni Zhang to partially substitute
! the original data reading subroutine RFILE. In this subroutine  
! all data related to mesh are distributed during they
! are read. 

      subroutine DistrData(nexind)
      USE MADIM
      USE MPICOM
      USE KindData
      USE UPDATEINFO
      USE temp_arrays
      USE Basic_Parameters
      USE Element_Attributes
      USE Grid_Geometry
      USE Connection_Attributes, ONLY: emissivity
      USE Solution_Matrix_Arrays, ONLY: X      
      USE TagN
      implicit none
      include "mpif.h"
      
       integer::iI,pid,M,N
       integer(iK4):: nexind(*)
       integer(iK4),allocatable,dimension(:)::iWork,iTem,iIdxTem,iTem0
       integer::stat(MPI_STATUS_SIZE)
       real(KIND = 8),allocatable,dimension(:)::rWork,rTem,rTem0
       real(KIND = 8),allocatable,dimension(:,:)::rTem1
       character*8,allocatable,dimension(:)::cWork,cTem
       character*1,allocatable,dimension(:)::cTem1,cWork1

       tag=100
       do iI=1,30
        tag(iI)=tag(iI)+(iI-1)*nprocs
       end do

       if (myid==iSlave) then
       call MPI_SEND(X,MNEL*NumComPlus1,MPI_DOUBLE_PRECISION,iMaster,44, &
     &  MPI_COMM_WORLD,ierr)
       call MPI_SEND(por0,MNEL,MPI_DOUBLE_PRECISION,iMaster,41, &
     &   MPI_COMM_WORLD,ierr)
       call MPI_SEND(ELEM0,MNEL*8,MPI_CHARACTER,iMaster,42, &
     &   MPI_COMM_WORLD,ierr)
       call MPI_SEND(MATX0,MNEL,MPI_INTEGER,iMaster,43, &
     &   MPI_COMM_WORLD,ierr)      
       call MPI_SEND(indx0,MNEL,MPI_INTEGER,iMaster,45, &
     &   MPI_COMM_WORLD,ierr)

       call MPI_SEND(perm0,MNEL*3,MPI_DOUBLE_PRECISION,iMaster,46, &
     &   MPI_COMM_WORLD,ierr)
       DEALLOCATE(por0,X,ELEM0,MATX0,indx0,perm0)
      end if
      ALLOCATE(X(LNEL*NumComPlus1))
      if(myid==iMaster) then
       allocate(rTem(MNEL*NumComPlus1))
       allocate(rTem0(MNEL))
       allocate(rTem1(3,MNEL))
       allocate(cTem(MNEL))
       allocate(iTem(MNEL))
       allocate(iTem0(MNEL))
       allocate(iIdxTem(0:N_unknowns))
       allocate(rWork(LMNEL*NumComPlus1))
       call MPI_RECV(rTem,MNEL*NumComPlus1,MPI_DOUBLE_PRECISION,iSlave,44, &
     &  MPI_COMM_WORLD,stat,ierr)
       call MPI_RECV(rTem0,MNEL,MPI_DOUBLE_PRECISION,iSlave,41, &
     &  MPI_COMM_WORLD,stat,ierr)
        call MPI_RECV(cTem,MNEL*8,MPI_CHARACTER,iSlave,42, &
     & MPI_COMM_WORLD,stat,ierr)
        call MPI_RECV(iTem,MNEL,MPI_INTEGER,iSlave,43, &
     & MPI_COMM_WORLD,stat,ierr)
        call MPI_RECV(iTem0,MNEL,MPI_INTEGER,iSlave,45, &
     & MPI_COMM_WORLD,stat,ierr)
       call MPI_RECV(rTem1,MNEL*3,MPI_DOUBLE_PRECISION,iSlave,46, &
     &  MPI_COMM_WORLD,stat,ierr)

! 
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
         iIdxTem=iUpdateIdx(pid,:)
         call rsendind(rTem, NumComPlus1, iUpdateN(pid), pid, iIdxTem, tag(1)+pid, rwork)
        END IF
       end do

       iIdxTem=iUpdateIdx(iMaster,:)
       call rindtransform(rTem, NumComPlus1, iIdxTem, iUpdateN(iMaster), rwork, 'R')
       X(1:LNEL*NumComPlus1)=rTEM(1:LNEL*NumComPlus1)       
     
       deallocate(rTem,rWork)
!
       allocate(rWork(LMNEL))
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
         iIdxTem=iUpdateIdx(pid,:)
         call rsendind(rTem0, 1, iUpdateN(pid), pid, iIdxTem, tag(2)+pid, rwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call rindtransform(rTem0, 1, iIdxTem, iUpdateN(iMaster), rwork, 'R')
       ElemMedia(1:LNEL,current)%porosity =rTEM0(1:LNEL)
       deallocate(rTem0)
!


       DO iI=1,3
        do pid=0,nprocs-1
         IF(pid .NE. iMaster) THEN
          iIdxTem=iUpdateIdx(pid,:)
          call rsendind(rTem1(iI,:), 1, iUpdateN(pid), pid, iIdxTem, tag(23+iI)+pid, rwork)
         END IF
        end do
        iIdxTem=iUpdateIdx(iMaster,:)
        call rindtransform(rTem1(iI,:), 1, iIdxTem, iUpdateN(iMaster), rwork, 'R')
        ElemMedia(1:LNEL,current)%perm(iI)=rTem1(iI,1:LNEL)
       END DO
       deallocate(rTem1,rWork)

       allocate(cWork(LMNEL))
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
         iIdxTem=iUpdateIdx(pid,:)
         call csendind(cTem, 1, iUpdateN(pid), pid,iIdxTem, tag(3)+pid, cwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call c8indtransform(cTem, 1,iIdxTem ,iUpdateN(iMaster), cwork, 'R')
       ELEM(1:LNEL)%name=cTEM(1:LNEL)
       deallocate(cTem,cWork)
!
       allocate(iWork(LMNEL))
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
         iIdxTem=iUpdateIdx(pid,:)
         call isendind(iTem, 1, iUpdateN(pid), pid,iIdxTem, tag(4)+pid, iwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call iindtransform(iTem, 1, iIdxTem,iUpdateN(iMaster), iwork, 'R')
       ELEM(1:LNEL)%MatNum=iTEM(1:LNEL)
       deallocate(iTem)
!
       do pid=0,nprocs-1

        IF(pid .NE. iMaster) THEN
         iIdxTem=iUpdateIdx(pid,:)
         call isendind(iTem0, 1, iUpdateN(pid), pid,iIdxTem, tag(5)+pid, iwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call iindtransform(iTem0, 1, iIdxTem,iUpdateN(iMaster), iwork, 'R')
       ElemState(1:LNEL)%index(current)=iTem0(1:LNEL)
!
       deallocate(iTem0,iWork)
       allocate(rTem(MNEL))
       allocate(rWork(LMNEL))
!
! read data from Mesh data file and distributed the data

       open (unit=70,file='MESHA',form='unformatted',status='old')
       read(70) iI
!      iI read from here should equal to MNEL     
       IF(iI .NE. MNEL) THEN
        WRITE(36, *) 'The MESHA and MESHB are not correct, delete them and run again.'
        CALL STOP_ALL
       END IF
!
       read(70) (rTem(iI),iI=1,MNEL)   ! for coord(1)

       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid, iIdxTem, tag(6)+pid, rwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call rindtransform(rTem, 1, iIdxTem,iUpdateN(iMaster), rwork, 'R') 
       elem(1:LNEL)%coord(1)=rTEM(1:LNEL) 
!
       read(70) (rTem(iI),iI=1,MNEL)   ! for coord(2)
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid, iIdxTem, tag(7)+pid, rwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call rindtransform(rTem, 1, iIdxTem, iUpdateN(iMaster), rwork, 'R')
       elem(1:LNEL)%coord(2)=rTEM(1:LNEL)
!
       read(70) (rTem(iI),iI=1,MNEL)   ! for coord(3)
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid, iIdxTem, tag(8)+pid, rwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call rindtransform(rTem, 1, iIdxTem, iUpdateN(iMaster), rwork, 'R')
       elem(1:LNEL)%coord(3)=rTEM(1:LNEL)
!
       read(70) (rTem(iI),iI=1,MNEL)   ! for vol
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid, iIdxTem, tag(9)+pid, rwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call rindtransform(rTem, 1, iIdxTem, iUpdateN(iMaster), rwork, 'R')
       elem(1:LNEL)%vol=rTEM(1:LNEL)
!
       read(70) (rTem(iI),iI=1,MNEL)   ! for PM    
       IF(ALL(rTem(1:MNEL) == 0.0)) rTem=1.0
       allocate(pmn(MNEL))
       pmn(1:MNEL)=rTem(1:MNEL)
       call Initialize_Perm_Modifiers
       rTem(1:MNEL)=pmn(1:MNEL)
       deallocate(pmn)
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid,iIdxTem, tag(10)+pid, rwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call rindtransform(rTem, 1, iIdxTem, iUpdateN(iMaster), rwork, 'R')
       elem(1:LNEL)%pm=rTEM(1:LNEL)
!
       read(70) (rTem(iI),iI=1,MNEL)   ! for AHT
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid, iIdxTem, tag(11)+pid, rwork)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call rindtransform(rTem, 1, iIdxTem, iUpdateN(iMaster), rwork, 'R')
       aht(1:LNEL)=rTEM(1:LNEL)
       deallocate(rTem)
!
       allocate(cTem1(MNEL))
       allocate(cWork1(LMNEL))
       read(70) (cTem1(iI),iI=1,MNEL)   ! for activity flag
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call c1sendind(cTem1, 1, iUpdateN(pid), pid, iIdxTem, tag(12)+pid, cWork1)
        END IF
       end do
       iIdxTem=iUpdateIdx(iMaster,:)
       call c1indtransform(cTem1, 1, iIdxTem, iUpdateN(iMaster), cWork1, 'R')
       elem(1:LNEL)%activity=cTEM1(1:LNEL)   

       DEALLOCATE(cWork1,cTem1,iUpdateIdx,iUpdateN,iIdxTem,rWork)

 ! read connection-related arrays
 !      iUpdateIdx and iUpdateN are used to store connection
 !      index and connection number of each PE
  
       ALLOCATE(iUpdateIdx(nprocs-1,LMNCON))
       ALLOCATE(iUpdateN(nprocs-1))
       ALLOCATE(iIdxTem(LMNCON))  
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        call MPI_RECV(iUpdateN(pid),1,MPI_INTEGER,pid,tag(13)+pid,MPI_COMM_WORLD,stat,ierr)
        N=iUpdateN(pid)
        call MPI_RECV(iIdxTem,N,MPI_INTEGER,pid,tag(14)+pid,MPI_COMM_WORLD,stat,ierr)
        iUpdateIdx(pid,:)=iIdxTem
        END IF
       end do
!
       allocate(rTem(MNCON))
       allocate(rWork(LMNCON))

       read(70) (rTem(iI),iI=1,MNCON)   ! for d1
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid,iIdxTem, tag(15)+pid, rwork) 
        END IF
       end do
       call rindtransform(rTem, 1, nexind, LNCON, rwork, 'R')
       conx(1:LNCON)%d1=rTEM(1:LNCON)       
!
       read(70) (rTem(iI),iI=1,MNCON)   ! for d2
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid,iIdxTem, tag(16)+pid, rwork)
        END IF
       end do
       call rindtransform(rTem, 1, nexind, LNCON, rwork, 'R')
       conx(1:LNCON)%d2=rTEM(1:LNCON)  
!
       read(70) (rTem(iI),iI=1,MNCON)   ! for area
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid,iIdxTem, tag(17)+pid, rwork)
        END IF
       end do
       call rindtransform(rTem, 1, nexind, LNCON, rwork, 'R')
       conx(1:LNCON)%area=rTEM(1:LNCON)   
!
       read(70) (rTem(iI),iI=1,MNCON)   ! for beta
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid,iIdxTem, tag(18)+pid, rwork)
        END IF
       end do
       call rindtransform(rTem, 1, nexind, LNCON, rwork, 'R')
       conx(1:LNCON)%beta=rTEM(1:LNCON)
!
       read(70) (rTem(iI),iI=1,MNCON)   ! for  emissivity
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call rsendind(rTem, 1, iUpdateN(pid), pid,iIdxTem, tag(19)+pid, rwork)
        END IF
       end do
       call rindtransform(rTem, 1, nexind, LNCON, rwork, 'R')
       emissivity(1:LNCON)=rTEM(1:LNCON)
       DEALLOCATE(rWork,rTem)
!
       allocate(iTem(MNCON))
       allocate(iWork(LMNCON))   
       read(70) (iTem(iI),iI=1,MNCON)   ! for ki
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call isendind(iTem, 1, iUpdateN(pid), pid, iIdxTem, tag(20)+pid, iwork)
        END IF
       end do
       call iindtransform(iTem, 1, nexind, LNCON, iwork, 'R')
       conx(1:LNCON)%ki=iTEM(1:LNCON)
!     
       DEALLOCATE(iWork,iTem)
       allocate(cTem(MNCON))
       allocate(cWork(LMNCON))
!      
       read(70) (cTem(iI),iI=1,MNCON)   ! for name1
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call csendind(cTem, 1, iUpdateN(pid), pid, iIdxTem, tag(21)+pid, cwork)
        END IF
       end do
       call c8indtransform(cTem, 1, nexind,LNCON, cwork, 'R') 
       conx(1:LNCON)%name1=cTEM(1:LNCON)          
!
       read(70) (cTem(iI),iI=1,MNCON)   ! for name2
       do pid=0,nprocs-1
        IF(pid .NE. iMaster) THEN
        iIdxTem=iUpdateIdx(pid,:)
        call csendind(cTem, 1, iUpdateN(pid), pid, iIdxTem, tag(22)+pid, cwork)
        END IF
       end do
       call c8indtransform(cTem, 1, nexind,LNCON, cwork, 'R')
       conx(1:LNCON)%name2=cTEM(1:LNCON)
       close(70)
       DEALLOCATE(cwork,cTem,iUpdateN,iUpdateIdx,iIdxTem)

      else
       call recvcom(myid,LNEL)
       call MPI_SEND(LNCON,1,MPI_INTEGER,iMaster,tag(13)+myid,MPI_COMM_WORLD,ierr) 
       call MPI_SEND(nexind,LNCON,MPI_INTEGER,iMaster,tag(14)+myid, MPI_COMM_WORLD,ierr)
       call recvallcon(myid,LNCON)
  
      end if   ! myid==iMaster
      return
      end 
  
 
!NEX2ADJCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine nex2adj(ADJ, XADJ) 
      USE KindData
      USE temp_arrays
      USE MADIM
      IMPLICIT NONE

      integer(iK4):: ADJ(*), XADJ(*)
      integer(iK4),ALLOCATABlE,DIMENSION(:)::iTem
      integer::nel,ncon
!     Input Scalars
!     nel:  number of elements
!     ncon: number of connections

!     Input arrays
!     nex1(ncon) and nex2(ncon) 
 
!     Output arrays
!     ADJ: Must be an array of size 2*ncon
!     XADJ: Must be of size  nel+1.

!     On output, ADJ and XADJ contains the adjecency matrix
!     representing all connections between elements.
!     The matrix is stored in compressed row format, also known as 
!     compact storage format (CSR) and this is the format requested
!     by METIS. In short, ADJ(XADJ(i)) ... ADJ(XADJ(j+1)-1) contains
!     the column indices for elements on row(i) in the adjecency 
!     matrix. In this context this means that all neighbours to node
!     number i are given by ADJ(XADJ(i)) ... ADJ(XADJ(j+1)-1).
!     Since all nodes and edges are unit weight, no vectors are used
!     to represent these (and hence we do not use the full sparse
!     matrix representation, instead we only use row and column 
!     indices,

!     Local variables
      integer i, ii, j, el1, el2,n1,n2
 
!     First we store the matrix temporarly, row by row. We here use
!     ADJ and XADJ as temporary arrays so that ADJ(i,j) tells which
!     is the i:th neighbour of the j:th node. we use maxnbs as the
!     leading dimension in ADJ in this temporary usage.
!     Here XADJ(j) is used to tell the current number of neighbours
!     found for node j. Note that this is only a temporary usage
!     of these vectors. The final data for these elements are
!     computed after this loop nest.

      nel=MNEL
      ncon=MNCON
 
        ALLOCATE(iTem(NEL))
        iTEM=0
      do j=1,ncon
       el1=nex1(j)
       el2=nex2(j)
       XADJ(el1)=XADJ(el1)+1
       XADJ(el2)=XADJ(el2)+1
      end do 
      j=XADJ(1)
      XADJ(1)=1
      do i=2,nel+1
       ii=XADJ(i)
       XADJ(i)=XADJ(i-1)+j
       j=ii
      end do   

      do j = 1, ncon
         el1 = nex1(j)
         el2 = nex2(j)
         iTEM(el1)=iTEM(el1)+1
!     Now insert el2 at the correct place in el1:s list of
!     neighbours
         i = 1
         n1=iTEM(el1)
         n2=XADJ(el1)-1

!     First skip neighbours with smaller number than el2
         do while ((i .lt. n1) &
     &       .AND. (ADJ(i+n2) .lt. el2)) 
            i = i + 1
         end do
 
!     Now we know that el2 should be put at place i, but first we
!     need to shift the ones with numbers higher than el2.

         do ii =n1-1, i, -1
            ADJ(ii+n2+1) = ADJ(ii+n2)
         end do
  !       ADJ(i+n2) = el2        
! modified  for temporarily to keep the global index information and 
! connection information in one array which are needed in getlnex.
         ADJ(i+n2)=-j

!     Now, repeat the whole procedure for el2:s new neighbour el1

!     Increment the neighbour counter for el2
         iTEM(el2) = iTEM(el2) + 1

!     Now insert el1 at the correct place in el2:s list of
!     neighbours
         i = 1
         n1=iTEM(el2)
         n2=XADJ(el2)-1

!     First skip neighbours with smaller number than el1
         do while ((i .lt. n1) &
     &       .AND. (ADJ(i+n2) .lt. el1))
           i = i + 1
         end do
 
!     Now we know that el1 should be put at place i, but first we
!     need to shift the ones with numbers higher than el1.

         do ii = n1-1, i, -1
            ADJ(ii+n2+1) = ADJ(ii+n2)
         end do
!         ADJ(i+n2) = el1
! modified for temporarily to keep the global index information and
! connection information in one array which are needed in getlnex.

         ADJ(i+n2)=j

      end do
 
      deallocate(iTEM)

      return
      end

!INITJACCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine initjac(ADJ,XADJ,nbr,nbc,neq,rpntr,cpntr,bpntr,bindx) 
      USE KindData
      IMPLICIT NONE

!     INITJAC sets up all pointers for storing the Jacobian matrix.
!     The storage scheme used is the DVBR format as described in
!     the AZTEC user's guide version 2.0. For short, this is a block 
!     format described by a set of pointers that identify each block
!     of the matrix (our Jacobian is an NEL x NEL block matrix).
!     The matrix elements in each block (of size NEQ x NEQ) are stored
!     consecutively, column order. The output arrays rpntr, cpntr, bpntr, 
!     bindx, and indx fully describes the structure of the matrix. 
!     In addition, another array (of size (2*NCON + NEL)*NEQ*NEQ is used
!     to hold the actual matrix elements. This array is not considered 
!     in this routine, instead it will first be used when the Jacobian 
!     matrix elements are computed.

!     In TOUGH2 all arrays are normally indexed from element 1 (often
!     called Fortran style) as opposed to indexing from 0 (C style).
!     The AZTEC software requires the DVBR representation to use
!     vectors indexed from 0 which means that also the pointers used 
!     here must make that assumption. Therefore, all routines that 
!     explicitly handles the DVBR representation of the Jacobian will 
!     assume indexing from 0, even though all other to TOUGH2 routines 
!     still views the matrix as starting from element (1,1). For example,
!     when adding an element to the first position in the Jacobian,
!     the element should be referred to as element (1,1) when calling 
!     the routine XXXX which inserts the element as element (0,0) in
!     the DVBR structure.

!     Input parameters
      integer nbr, nbc, neq
      integer(iK4)::ADJ(1:*), XADJ(1:*)
 
!     Output parameters
      integer(iK4):: rpntr(0:*), cpntr(0:*), bpntr(0:*), bindx(0:*)
  
!     Input Scalars
!     nbr: Number of block rows. If this is the full Jacobian then
!       nbr is normally equal to NEL. If this is only a part of
!       the whole Jacobian, nbr should be the number of grid elements
!       represented by this part of the Jacobian matrix.
!     nbc: number block columns. Normally equal to NEL.
!     neq: number of equation per grid elements

!     Input Arrays
!     ADJ(1:*), XADJ(1:*) as produced by routine nex2adj
 
!     Output arrays
!     rpntr(0:nbr)
!     cpntr(0:nbc)
!     bpntr(0:nbr)
!     bindx(0:nbr)
!     indx(0:XADJ(nbr+1)-1) (the number of matrix blocks to be stored,
!                         i.e., 0:2*ncon+nel for the full Jacobian)

!     Local variables
      integer i, j, k

!     Block row k starts at k*neq (i.e., each block row has neq rows).
      do k = 0, nbr
         rpntr(k) = k*neq
      end do 

!     Block column k starts at k*neq (i.e., each block column has 
!     neq columns).
      do k = 0, nbc
         cpntr(k) = k*neq
      end do 

!     Insert number of non zero blocks in each block row.
!     The k added is for the diagonal block (which is not counted in 
!     xadj). The subtraction "-1" is to compensate for indexing from 
!     one in xadj.
      do k = 0, nbr
         bpntr(k) = xadj(k+1) + k - 1
      end do 
 
!     For each block row, insert the column indices for all non zero
!     blocks. This should result in an array bindx that is equal to 
!     adj, with three exceptions: it should start indexing from 0, 
!     all column indices provided by this vector should be "1" 
!     smaller than in adj, and there should be new entries inserted 
!     for the diagonal elements.

      k = 0
      do i = 1, nbr
!     Insert diagonal element first in block row
         bindx(k) = i-1
         k = k + 1
         do j = xadj(i), xadj(i+1)-1
            bindx(k) = adj(j) - 1
            k = k + 1
         end do
      end do


! Beginning of alternative section (not used now) where the Diagonal
! blocks are stored "in place" instead of first in each block row.
! If this section is to be used, insert declaration "logical dfound" 
! is above.
!
!      k = 0
!      do i = 1, nbr
!      dfound = .FALSE.
!      do j = xadj(i), xadj(i+1)-1
!         if ((.NOT. dfound) .AND. (adj(j) .gt. i)) then
!           Insert diagonal element
!            bindx(k) = i-1
!            k = k + 1
!            dfound = .TRUE.
!         end if
!         bindx(k) = adj(j) - 1
!         k = k + 1
!      end do
!     Special treatment of the case where the diagonal element 
!     is the last non-zero block on the row.
!      if (.NOT. dfound) then
!        Insert diagonal element
!         bindx(k) = i-1
!         k = k + 1
!      end if
!      end do
! End of alternative section for storage of diagonal blocks

!     Number of non zeros in all blocks (which are all the same size)
!      do i = 0, k
!         indx(i) = neq*neq*i
!      end do 
      
      return
      end

!ADD2ACCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine add2a(brow, bcol, row, col, value, rpntr, cpntr, bpntr, bindx, indx, aval)
      USE KindData
      IMPLICIT NONE
  
!     Inserts "value" in the block (brow, bcol) at position (row, col).

!     Input parameters
      integer brow, bcol, row, col
      integer(iK8):: rpntr(0:*), cpntr(0:*), bpntr(0:*), bindx(0:*)
      integer(iK8):: indx(0:*) 
      real*8 value

!     Input/Output parameters
      real*8 aval(0:*)

!     Local variables
      integer br, bc, r, c, ks, ke, ki, blklda
      logical found
  
!     Create new indices for indexing from (0,0) instead of from (1,1)
!     Then, never use brow, bcol, row, and col in this routine again!

      br = brow - 1 
      bc = bcol - 1 
      r = row - 1 
      c = col - 1 

!     Go through block row br (from ks to ke) in order to find 
!     the block column bc that should be used here.
!     Index for the correct block column is stored in the variable ki.
      found = .FALSE.
      ks = bpntr(br)
      ke = bpntr(br+1)-1
      ki = ks
      do while ( (.NOT. found) .AND. (ki .LE. ke) )
         if (bindx(ki) .EQ. bc) then
            found = .TRUE.
         else
            ki = ki + 1
         end if
      end do

      if (.NOT. found) then
         write(36,*)'Error in ADD2A: could not find block of A'
         write(36,*)'br, bc, r, c = ',br, bc, r, c
         write(36,*)'rpntr = ',(rpntr(ki), ki=0, 20)
         write(36,*)'cpntr = ',(cpntr(ki), ki=0, 20)
         write(36,*)'bpntr = ',(bpntr(ki), ki=0, 20)
         write(36,*)'bindx = ',(bindx(ki), ki=0, 20)
         write(36,*)'indx = ',(indx(ki), ki=0, 30)
      else
!     The leading dimension for the block is denoted blklda
         blklda = rpntr(br+1) - rpntr(br)
         aval(indx(ki)+c*blklda+r) = aval(indx(ki)+c*blklda+r) + value

      end if

      return
      end

!ADD2BCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine add2b( b, ndimb, cpntr, ibkmax, ibrow, irow, value)
      
      USE KindData
      USE MPICOM
      integer(iK8)::cpntr(*)
      integer ibkmax, irow, ibrow, ndimb
!
      real*8 b(*), value
!
!       add "value" to b() vector
!
!
!    input:
!       b()    rhs vector
!       cpntr()  See description of VBR format i AZTEC User's Guide.
!                cpntr is identical to the vector kvst used in an
!                earlier version of this program.
!       ndimb   size of b()
!       ibkmax  number of nodes
!       ibrow   block row
!       irow    subvector row
!       value   value to be added
!
       integer itemp

       itemp = cpntr(ibrow) + irow

       b( itemp) =  b(itemp) + value
       return
       end

!CPLIDXCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine cplidx(pid, part, nel, nblkrow, nblkcol, nblk, &
     &  lupdate, lrpntr, lcpntr, lbpntr, lbindx, lindx, &
     &  rpntr, cpntr, bpntr, bindx, sizes)

!     Given a matrix in VBR format, all parts that belongs to
!     processor "pid" are copied into the corresponding arrays 
!     with the prefix "l". The vector "part" tells what
!     elements belongs to processor "pid".
!
      USE KindData
      IMPLICIT NONE
  
!     Input parameters
      integer(iK8):: lrpntr(0:*), lcpntr(0:*), lbpntr(0:*), lbindx(0:*)
      integer(iK8):: lindx(0:*) , lupdate(0:*)
      integer(iK4):: rpntr(0:*), cpntr(0:*), bpntr(0:*), bindx(0:*)
      integer pid, nel, nblkrow, nblkcol, nblk
      integer(iK8) sizes(*)
      integer(iK4)::part(0:*)

!     Local variables
      integer i, j, k, numrow, blksiz, colind, numblk, aind, el

      i = 0
      j = 0
      k = 0
      numrow = rpntr(1)   ! Number of rows in each block
      blksiz = rpntr(1)*cpntr(1) ! #elem's/block (here assumed uniform)

      lrpntr(0) = 0
      lbpntr(0) = 0
      lindx(0) = 0
      do el = 0, nel-1
        if (part(el) .EQ. pid) then
           lupdate(i) = el
           i = i + 1
           numblk = bpntr(el+1) - bpntr(el)
           lrpntr(i) = lrpntr(i-1) + numrow
           lbpntr(i) = lbpntr(i-1) + numblk
           do colind = bpntr(el), bpntr(el+1) - 1
              lbindx(k) = bindx(colind)
              k = k + 1
              lindx(k) = lindx(k-1) + blksiz
           end do
        end if
      end do
      
      sizes(1) = i
      sizes(2) = i
      sizes(4) = i
      sizes(5) = k-1
      sizes(6) = k
      

      nblkrow = i
      nblk = k

      nblkcol = nel
      do i = 0, nblkcol
        lcpntr(i) = cpntr(i)
      end do

      sizes(3) = nblkcol

      return
      end


!DISTIDXCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine distidx(myid, part, nel, nblkrow, nblkcol, &
     &  nblk, nprocs, lxlen, nk1, &
     &  lupdate, ilupdate, lrpntr, ilrpntr, &
     &  lcpntr, ilcpntr, lbpntr, ilbpntr, &
     &  lbindx, ilbindx, lindx, ilindx,&
     &  rpntr, irpntr, &
     &  cpntr, icpntr, bpntr, ibpntr,  &
     &  bindx, ibindx, iindx, err) 

      USE KindData
      USE UPDATEINFO
      USE MADIM
      IMPLICIT NONE
      include "mpif.h"

!     Given a matrix in VBR format on processor 0, this routine
!     distributes all local index vectors to all processors (including
!     the parts that processor 0 should keep for itself.
     
  
!     Input parameters
      integer::i
      integer(iK8):: lrpntr(0:*), lcpntr(0:*), lbpntr(0:*), lbindx(0:*)
      integer(iK8):: lindx(0:*) 
      integer ilrpntr, ilcpntr, ilbpntr, ilbindx, ilindx, ilupdate
      integer(iK4):: rpntr(0:*), cpntr(0:*), bpntr(0:*), bindx(0:*)
      integer(iK8):: lupdate(0:*),lxlen(0:*)
      integer(iK4):: part(0:*)
      integer irpntr, icpntr, ibpntr, ibindx, iindx, iupdate
      integer myid, nel, nblkrow, nblkcol, nblk, nprocs
      integer nk1, err

!     Local variables
      integer pid, locsiz(3), stat(MPI_STATUS_SIZE), ierr, iMaster
      integer noupdate, maxnoupdate
      integer NEQ, sizes(8), msizes(8)
      data msizes/8*0/

      integer max
      intrinsic max

      maxnoupdate = MAXXPART
      iMaster=err
      err=0

      if (myid .EQ. iMaster) then
         do pid = 0, nprocs-1
         IF(pid .NE. iMaster) THEN
!
!        For each processor, get the values for VBR indices and 
!        send as separate messages.
!
            call cplidx(pid, part, nel, nblkrow, nblkcol, nblk, &
     &           lupdate, lrpntr, lcpntr, lbpntr, lbindx, lindx, &
     &           rpntr, cpntr, bpntr, bindx, sizes)
            msizes(1) = max(sizes(1), msizes(1))
            msizes(2) = max(sizes(2), msizes(2))
            msizes(3) = max(sizes(3), msizes(3))
            msizes(4) = max(sizes(4), msizes(4))
            msizes(5) = max(sizes(5), msizes(5))
            msizes(6) = max(sizes(6), msizes(6))
            locsiz(1) = nblkrow
            locsiz(2) = nblkcol
            locsiz(3) = nblk
            call MPI_SEND(locsiz, 3, MPI_INTEGER, pid, & 
     &                    pid, MPI_COMM_WORLD, ierr)
            call MPI_SEND(lrpntr, nblkrow+1, MPI_INTEGER, pid, &
     &                    1000+pid, MPI_COMM_WORLD, ierr) 
            call MPI_SEND(lcpntr, nblkcol+1, MPI_INTEGER, pid, &
     &                    2000+pid, MPI_COMM_WORLD, ierr)
            call MPI_SEND(lbpntr, nblkrow+1, MPI_INTEGER, pid, &
     &                    3000+pid, MPI_COMM_WORLD, ierr)
            call MPI_SEND(lbindx, nblk, MPI_INTEGER, pid, &
     &                    4000+pid, MPI_COMM_WORLD, ierr)
            call MPI_SEND(lindx, nblk+1, MPI_INTEGER, pid, &
     &                    5000+pid, MPI_COMM_WORLD, ierr)
            call MPI_SEND(lupdate, nblkrow, MPI_INTEGER, pid, &
     &                    6000+pid, MPI_COMM_WORLD, ierr)
            lxlen(pid) = nblkrow*nk1
            if ((nblkrow .eq. 0) .OR. (nblkrow .gt. LNEL)) then
               err = 1
               write(36,*)'In distidx: nblkrow, LNEL = ',nblkrow, LNEL
            end if

            noupdate = nblkrow
            iUpdateN(pid)=noupdate
            iUpdateIdx(pid,0:noupdate)=lupdate(0:noupdate)
!            call sendcom(pid, lupdate, noupdate, rwork, 
!     &      iwork, cwork)

         END IF
         end do
!
!     Master Processor gets its own local information by just 
!     calling cplidx asking for data for Master processor.
!
         call cplidx(iMaster, part, nel, nblkrow, nblkcol, nblk, &
     &         lupdate, lrpntr, lcpntr, lbpntr, lbindx, lindx, &
     &          rpntr, cpntr, bpntr, bindx, sizes)

         msizes(1) = max(sizes(1), msizes(1))
         msizes(2) = max(sizes(2), msizes(2))
         msizes(3) = max(sizes(3), msizes(3))
         msizes(4) = max(sizes(4), msizes(4))
         msizes(5) = max(sizes(5), msizes(5))
         msizes(6) = max(sizes(6), msizes(6))

         lxlen(iMaster) = nblkrow*nk1
         if (nblkrow .eq. 0) then
            err = 1
         end if

         noupdate = nblkrow
         iUpdateN(iMaster)=noupdate
         iUpdateIdx(iMaster,0:noupdate)=lupdate(0:noupdate)
!         call cpcom(pid, lupdate, noupdate, rwork, iwork, cwork)
!         deallocate(rwork, iwork, cwork)

         if (msizes(1) .gt. ilupdate) then
            write(36,*)'DISTIDX: ilupdate must be at least: ', msizes(1)
            write(36,*)'         ilupdate is currently: ',ilupdate
            err = 11
         else if (msizes(2) .gt. ilrpntr) then
            write(36,*)'DISTIDX: ilrpntr must be at least: ', msizes(2)
            write(36,*)'         ilrpntr is currently: ',ilrpntr
            err = 12
         else if (msizes(3) .gt. ilcpntr) then
            write(36,*)'DISTIDX: ilcpntr must be at least: ', msizes(3)
            write(36,*)'         ilcpntr is currently: ',ilcpntr
            err = 13
         else if (msizes(4) .gt. ilbpntr) then
            write(36,*)'DISTIDX: ilbpntr must be at least: ', msizes(4)
            write(36,*)'         ilbpntr is currently: ',ilbpntr
            err = 14
         else if (msizes(5) .gt. ilbindx) then
            write(36,*)'DISTIDX: ilbindx must be at least: ', msizes(5)
            write(36,*)'         ilbindx is currently: ',ilbindx
            err = 15
         else if (msizes(6) .gt. ilindx) then
            write(36,*)'DISTIDX: ilindx must be at least: ', msizes(6)
            write(36,*)'         ilindx is currently: ',ilindx
            err = 16
         end if
      else

!     Slave Processors receive their data.

         call MPI_RECV(locsiz, 3, MPI_INTEGER, iMaster, &
     &                 myid, MPI_COMM_WORLD, stat, ierr)
         nblkrow = locsiz(1)
         nblkcol = locsiz(2)
         nblk = locsiz(3)
         call MPI_RECV(lrpntr, ilrpntr, MPI_INTEGER, iMaster, &
     &                 1000+myid, MPI_COMM_WORLD, stat, ierr)
         call MPI_RECV(lcpntr, ilcpntr, MPI_INTEGER, iMaster, &
     &                 2000+myid, MPI_COMM_WORLD, stat, ierr)
         call MPI_RECV(lbpntr, ilbpntr, MPI_INTEGER, iMaster, &
     &                 3000+myid, MPI_COMM_WORLD, stat, ierr)
         call MPI_RECV(lbindx, ilbindx, MPI_INTEGER, iMaster, &
     &                 4000+myid, MPI_COMM_WORLD, stat, ierr)
         call MPI_RECV(lindx, ilindx, MPI_INTEGER, iMaster, &
     &                 5000+myid, MPI_COMM_WORLD, stat, ierr)
         call MPI_RECV(lupdate, ilupdate, MPI_INTEGER, iMaster, &
     &                 6000+myid, MPI_COMM_WORLD, stat, ierr)

         maxnoupdate = nblkrow
!         call recvcom(myid, maxnoupdate)

      end if

      return
      end

!RSERWRITECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine rserwrite(what, data, numel)

!     When called by all processors, this routine prints
!     the first "numel" elements of "data" on standard output
!     for all processors in ascending order, including 
!     information about which processor that produces the 
!     output. Processor 0 also prints the string "what" 
!     which typically should include information about 
!     what is printed.
!     This version of the routine is for REAL*8 data.

      IMPLICIT NONE
      include "mpif.h"
      integer nprocs, myid, numel
      real*8 data(*)
      character*6 what

      integer i, dummy, to
      integer ierr, stat(MPI_STATUS_SIZE)

      call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierr)

      if (myid .eq. 0) then
         write(*,99)what
  99     format(6a)
         write(*,*)' '
         write(*,*)'From myid =',myid,': ',(data(i),i=1,numel)
         write(*,*)' '
         call MPI_SEND(dummy, 1, MPI_INTEGER, 1, &
     &                 1, MPI_COMM_WORLD, ierr)
     
         call MPI_RECV(dummy, 1, MPI_INTEGER, nprocs-1, &
     &              0, MPI_COMM_WORLD, stat, ierr)

      else
         call MPI_RECV(dummy, 1, MPI_INTEGER, myid-1,&
     &              myid, MPI_COMM_WORLD, stat, ierr)
         write(*,*)'From myid =',myid,': ',(data(i),i=1,numel)
         write(*,*)' '
         if (myid .eq. nprocs-1) then
            to = 0
         else 
            to = myid + 1
         end if
         call MPI_SEND(dummy, 1, MPI_INTEGER, to, &
     &                 to, MPI_COMM_WORLD, ierr)
     
      end if

      return
      end
     
!CSERWRITECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
     
      subroutine cserwrite(what, data, numel)

!     When called by all processors, this routine prints
!     the first "numel" elements of "data" on standard output
!     for all processors in ascending order, including 
!     information about which processor that produces the 
!     output. Processor 0 also prints the string "what" 
!     which typically should include information about 
!     what is printed.
!     This version of the routine is for character*8 data.

      IMPLICIT NONE
      include "mpif.h"
      integer nprocs, myid, numel
      character*8 data(*)
      character*6 what

      integer i, dummy, to
      integer ierr, stat(MPI_STATUS_SIZE)

      call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierr)

      if (myid .eq. 0) then
         write(*,99)what
  99     format(6a)
         write(*,*)' '
         write(*,*)'From myid =',myid,': ',(data(i),i=1,numel)
         write(*,*)' '
         call MPI_SEND(dummy, 1, MPI_INTEGER, 1, &
     &                 1, MPI_COMM_WORLD, ierr)
     
         call MPI_RECV(dummy, 1, MPI_INTEGER, nprocs-1, &
     &              0, MPI_COMM_WORLD, stat, ierr)

      else
         call MPI_RECV(dummy, 1, MPI_INTEGER, myid-1,&
     &              myid, MPI_COMM_WORLD, stat, ierr)
         write(*,*)'From myid =',myid,': ',(data(i),i=1,numel)
         write(*,*)' '
         if (myid .eq. nprocs-1) then
            to = 0
         else 
            to = myid + 1
         end if
         call MPI_SEND(dummy, 1, MPI_INTEGER, to, &
     &                 to, MPI_COMM_WORLD, ierr)
     
      end if

      return
      end
     

!ISERWRITECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine iserwrite(what, data, numel)

!     When called by all processors, this routine prints
!     for all processors in ascending order, including 
!     information about which processor that produces the 
!     output. Processor 0 also prints the string "what" 
!     which typically should include information about 
!     what is printed.
!     This version of the routine is for INTEGER data.

      IMPLICIT NONE
      include "mpif.h"
      integer nprocs, myid, numel, data(*)
      character*6 what

      integer i, dummy, to
      integer ierr, stat(MPI_STATUS_SIZE)

      call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierr)

      if (myid .eq. 0) then
         write(*,99)what
  99     format(6a)
         write(*,*)' '
         write(*,*)'From myid =',myid,': ',(data(i),i=1,numel)
         write(*,*)' '
         call MPI_SEND(dummy, 1, MPI_INTEGER, 1, &
     &                 1, MPI_COMM_WORLD, ierr)
     
         call MPI_RECV(dummy, 1, MPI_INTEGER, nprocs-1,& 
     &              0, MPI_COMM_WORLD, stat, ierr)

      else
         call MPI_RECV(dummy, 1, MPI_INTEGER, myid-1,&
     &              myid, MPI_COMM_WORLD, stat, ierr)
         write(*,*)'From myid =',myid,': ',(data(i),i=1,numel)
         write(*,*)' '
         if (myid .eq. nprocs-1) then
            to = 0
         else 
            to = myid + 1
         end if
         call MPI_SEND(dummy, 1, MPI_INTEGER, to, &
     &                 to, MPI_COMM_WORLD, ierr)
     
      end if

      return
      end

!EXCHALLEXTCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine exchallext(myid, data_org, update, update_index,rwork, iwork, cwork)             
!     Exchange the external parts of all vectors required.
!
      USE KindData
      USE MADIM
      USE TagN
      USE Grid_Geometry 
      USE Element_Attributes
      USE Solution_Matrix_Arrays
      USE Basic_Parameters
      IMPLICIT NONE

      integer data_org(*), myid,iTem(LNEL),iI
      integer(iK8)::update(*), update_index(*)
      real*8 rwork(*)
      integer(iK8)::iwork(*)
      character*8 cwork(*)

      call cexchexternal(myid, data_org, elem(1:LNEL)%name, cwork, 1, tag(1))
      call iexchexternal(myid, data_org, elem(1:LNEL)%MatNum, iwork, 1, tag(2))
      call rexchexternal(myid, data_org, ElemMedia(1:LNEL,current)%porosity, rwork, 1, tag(3))
      DO iI=1,3
       call rexchexternal(myid, data_org, ElemMedia(1:LNEL,current)%perm(iI), rwork, 1, tag(12+iI))
      END DO
      call rexchexternal(myid, data_org, elem(1:LNEL)%vol, rwork, 1, tag(4))
      call rexchexternal(myid, data_org, elem(1:LNEL)%coord(1),rwork,1,tag(5))
      call rexchexternal(myid, data_org, elem(1:LNEL)%coord(2),rwork,1,tag(6))
      call rexchexternal(myid, data_org, AHT, rwork, 1, tag(7))
      call rexchexternal(myid, data_org, elem(1:LNEL)%coord(3),rwork,1,tag(8))
      call rexchexternal(myid, data_org, X, rwork, NumComPlus1, tag(9))
      call rexchexternal(myid, data_org, elem(1:LNEL)%pm, rwork, 1, tag(10))
      iTem(1:LNEL)=ElemState(1:LNEL)%index(current)
      call iexchexternal(myid, data_org, iTem(1:LNEL), iwork, 1, tag(11))
      ElemState(1:LNEL)%index(current)=iTem(1:LNEL)
      call c1exchexternal(myid, data_org, elem(1:LNEL)%activity, cwork, 1, tag(12))
      return
      end

!REXCHEXTERNALCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine rexchexternal(myid, data_org, data, work, noel, tag)

!     This routine should be called simultaneously by all processors.
!     Then, each processor exchanges the "external" parts of the 
!     vector "data" with all its neighbouring nodes. On input, "data"
!     contains the "update" (=internal+border) parts of the vector.
!     On output, the external elements are appended as specified by the 
!     Aztec format. Note that this routine is supposed to be used also
!     for the initial distribution of vectors that are not further updated
!     during the computations. This is the real*8 version of this routine.
      USE MADIM
      USE UPDATEINFO
      USE Basic_Parameters
      USE General_Control_Parameters
      IMPLICIT NONE
      include "mpif.h"
      include "az_aztecf.h"

      integer data_org(*), noel, tag, myid
      real*8 data(*), work(*)
      integer recvstart, sendstart,i, ierr, len, proc, iw

      recvstart = noel*((data_org(AZ_N_internal+1) +  &
     &                 data_org(AZ_N_border+1))/NumEqu) + 1
      sendstart = AZ_send_list+1
      num_com = 0
      iw = 1

      do i = 1, data_org(AZ_N_neigh+1)
         proc = data_org(AZ_neighbors+(i-1)+1)

!     Receive from processor i in the list
         len = data_org(AZ_rec_length+(i-1)+1)
!     len from dataorg includes neq vector entries per external element
!     therefore, len should be adjusted to mean noel element per ext.
         len = (len*noel)/NumEqu
         num_com = num_com + 1
         call MPI_IRECV(data(recvstart), len, MPI_DOUBLE_PRECISION,&
     &              proc, tag+myid, MPI_COMM_WORLD, req(num_com), ierr)
         recvstart = recvstart + len

!     Send to processor i in the list
         len = data_org(AZ_send_length+(i-1)+1)
!     len from dataorg includes neq vector entries per external element
!     therefore, len should be adjusted to mean noel element per ext.
         call rpack(data, data_org(sendstart), len, work(iw), NumEqu, noel)
         num_com = num_com + 1
         len = (len*noel)/NumEqu
         call MPI_ISEND(work(iw), len, MPI_DOUBLE_PRECISION, &
     &              proc, tag+proc, MPI_COMM_WORLD, req(num_com), ierr)
         iw = iw + len
         sendstart = sendstart + data_org(AZ_send_length+(i-1)+1)
      end do

      if ((recvstart-1) .GT. LNEL*noel) then
         write(36,*)'REXCHEXTERNAL: Insufficient space for externals'
         write(36,*)'Allocated length:',LNEL*noel, &
     &             'Last element received:',recvstart-1
      end if
      if (NumTimeSteps<1) call MPI_WAITALL(num_com, req, stat_no_B, ierr)

!     Note! the work array must be at least of length iw-1.
      return
      end

!IEXCHEXTERNALCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine iexchexternal(myid, data_org, data, work, noel, tag)

!     This routine should be called simulaneously by all processors.
!     Then, each processor exchanges the "external" parts of the 
!     vector "data" with all its neighbouring nodes. On input, "data"
!     contains the "update" (=internal+border) parts of the vector.
!     On output, the external elements are appended as specified by the 
!     Aztec format. Note that this routine is supposed to be used also
!     for the initial distribution of vectors that are not further updated
!     during the computations. This is the character*8 version of this 
!     routine.
      USE MADIM
      USE KindData
      USE Basic_Parameters
      IMPLICIT NONE
      include "mpif.h"
      include "az_aztecf.h"
      integer data_org(*), noel, tag, myid
      integer(iK8):: data(*), work(*)
      integer recvstart, sendstart, num_com, i, ierr, len, proc, iw
      integer req(2*MAXPROCS), stat(MPI_STATUS_SIZE, 2*MAXPROCS)

      recvstart = noel*((data_org(AZ_N_internal+1) + &
     &                 data_org(AZ_N_border+1))/NumEqu) + 1
      sendstart = AZ_send_list+1
      num_com = 0
      iw = 1

      do i = 1, data_org(AZ_N_neigh+1)
         proc = data_org(AZ_neighbors+(i-1)+1)

!     Receive from processor i in the list
         len = data_org(AZ_rec_length+(i-1)+1)
!     len from dataorg includes neq vector entries per external element
!     therefore, len should be adjusted to mean noel element per ext.
         len = (len*noel)/NumEqu
         num_com = num_com + 1
         call MPI_IRECV(data(recvstart), len, MPI_INTEGER, &
     &              proc, tag+myid, MPI_COMM_WORLD, req(num_com), ierr)
         recvstart = recvstart + len
!     Send to processor i in the list
         len = data_org(AZ_send_length+(i-1)+1)
         call ipack(data, data_org(sendstart), len, work(iw), NumEqu, noel)
         num_com = num_com + 1
         len = (len*noel)/NumEqu
         call MPI_ISEND(work(iw), len, MPI_INTEGER,&
     &              proc, tag+proc, MPI_COMM_WORLD, req(num_com), ierr)
         iw = iw + len
         sendstart = sendstart + data_org(AZ_send_length+(i-1)+1)
      end do

      if ((recvstart-1) .GT. LNEL*noel) then
         write(36,*)'IEXCHEXTERNAL: Insufficient space for externals'
         write(36,*)'Allocated length:',LNEL*noel, &
     &             'Last element received:',recvstart-1
      end if
      call MPI_WAITALL(num_com, req, stat, ierr)

      return
      end

!CEXCHEXTERNALCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine cexchexternal(myid, data_org, data, work, noel, tag)

!     This routine should be called simulaneously by all processors.
!     Then, each processor exchanges the "external" parts of the 
!     vector "data" with all its neighbouring nodes. On input, "data"
!     contains the "update" (=internal+border) parts of the vector.
!     On output, the external elements are appended as specified by the 
!     Aztec format. Note that this routine is supposed to be used also
!     for the initial distribution of vectors that are not further updated
!     during the computations. This is the character*8 version of this 
!     routine.
      USE MADIM
      USE Basic_Parameters    
      IMPLICIT NONE
      include "mpif.h"
      include "az_aztecf.h"
      integer data_org(*), noel, tag, myid
      character*8 data(*), work(*)
      integer recvstart, sendstart, num_com, i, ierr, len, proc, iw
      integer req(2*MAXPROCS), stat(MPI_STATUS_SIZE, 2*MAXPROCS)

      recvstart = noel*((data_org(AZ_N_internal+1) + &
     &                 data_org(AZ_N_border+1))/NumEqu) + 1
      sendstart = AZ_send_list+1
      num_com = 0
      iw = 1

      do i = 1, data_org(AZ_N_neigh+1)
         proc = data_org(AZ_neighbors+(i-1)+1)

!     Receive from processor i in the list
         len = data_org(AZ_rec_length+(i-1)+1)
!     len from dataorg includes neq vector entries per external element
!     therefore, len should be adjusted to mean noel element per ext.
         len = (len*noel)/NumEqu
         num_com = num_com + 1
         call MPI_IRECV(data(recvstart), len*8, MPI_CHARACTER, &
     &              proc, tag+myid, MPI_COMM_WORLD, req(num_com), ierr)
         recvstart = recvstart + len
!     Send to processor i in the list
         len = data_org(AZ_send_length+(i-1)+1)
         call c8pack(data, data_org(sendstart),len,work(iw),NumEqu,noel)
         num_com = num_com + 1
         len = (len*noel)/NumEqu
         call MPI_ISEND(work(iw), len*8, MPI_CHARACTER, &
     &              proc, tag+proc, MPI_COMM_WORLD, req(num_com), ierr)
         iw = iw + len
         sendstart = sendstart + data_org(AZ_send_length+(i-1)+1)
      end do

      if ((recvstart-1) .GT. LNEL*noel) then
         write(36,*)'CEXCHEXTERNAL: Insufficient space for externals'
         write(36,*)'Allocated length:',LNEL*noel, &
     &             'Last element received:',recvstart-1
      end if
      call MPI_WAITALL(num_com, req, stat, ierr)

      return
      end

!C1HEXTERNALCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine c1exchexternal(myid, data_org, data, work, noel, tag)
 
!     This routine should be called simulaneously by all processors.
!     Then, each processor exchanges the "external" parts of the
!     vector "data" with all its neighbouring nodes. On input, "data"
!     contains the "update" (=internal+border) parts of the vector.
!     On output, the external elements are appended as specified by the
!     Aztec format. Note that this routine is supposed to be used also
!     for the initial distribution of vectors that are not further updated
!     during the computations. This is the character*1 version of this
!     routine.
      USE MADIM
      USE Basic_Parameters
      IMPLICIT NONE
      include "mpif.h"
      include "az_aztecf.h"
      integer data_org(*), noel, tag, myid
      character*8 work(*)
      character*1 data(*)
      integer recvstart, sendstart, num_com, i, ierr, len, proc, iw
      integer req(2*MAXPROCS), stat(MPI_STATUS_SIZE, 2*MAXPROCS)
 
      recvstart = noel*((data_org(AZ_N_internal+1) + &
     &                 data_org(AZ_N_border+1))/NumEqu) + 1
      sendstart = AZ_send_list+1
      num_com = 0
      iw = 1
 
      do i = 1, data_org(AZ_N_neigh+1)
         proc = data_org(AZ_neighbors+(i-1)+1)
 
!     Receive from processor i in the list
         len = data_org(AZ_rec_length+(i-1)+1)
!     len from dataorg includes neq vector entries per external element
!     therefore, len should be adjusted to mean noel element per ext.
         len = (len*noel)/NumEqu
         num_com = num_com + 1
         call MPI_IRECV(data(recvstart), len*1, MPI_CHARACTER, &
     &              proc, tag+myid, MPI_COMM_WORLD, req(num_com), ierr)
         recvstart = recvstart + len
!     Send to processor i in the list
         len = data_org(AZ_send_length+(i-1)+1)
         call c1pack(data, data_org(sendstart),len,work(iw)(1:1),NumEqu,noel)
         num_com = num_com + 1
         len = (len*noel)/NumEqu
         call MPI_ISEND(work(iw)(1:1), len*1, MPI_CHARACTER, &
     &              proc, tag+proc, MPI_COMM_WORLD, req(num_com), ierr)
         iw = iw + len
         sendstart = sendstart + data_org(AZ_send_length+(i-1)+1)
      end do
 
      if ((recvstart-1) .GT. LNEL*noel) then
         write(36,*)'CEXCHEXTERNAL: Insufficient space for externals'
         write(36,*)'Allocated length:',LNEL*noel, &
     &             'Last element received:',recvstart-1
      end if
      call MPI_WAITALL(num_com, req, stat, ierr)
 
      return
      end

!RPACKCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine rpack(invec, index, len, outvec, neq, noel)

!     Copies the elements of invec (specified by the vector index)
!     into the vector outvec. Every neq element of the vector index is
!     used, and noel elements are copied for each of them.
!     This is the real*8 version of this routine.

      IMPLICIT NONE
      integer index(*), len, neq, noel
      real*8 invec(*), outvec(*)

!     Local variables
      integer i, k

      do i = 1, len, neq
         do k = 1, noel
            outvec(((i-1)/neq)*noel+k) = invec((index(i)/neq)*noel+k)
         end do
      end do 

      return
      end


!IPACKCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine ipack(invec, index, len, outvec, neq, noel)

!     Copies the elements of invec (specified by the vector index)
!     into the vector outvec. Every neq element of the vector index is
!     used, and noel elements are copied for each of them.
!     This is the integer version of this routine.
      USE KindData
      IMPLICIT NONE
!     Input/output variables
      integer index(*), len, neq, noel
      integer(iK8)::invec(*), outvec(*)

!     Local variables
      integer i, k

      do i = 1, len, neq
         do k = 1, noel
            outvec(((i-1)/neq)*noel+k) = invec((index(i)/neq)*noel+k)
         end do
      end do 

      return
      end


!C8PACKCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine c8pack(invec, index, len, outvec, neq, noel)

!     Copies the elements of invec (specified by the vector index)
!     into the vector outvec. Every neq element of the vector index is
!     used, and noel elements are copied for each of them.
!     This is the character*8 version of this routine.

      IMPLICIT NONE
!     Input/output variables
      integer index(*), len, neq, noel
      character*8 invec(*), outvec(*)

!     Local variables
      integer i, k

      do i = 1, len, neq
         do k = 1, noel
            outvec(((i-1)/neq)*noel+k) = invec((index(i)/neq)*noel+k)
         end do
      end do 

      return
      end

!C1PACKCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine c1pack(invec, index, len, outvec, neq, noel)
 
!     Copies the elements of invec (specified by the vector index)
!     into the vector outvec. Every neq element of the vector index is
!     used, and noel elements are copied for each of them.
!     This is the character*1 version of this routine.
 
      IMPLICIT NONE
!     Input/output variables
      integer index(*), len, neq, noel
      character*1 invec(*), outvec(*)
 
!     Local variables
      integer i, k
 
      do i = 1, len, neq
         do k = 1, noel
            outvec(((i-1)/neq)*noel+k) = invec((index(i)/neq)*noel+k)
         end do
      end do
 
      return
      end

!RSENDINDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine rsendind(data, noel, noindex, pid, index, tag, work)

!     Sends the elements of the vector "data" that are specified by
!     the index vector "index" to processor "pid".
!     "noel" is the number of vector elements that should be sent for 
!     each element in "index".
!     Work must be at least of length noel*noindex
!     This is the real*8 version of this routine.
      USE MADIM
      USE KindData
      IMPLICIT NONE
      include "mpif.h"
      
      integer noindex, noel, tag, pid
      integer(iK8):: index(*)  
      integer i, j, ierr, len
      real*8 data(*), work(*)

      do i = 1, noindex
         do j = 1, noel
            work((i-1)*noel+j) = data(noel*index(i)+j)
         end do 
      end do 
      len = noindex*noel
      call MPI_SEND(work, len, MPI_DOUBLE_PRECISION, pid, &
     &                 tag, MPI_COMM_WORLD, ierr)
      return
      end

!ISENDINDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine isendind(data, noel, noindex, pid, index, tag, work)

!     Sends the elements of the vector "data" that are specified by
!     the index vector "index" to processor "pid".
!     "noel" is the number of vector elements that should be sent for 
!     each element in "index".
!     Work must be at least of length noel*noindex
!     This is the real*8 version of this routine.
      USE MADIM
      USE KindData
      IMPLICIT NONE
      include "mpif.h"
      integer noindex, noel, tag, pid
      integer i, j, ierr, len
      integer(iK8)::index(*)
      integer(iK8):: data(*), work(*)

      do i = 1, noindex
         do j = 1, noel
            work((i-1)*noel+j) = data(noel*index(i)+j)
         end do 
      end do 
      len = noindex*noel
      call MPI_SEND(work, len, MPI_INTEGER, pid, &
     &                 tag, MPI_COMM_WORLD, ierr)
      return
      end

!CSENDINDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine csendind(data, noel, noindex, pid, index, tag, work)

!     Sends the elements of the vector "data" that are specified by
!     the index vector "index" to processor "pid".
!     "noel" is the number of vector elements that should be sent for 
!     each element in "index".
!     Work must be at least of length noel*noindex
!     This is the character*8 version of this routine.
      USE MADIM
      USE KindData
      IMPLICIT NONE
      include "mpif.h"
      integer noindex, noel, tag, pid
      integer(iK8):: index(*)
      integer:: i, j, ierr, len
      character*8 data(*), work(*)

      do i = 1, noindex
         do j = 1, noel
            work((i-1)*noel+j) = data(noel*index(i)+j)
         end do 
      end do 
      len = noindex*noel
      call MPI_SEND(work, 8*len, MPI_CHARACTER, pid, & 
     &                 tag, MPI_COMM_WORLD, ierr)
      return
      end

!C1SENDINDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine c1sendind(data, noel, noindex, pid, index, tag, work)
 
!     Sends the elements of the vector "data" that are specified by
!     the index vector "index" to processor "pid".
!     "noel" is the number of vector elements that should be sent for
!     each element in "index".
!     Work must be at least of length noel*noindex
!     This is the character*8 version of this routine.
      USE MADIM
      USE KindData
      IMPLICIT NONE
      include "mpif.h"
      integer noindex, noel, tag, pid
      integer(iK8):: index(*)
      integer:: i, j, ierr, len
      character*1 data(*), work(*)
 
      do i = 1, noindex
         do j = 1, noel
            work((i-1)*noel+j) = data(noel*index(i)+j)
         end do
      end do
      len = noindex*noel
      call MPI_SEND(work, len, MPI_CHARACTER, pid, &
     &                 tag, MPI_COMM_WORLD, ierr)
      return
      end

!RRECVINDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine rrecvind(data, noel, maxlen, tag)
!     Receives the part of the vector "data" that is specified by the
!     vector index
!     This is the real*8 version of this routine.
      USE MADIM
      IMPLICIT NONE
      include "mpif.h"
      integer maxlen, noel, tag
      integer i, ierr, len, stat(MPI_STATUS_SIZE)
      real*8 data(*)

      len = maxlen*noel

      call MPI_RECV(data, len, MPI_DOUBLE_PRECISION, 0, tag, MPI_COMM_WORLD, stat, ierr)
      return
      end


!IRECVINDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine irecvind(data, noel, maxlen, tag)
!     Receives the part of the vector "data" that is specified by the
!     vector index
!     This is the integer version of this routine.
      USE MADIM
      USE KindData
      IMPLICIT NONE
      include "mpif.h"
      integer maxlen, noel, tag
      integer i, ierr, len, stat(MPI_STATUS_SIZE)
      integer(iK8):: data(*)

      len = maxlen*noel

      call MPI_RECV(data, len, MPI_INTEGER, 0, tag, MPI_COMM_WORLD, stat, ierr)
      return
      end


!CRECVINDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine crecvind(data, noel, maxlen, tag)
!     Receives the part of the vector "data" that is specified by the
!     vector index
!     This is the character*8 version of this routine.
      USE MADIM
      IMPLICIT NONE
      include "mpif.h"
      integer maxlen, noel, tag
      integer i, ierr, len, stat(MPI_STATUS_SIZE)
      character*8 data(*)

      len = maxlen*noel

      call MPI_RECV(data, 8*len, MPI_CHARACTER, 0, tag, MPI_COMM_WORLD, stat, ierr)
      return
      end

!C1RECVINDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine c1recvind(data, noel, maxlen, tag)
!     Receives the part of the vector "data" that is specified by the
!     vector index
!     This is the character*1 version of this routine.
      USE MADIM
      IMPLICIT NONE
      include "mpif.h"
      integer maxlen, noel, tag
      integer i, ierr, len, stat(MPI_STATUS_SIZE)
      character*1 data(*)
 
      len = maxlen*noel
 
      call MPI_RECV(data, len, MPI_CHARACTER, 0, tag, MPI_COMM_WORLD, stat, ierr)
      return
      end

!LOCNOGNCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine locnogn(update, update_index, noupdate, rwork, iwork, cwork)                

!     This routine transforms all vectors relating to sinks and 
!     sources into the local numbering. (NumSS is the length of 
!     these vectors.)
      USE MADIM
      USE Basic_Parameters
      USE Sources_and_Sinks    
      USE temp_arrays  
      USE MPICOM
      IMPLICIT NONE
      integer update(*), update_index(*), noupdate, iwork(*)
      real*8 rwork(*)
      character*8 cwork(*)
      character*1 cwork1(Max_NumSS)
      character*5 cwork5(Max_NumSS)
      character*4 cwork4(Max_NumSS)
      real*8 tn(Max_NumSS,1000),rn(Max_NumSS,1000),en(Max_NumSS,1000)  
   
      
      integer i, j, k, iJ,iI

      ALLOCATE(nexgind(Max_NumSS))

!   The following dummy 9 lines are for preventing error in late rmemory allocation and deallocation at iMaster 
      IF (myid==iMaster) THEN
       DO iI=1,NumSS
         j = ABS(SS(iI)%Table_Length)
         IF(j <= 1 .OR. SS(iI)%type(1:4) == 'DELV') THEN
           ALLOCATE(SS(iI)%TimeColumn(1),SS(iI)%RateColumn(1), STAT = i) 
           IF(SS(iI)%type(5:5) == ' ') allocate(SS(iI)%EnthColumn(1))
         END IF
       END DO
      END IF
!    
      k = 0
      do i = 1, NumSS
         do j = 1, noupdate
            if (SS(i)%ElemNum .eq. update(j)+1) then
               k = k + 1
               nexgind(k) = i-1
               iwork(k) = update_index(j)+1
            end if
         end do
      end do

      NumSS= k
      if (NumSS .eq. 0) return
      SS(1:NumSS)%ElemNum =iwork(1:NumSS)
!
      call c8indtransform(SS(1:Max_NumSS)%ElemName, 1, NEXGIND, NumSS, cwork, 'R')
      call c5indtransform(SS(1:Max_NumSS)%name, 1, NEXGIND, NumSS, cwork5, 'R')
      call c5indtransform(SS(1:Max_NumSS)%type, 1, NEXGIND, NumSS, cwork5, 'R')
      call c5indtransform(SS(1:Max_NumSS)%DelivFileName, 1, NEXGIND, NumSS, cwork5, 'R')
      call c1indtransform(SS(1:Max_NumSS)%Rate_vs_time, 1, NEXGIND, NumSS, cwork1, 'R')
      call c4indtransform(SS(1:Max_NumSS)%RateType, 1, NEXGIND, NumSS, cwork4, 'R')
      call c4indtransform(SS(1:Max_NumSS)%RateResponse, 1, NEXGIND, NumSS, cwork4, 'R')
      call rindtransform(SS(1:Max_NumSS)%Rate, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%TempRate, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%enth, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%ProdIndx, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%BtmHoleP, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%LayerZ, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%grad, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%PresLimit, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%RateStepChange, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%RateFactor, 1, NEXGIND, NumSS, rwork, 'R')
      call rindtransform(SS(1:Max_NumSS)%RateLimit, 1, NEXGIND, NumSS, rwork, 'R')
!
      call iindtransform(SS(1:Max_NumSS)%TypeIndx, 1, NEXGIND, NumSS, iwork, 'R')
      call iindtransform(SS(1:Max_NumSS)%Table_Length, 1, NEXGIND, NumSS, iwork, 'R')
      call iindtransform(SS(1:Max_NumSS)%NumEquTypes, 1, NEXGIND, NumSS, iwork, 'R')
      k=MaxVal(SS(1:Max_NumSS)%Table_Length)
      if(k>999) write(*,*) 'The array size of tn,rn,en in locnogn is too small'
       DO iI=1,NumSS
        IF (SS(iI)%Rate_vs_time=='T') THEN
         iJ=ABS(SS(iI)%Table_Length)
         IF(iJ==0) iJ=1 
         tn(iI,1:iJ)=SS(NEXGIND(iI)+1)%TimeColumn(1:iJ)
         rn(iI,1:iJ)=SS(NEXGIND(iI)+1)%RateColumn(1:iJ)
         IF(SS(iI)%type(5:5) /= ' ') en(iI,1:iJ)=SS(NEXGIND(iI)+1)%EnthColumn(1:iJ)
        END IF
       END DO
        IF (myid==iMaster) THEN
         DO iI=1, Max_NumSS
         deallocate(SS(iI)%TimeColumn)
         deallocate(SS(iI)%RateColumn)
         deallocate(SS(iI)%EnthColumn)
         END DO
        END IF
       DO iI=1,NumSS
        IF (SS(iI)%Rate_vs_time=='T') THEN
         iJ=ABS(SS(iI)%Table_Length)
         IF(iJ==0) iJ=1
         ALLOCATE(SS(iI)%TimeColumn(iJ),SS(iI)%RateColumn(iJ), STAT = i)        
         IF(SS(iI)%type(5:5) /= ' ') ALLOCATE(SS(iI)%EnthColumn(iJ), STAT = i)
         SS(iI)%TimeColumn(1:iJ)=tn(iI,1:iJ)
         SS(iI)%RateColumn(1:iJ)=rn(iI,1:iJ)
         IF(SS(iI)%type(5:5) /= ' ') SS(iI)%EnthColumn(1:iJ)=en(iI,1:iJ)
        END IF
       END DO
       call iindtransform(WDel(1:Max_NumSS)%idTab, 1, NEXGIND, NumSS, iwork, 'R')
      return
      end

!RECVCOMCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine recvcom(myid, maxnoupdate)
!     Receives the appropriate parts of all vectors requested from
!     processor 0.
      USE MADIM
      USE TagN
      USE Basic_Parameters
      USE Element_Attributes
      USE Grid_Geometry
      USE Solution_Matrix_Arrays, ONLY: X
      USE Connection_Attributes, ONLY: emissivity
      USE Basic_Parameters

!
!$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
!
      
      IMPLICIT NONE
      include "mpif.h"
      integer maxnoupdate, myid
      integer i, ierr, len,iTem(maxnoupdate)

      call rrecvind(X, NumComPlus1, maxnoupdate, tag(1)+myid)
      call rrecvind(ElemMedia(:,current)%porosity, 1, maxnoupdate, tag(2)+myid)
      DO i=1,3
       call rrecvind(ElemMedia(:,current)%perm(i), 1, maxnoupdate, tag(23+i)+myid)
      END DO

      call crecvind(ELEM(:)%name, 1, maxnoupdate, tag(3)+myid)
      call irecvind(ELEM(:)%MatNum, 1, maxnoupdate, tag(4)+myid)
      call irecvind(iTem, 1, maxnoupdate, tag(5)+myid)
      ElemState(1:maxnoupdate)%index(current)=iTem(1:maxnoupdate)
!
      call rrecvind(ELEM(:)%coord(1), 1, maxnoupdate, tag(6)+myid)
      call rrecvind(ELEM(:)%coord(2), 1, maxnoupdate, tag(7)+myid)
      call rrecvind(ELEM(:)%coord(3), 1, maxnoupdate, tag(8)+myid)
      call rrecvind(ELEM(:)%vol, 1, maxnoupdate, tag(9)+myid)
      call rrecvind(ELEM(:)%pm, 1, maxnoupdate, tag(10)+myid)
      call rrecvind(aht, 1, maxnoupdate, tag(11)+myid)
      call c1recvind(ELEM(:)%activity, 1, maxnoupdate, tag(12)+myid)
      return
      end

!GETLNEXCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine getlnex(adj,xadj,lnex1, lnex2, update, &
     &        update_index, N_update, external, &
     &        extern_index, N_external, ncon, lncon, nexind)
      USE KindData     
      USE temp_arrays
      IMPLICIT NONE
      integer  N_update, N_external
      integer i, j, k, c,j1,ncon,lncon,n1,n2,m,Ix,iI
      integer(iK8)::update(N_update), external(*)
      integer(iK8)::update_index(*), extern_index(*)
      integer(iK8)::lnex1(*), lnex2(*), nexind(*)
      integer(iK4)::adj(*),xadj(*)
      logical found1, found2
      integer(1),allocatable,dimension(:)::t1
      integer(iK4),allocatable,dimension(:)::indx1,indx2 

!     On output, lnex1 and lnex2 holds the local indices for the
!     connections relevant for this processor. nexind holds the 
!     indices to the coresponding connections in the global vectors
!     nex1 and nex2.

      ALLOCATE(t1(NCON),STAT=iI) 
      ALLOCATE(indx1(N_update),STAT=iI)
      ALLOCATE(indx2(N_external),STAT=iI)

      t1=0 
      do k=1,N_update
       indx1(k)=k
      end do
      do k=1,N_external
       indx2(k)=k
      end do 
      CALL QCKSRTONEKEY_i(N_update,update,indx1) 
      CALL QCKSRTONEKEY_i(N_external,external,indx2)

      c = 0
      do k=1,N_update
       i=update(k)+1
       n1=xadj(i)
       n2=xadj(i+1)-1
       do j=n1,n2
        m=abs(adj(j))

        if (t1(m)<1) then

         c=c+1
         if (adj(j)<0) then
          Ix=NEX2(-adj(j))-1
         else
          Ix=NEX1(adj(j))-1
         end if
         j1=0
         found1 = .FALSE.

         CALL BSEARCH_i(N_update, update, indx1,Ix,j1)
         if (j1>0) found1 = .TRUE.

         found2=.TRUE.
        if (found1) then
         if (adj(j)<0) then
           lnex2(c)=update_index(j1)+1
           lnex1(c)=update_index(k)+1
         else
          lnex1(c)=update_index(j1)+1
          lnex2(c)=update_index(k)+1
         end if
       else
         j1=0
         found2 = .FALSE.
         CALL BSEARCH_i(N_external,external,indx2,Ix,j1)
         if (j1>0) found2 = .TRUE.

         if (found2) then
          if (adj(j)<0) then
           lnex2(c)=extern_index(j1)+1
           lnex1(c)=update_index(k)+1
          else
           lnex1(c)=extern_index(j1)+1
           lnex2(c)=update_index(k)+1
          end if
        end if  ! found2
       end if    ! found1   

      nexind(c) = m
      t1(m)=2

      if (.NOT. found2) then
         write(36,*)'Trouble finding local connections'
         write(36,*)'One node is found but not its neighbor '
         write(36,*)'Nex1 Node found, ind:',nex1(i), i
         write(36,*)' '
      end if

      end if    ! t1


      end do     ! j
      end do     ! k

!     lncon is the number of connection on this processor, 
!     including connections to external elements.
      lncon = c
      deallocate(t1,indx1,indx2) 
      return
      end
         
!RECVALLCONCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine recvallcon(myid, len)
      USE MADIM
      USE Grid_Geometry
      USE Connection_Attributes
      USE TagN
      IMPLICIT NONE
      integer len
      integer myid, i, j


      call rrecvind(conx(:)%d1, 1, len, tag(15)+myid)
      call rrecvind(conx(:)%d2, 1, len, tag(16)+myid)
      call rrecvind(conx(:)%area, 1, len, tag(17)+myid) 
      call rrecvind(conx(:)%beta, 1, len, tag(18)+myid)
      call rrecvind(emissivity, 1, len, tag(19)+myid)
      call irecvind(conx(:)%ki, 1, len, tag(20)+myid)
      call crecvind(conx(:)%name1, 1, len, tag(21)+myid)
      call crecvind(conx(:)%name2, 1, len, tag(22)+myid)

      return
      end

!RINDTRANSFORMCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine rindtransform(data, noel, index, noindex, work, LR)
      USE KindData
      IMPLICIT NONE
      integer(iK8):: index(*)
      integer:: noel, noindex
      real*8 data(*), work(*)
      character LR

!     Transforms the vector data so that on output element data(i)
!     contains the noel elements specified by index(i)
!     This is the real*8 version of this routine
    
      integer i, j, len

      if (LR .eq. 'R') then
         do i = 1, noindex
            do j = 1, noel
               work((i-1)*noel+j) = data(noel*index(i)+j)
            end do 
         end do 
      elseif (LR .eq. 'L') then
         do i = 1, noindex
            do j = 1, noel
               work(noel*index(i)+j) = data((i-1)*noel+j)
            end do 
         end do 
      else
         write(36,*)'rindtransform: Illegal value for parameter LR'
      end if

      len = noindex*noel
      call dcopy(len, work, 1, data, 1)
      return
      end

!IINDTRANSFORMCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine iindtransform(data, noel, index, noindex, work, LR)
      USE KindData
      IMPLICIT NONE
      integer  noel, noindex
      integer(iK8)::index(*), data(*), work(*)
      character LR

!     Transforms the vector data so that on output element data(i)
!     contains the noel elements specified by index(i)
!     This is the integer version of this routine
    
      integer i, j, len

      if (LR .eq. 'R') then
         do i = 1, noindex
            do j = 1, noel
               work((i-1)*noel+j) = data(noel*index(i)+j)
            end do 
         end do 
      elseif (LR .eq. 'L') then
         do i = 1, noindex
            do j = 1, noel
               work(noel*index(i)+j) = data((i-1)*noel+j)
            end do 
         end do 
      else
         write(36,*)'iindtransform: Illegal value for parameter LR'
      end if

      len = noindex*noel
      data(1:len)=work(1:len)
      return
      end

!C8INDTRANSFORMCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine c8indtransform(data, noel, index, noindex, work, LR)
      USE KindData
      IMPLICIT NONE
      integer index(*), noel, noindex
      character*8 data(*), work(*)
      character LR

!     Transforms the vector data so that on output element data(i)
!     contains the noel elements specified by index(i)
!     This is the character*8 version of this routine
    
      integer i, j, len

      if (LR .eq. 'R') then
         do i = 1, noindex
            do j = 1, noel
               work((i-1)*noel+j) = data(noel*index(i)+j)
            end do 
         end do 
      elseif (LR .eq. 'L') then
         do i = 1, noindex
            do j = 1, noel
               work(noel*index(i)+j) = data((i-1)*noel+j)
            end do 
         end do 
      else
         write(36,*)'c8indtransform: Illegal value for parameter LR'
      end if

      len = noindex*noel
      do i = 1, len
         data(i) = work(i)
      end do
      return
      end

!C5INDTRANSFORMCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine c5indtransform(data, noel, index, noindex, work, LR)
      USE KindData
      IMPLICIT NONE
      integer index(*)
      integer noel, noindex
      character*5 data(*), work(*)
      character LR

!     Transforms the vector data so that on output element data(i)
!     contains the noel elements specified by index(i)
!     This is the character*5 version of this routine
    
      integer i, j, len

      if (LR .eq. 'R') then
         do i = 1, noindex
            do j = 1, noel
               work((i-1)*noel+j) = data(noel*index(i)+j)
            end do 
         end do 
      elseif (LR .eq. 'L') then
         do i = 1, noindex
            do j = 1, noel
               work(noel*index(i)+j) = data((i-1)*noel+j)
            end do 
         end do 
      else
         write(36,*)'c5indtransform: Illegal value for parameter LR'
      end if

      len = noindex*noel
      do i = 1, len
         data(i) = work(i)
      end do
      return
      end

!C4INDTRANSFORMCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine c4indtransform(data, noel, index, noindex, work, LR)
      USE KindData
      IMPLICIT NONE
      integer index(*)
      integer noel, noindex
      character*4 data(*), work(*)
      character LR
 
!     Transforms the vector data so that on output element data(i)
!     contains the noel elements specified by index(i)
!     This is the character*4 version of this routine
 
      integer i, j, len
 
      if (LR .eq. 'R') then
         do i = 1, noindex
            do j = 1, noel
               work((i-1)*noel+j) = data(noel*index(i)+j)
            end do
         end do
      elseif (LR .eq. 'L') then
         do i = 1, noindex
            do j = 1, noel
               work(noel*index(i)+j) = data((i-1)*noel+j)
            end do
         end do
      else
         write(36,*)'c4indtransform: Illegal value for parameter LR'
      end if
 
      len = noindex*noel
      do i = 1, len
         data(i) = work(i)
      end do
      return
      end

!C1INDTRANSFORMCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
 
      subroutine c1indtransform(data, noel, index, noindex, work, LR)
      USE KindData
      IMPLICIT NONE
      integer(iK8):: index(*)
      integer noel, noindex
      character*1 data(*), work(*)
      character LR

!     Transforms the vector data so that on output element data(i)
!     contains the noel elements specified by index(i)
!     This is the character*1 version of this routine
    
      integer i, j, len

      if (LR .eq. 'R') then
         do i = 1, noindex
            do j = 1, noel
               work((i-1)*noel+j) = data(noel*index(i)+j)
            end do 
         end do 
      elseif (LR .eq. 'L') then
         do i = 1, noindex
            do j = 1, noel
               work(noel*index(i)+j) = data((i-1)*noel+j)
            end do 
         end do 
      else
         write(36,*)'c1indtransform: Illegal value for parameter LR'
      end if

      len = noindex*noel
      do i = 1, len
         data(i) = work(i)
      end do
      return
      end

!TRANSCOMCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine transcom(index, len, rwork, iwork, cwork)
      USE MADIM
      USE Grid_Geometry 
      USE Element_Attributes
      USE Solution_Matrix_Arrays
      USE Basic_Parameters
      USE General_External_File_Units
      IMPLICIT NONE

      integer index(*), len,iI
      real*8 rwork(*)
      integer iwork(*)
      character*8 cwork(*)
      character*1 cwork1(LNEL) 
      integer iTem(LNEL)

!     Transforms the "update" part of the vectors in common
!     blocks that were received in recvcom so that the new
!     order follows the order specified by index.
!     (In order to get the local ordering for all vectors
!     on all processors.)

      call c8indtransform(elem(1:LNEL)%name, 1, index, len, cwork, 'L')
      call iindtransform(elem(1:LNEL)%MatNum, 1, index, len, iwork, 'L')
      call rindtransform(elem(1:LNEL)%vol, 1, index, len, rwork, 'L')
      call rindtransform(ElemMedia(1:LNEL,current)%porosity, 1, index, len, rwork, 'L')
      DO iI=1,3
       call rindtransform(ElemMedia(1:LNEL,current)%perm(iI), 1, index, len, rwork, 'L')
      END DO
      call rindtransform(elem(1:LNEL)%coord(1), 1, index, len, rwork, 'L')
      call rindtransform(elem(1:LNEL)%coord(2), 1, index, len, rwork, 'L')
      call rindtransform(elem(1:LNEL)%coord(3), 1, index, len, rwork, 'L')
      call rindtransform(AHT, 1, index, len, rwork, 'L')
      call rindtransform(X, NumComPlus1, index, len, rwork, 'L')
      iTem(1:LNEL)=ElemState(1:LNEL)%index(current)
      call iindtransform(iTem, 1, index, len, iwork, 'L')
      ElemState(1:LNEL)%index(current)=iTem(1:LNEL)
      call c1indtransform(elem(1:LNEL)%activity, 1, index, len, cwork1, 'L')
      call rindtransform(elem(1:LNEL)%pm, 1, index, len, rwork, 'L')
      return
      end

!ALLREPLICOMCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine allreplicom
      
!     When called by all processors, smaller matrices, scalars, and
!     the vectors NEX1 and NEX2 in common blocks are fully replicated 
!     on all processors by broadcasts from proc 0.

      USE MADIM
      USE MPICOM
      USE temp_arrays
      USE Basic_Parameters
      USE EOS_Parameters
      USE General_Control_Parameters
      USE Time_Series_Parameters 
      USE Diffusion_Parameters
      USE SELEC_Param
      USE Geologic_Media_Properties
      USE Diffusion_Parameters
      USE Sources_and_Sinks
      USE Real_Gas_Property_Database
      USE Hydrate_Parameters
      USE Boundary_Conditions
      USE Solver_Parameters
      USE General_External_File_Units
      USE Real_Gas_Properties
      USE Subdomain_Definitions

      IMPLICIT NONE

      include "mpif.h"
      integer::iI,iJ,iK,ier,nG,nH

!---------------------------------------------
      call MPI_BCAST(RestartSimulation, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(variable_porosity, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
!kz      call MPI_BCAST(variable_permeability, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(RadiativeHeat, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Flag_SaturationBasedTortuosity, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NumObsElem, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(ObsElem_Flag, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(ObsElem%name, 8*100, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NumObsSS, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(ObsSS_Flag, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(ObsSS%name, 5*100, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NumObsConx, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(ObsConx_Flag, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(ObsConx%name, 16*100, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(diffusivity, Max_NumMobPhases*Max_NumMassComp, MPI_DOUBLE_PRECISION, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(FE, 512, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(IE, 16, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%DensG, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Poros, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Perm(1), Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Perm(2), Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Perm(3), Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%KThrW, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%SpcHt, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Compr, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Expan, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%KThrD, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Tortu, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Klink, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%CritSat, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%PermExpon, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%beta, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%gama, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%HiComp, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%LoComp, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%SatAtHiComp, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%SatAtLoComp, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%DeltaSat, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%RelPermEquationNum,Max_NumMedia, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%PcapEquationNum,Max_NumMedia, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%PhiPolyOrder,Max_NumMedia, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      DO iI=1,7
       call MPI_BCAST(media(1:Max_NumMedia)%RelPermParam(iI), Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD,ierr)
       call MPI_BCAST(media(1:Max_NumMedia)%PcapParam(iI), Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
       call MPI_BCAST(media(1:Max_NumMedia)%PhiCoeff(iI-1), Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr) 
      END DO

      call MPI_BCAST(media(1:Max_NumMedia)%Sw_irr, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Sg_irr, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%So_irr, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(media(1:Max_NumMedia)%Pc_max, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(PoMed(1:Max_NumMedia)%RGrain, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(PoMed(1:Max_NumMedia)%SArea, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(PoMed(1:Max_NumMedia)%NVoid, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(PoMed(1:Max_NumMedia)%VVoid, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(PoMed(1:Max_NumMedia)%RVoid, Max_NumMedia, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Max_NumNRIterations, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_OutputAmount, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Max_NumTimeSteps, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(output_frequency, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SAVE_frequency, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(TimeSeriesPrint_frequency, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(CPU_MaxTime, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(DiffusionExpon, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(DiffusionStrength, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(TimeOrigin, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SimulationTimeEnd, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(InitialTimeStep, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(MaxTimeStep, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(gravity, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(DtReductionFactor, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SCALE, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(TrackedElemName, 8, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(LongKeyWord, 8, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NumUserTimeSteps, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(UserTimeStep, 100, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(rel_convergence_crit, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(abs_convergence_crit, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
!      call MPI_BCAST(U_p, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(W_upstream, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(W_NRSolution, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(P_overshoot, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(T_overshoot, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(S_overshoot, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(derivative_increment, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(W_implicitness, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Tot_NumTimeSteps, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NumNRIterations, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Tot_NumNRIterations, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(TimeShift, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
!kz      call MPI_BCAST(CumValue, 6, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NumPrintTimes, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(TimeStepMax_PostPrint, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(OutputTimes, 100, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(longest(1), 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
!
      call MPI_BCAST(DoublingDtCriterion, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_ProducedFluidComposition, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_SolidPhasesEffect, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_ThermalConductivity, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_UpstreamWeighting, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_SSRateInterpolation, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_PermeabilityAdjustment, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_GasSolubility, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_InterfaceDensity, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_Print_NRIterationInfo, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_Print_CyclingInfo, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_Print_JacobianInfo, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_Print_SourceSinkInfo, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_Print_EOSInfo, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_Print_SolverInfo, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_BinaryGasDiffusivities, 4, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_OutputFormat, 4, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Option_InterfaceDiffusiveFluxes, 4, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Flag_CheckInitialConditions, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Flag_PrintInputs, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Flag_SemianalyticHeatExchange, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Flag_MaxP_OutputFile, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
!
      call MPI_BCAST(SS(1:Max_NumSS)%ElemName, 8*Max_NumSS, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%name, 5*Max_NumSS, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%type, 5*Max_NumSS, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%RateType, 4*Max_NumSS, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%RateResponse, 4*Max_NumSS, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%DelivFileName, 5*Max_NumSS, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%Rate_vs_time, Max_NumSS, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%ElemNum, Max_NumSS, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%TypeIndx, Max_NumSS, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%Table_Length, Max_NumSS, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%NumEquTypes, Max_NumSS, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%Rate, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%TempRate, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%enth, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%ProdIndx, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%BtmHoleP, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%LayerZ, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%grad, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%PresLimit, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)% RateStepChange, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%RateFactor, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(SS(1:Max_NumSS)%RateLimit, Max_NumSS, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      DO iI=1,NumSS
       IF (SS(iI)%Rate_vs_time=='T') THEN
        iJ=ABS(SS(iI)%Table_Length)
        IF(iJ==0) iJ=1 
        IF(myid/=iMaster) then
         ALLOCATE(SS(iI)%TimeColumn(iJ),SS(iI)%RateColumn(iJ), STAT = ier)        
         IF(SS(iI)%type(5:5) /= ' ') ALLOCATE(SS(iI)%EnthColumn(iJ), STAT = ier)
        END IF        
        call MPI_BCAST(SS(iI)%TimeColumn(1:iJ), iJ, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
        call MPI_BCAST(SS(iI)%RateColumn(1:iJ), iJ, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
        IF(SS(iI)%type(5:5) /= ' ') then  
         call MPI_BCAST(SS(iI)%EnthColumn(1:iJ), iJ, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
        END IF
       END IF
      END DO
      call MPI_BCAST(WDel(1:Max_NumSS)%idTab, Max_NumSS, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(WDel(1:Max_NumSS)%NumG, Max_NumSS, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(WDel(1:Max_NumSS)%NumH, Max_NumSS, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
!kzhang
     DO iI=1, NumSS
      iJ=WDel(iI)%idTab  
      if (iJ>0) THEN
       nG=WDel(iJ)%NumG
       nH=WDel(iJ)%NumH     
! ... Allocate memory to the array including the bottomhole pressure data
! ----------
       iK=nG+nH+nG*nH
       IF(myid .ne. iMaster) then
        IF ((nG+nH)>0) ALLOCATE(WDel(iJ)%p_bhp(iK), STAT = ier)  
       END IF
       IF ((nG+nH)>0) THEN
        call MPI_BCAST(WDel(iJ)%p_bhp(1:iK), iK, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
       END IF
      end if
     END DO

      call MPI_BCAST(NoVersion, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(HCom%NCom, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      IF (myid/=iMaster) ALLOCATE(GPhase_Com(Max_NumGasComp), STAT=ier) 
      iI=HCom%NCom+1
      call MPI_BCAST(HCom%nameG(1:iI), iI*6, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(HCom%hydrN(1:iI), iI, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(HCom%moleF(1:iI), iI, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(GPhase_Com(1:iI), iI*6, MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      IF (myid/=iMaster) Then 
       CALL Real_Gas_Setup(Max_NumGasComp,GPhase_Com(1:Max_NumGasComp),'PR ','G', VERS_Unit)
      END IF
      call MPI_BCAST(HCom%MolWt(1:iI), iI, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(HCom%GasMF(1:iI), iI, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(HCom%H2OMF(1:iI), iI, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(GH%N_ThC, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(GH%N_SpH, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      iI=GH%N_SpH
      IF (myid/=iMaster) ALLOCATE(GH%p_SpH(iI), STAT = ier)
      call MPI_BCAST(GH%p_SpH(1:iI), iI, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      iI=GH%N_ThC
      IF (myid/=iMaster) ALLOCATE(GH%p_ThC(iI), STAT = ier)
      call MPI_BCAST(GH%p_ThC(1:iI), iI, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(GH%N_Rho, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      iI=GH%N_Rho
       IF (myid/=iMaster) ALLOCATE(GH%p_Rho(iI), STAT = ier) 
      call MPI_BCAST(GH%p_Rho(1:iI), iI, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
!      call MPI_BCAST(inhibitor_flag, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(RefDepartureEnth, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Max_TShift, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Y_atMax_TShift, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr) 
      call MPI_BCAST(InhibitorMW, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr) 
      call MPI_BCAST(InhibitorDens, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr) 
      call MPI_BCAST(InhibitorEnthSol, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr) 
      call MPI_BCAST(InhibitorSpecHeatCoef(0:2), 3, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(KineticReaction, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(EquationOption, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(ActivationEnergy, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(IntrinsicRateConstant, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(Area_Factor, 1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      IF_Kinetic: IF(KineticReaction) THEN
         if (myid/=iMaster) ALLOCATE(SArea_Hyd(NumElemTot), STAT = ier)
      END IF IF_Kinetic
      call MPI_BCAST(MaxNumBC_Coef, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NumBC_Input, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      IF (NumBC_Input>0) THEN
       IF (myid/=iMaster) Then
        ALLOCATE(BC_Poly(NumBC_Input), STAT=ier)
        ALLOCATE(BC_PolyCoef(NumBC_Input,MaxNumBC_Coef), STAT=ier)
       END IF
       call MPI_BCAST(BC_Poly%PrimVarNum, NumBC_Input, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
       call MPI_BCAST(BC_Poly%NumBC_Coef, NumBC_Input, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
       call MPI_BCAST(BC_Poly%TimeSegEnd, NumBC_Input, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
       call MPI_BCAST(BC_PolyCoef, NumBC_Input*MaxNumBC_Coef, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      END IF
      call MPI_BCAST(PolynomialBC_Flag, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
! 
     call MPI_BCAST(MatrixSolver, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(CG_convergence_crit,1, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NEX1, MNCON, MPI_INTEGER, iMaster,MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NEX2, MNCON, MPI_INTEGER, iMaster,MPI_COMM_WORLD, ierr) 
!
      call MPI_BCAST(NumTransientBound, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NumEquBound, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr) 
      call MPI_BCAST(NumTableBound, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      call MPI_BCAST(NumTable, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
      IF (NumTransientBound>0) THEN
       IF(myid/=iMaster) THEN
        ALLOCATE(boundary(NumTransientBound), STAT = ierr)
       END IF
       call MPI_BCAST(boundary%ElemNum, NumTransientBound, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
       call MPI_BCAST(boundary%DataForm, NumTransientBound,MPI_CHARACTER, iMaster,MPI_COMM_WORLD, ierr)
      END IF
!
      IF (NumTable>0) THEN
       IF(myid/=iMaster) THEN
        ALLOCATE(BC_table(NumTable), STAT = ierr)
       END IF
        DO iI=-1,10
         call MPI_BCAST(BC_table%PvarNum(iI),NumTable,MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)       
        END DO
         call MPI_BCAST(BC_table%NumRow,NumTable,MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
         call MPI_BCAST(BC_table%NumCol,NumTable,MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
        DO iI=1,NumTable
         IF(BC_table(iI)%NumRoW>0 .and. BC_table(iI)%NumCol>0) THEN
          IF(myid/=iMaster) ALLOCATE( BC_table(iI)%entry(1:BC_table(iI)%NumRow, 0:BC_table(iI)%NumCol-1), STAT = ierr)
          call MPI_BCAST(BC_table(iI)%entry, BC_table(iI)%NumRow*BC_table(iI)%NumCol, MPI_DOUBLE_PRECISION, iMaster,     &
      &   MPI_COMM_WORLD, ierr)
         END IF
        END DO
      END IF
!
      call MPI_BCAST(Flag_MonitorSubdomains, 1, MPI_LOGICAL, iMaster, MPI_COMM_WORLD, ierr)
      IF(Flag_MonitorSubdomains .EQV. .TRUE.) THEN
       call MPI_BCAST(NumSubdomains,1 , MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)   
       IF(myid/=iMaster) THEN
        ALLOCATE (subdomain(NumSubdomains), STAT=ier)
       END IF
       DO iI = 1,NumSubdomains
        call MPI_BCAST(subdomain(iI)%NumElements,1,MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
        IF(myid/=iMaster) THEN
         ALLOCATE (subdomain(iI)%ElemNum(subdomain(iI)%NumElements), STAT = ier)
        END IF
        iJ=subdomain(iI)%NumElements
        call MPI_BCAST(subdomain(iI)%ElemNum(1:iJ),iJ, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
       END DO
      END IF
      return
      end

!SOLVEINFOCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine solveinfo(unit)
      USE AZ_ME_para
      include "az_aztecf.h"
      integer i, unit

      if (options(AZ_solver+1) .eq. AZ_cg) then
         write(unit,*)'ZZZ Linear Solver Used: CG'
      elseif (options(AZ_solver+1) .eq. AZ_gmres) then
         write(unit,*)'ZZZ Linear Solver Used: GMRES'
      elseif (options(AZ_solver+1) .eq. AZ_cgs) then
         write(unit,*)'ZZZ Linear Solver Used: CGS'
      elseif (options(AZ_solver+1) .eq. AZ_tfqmr) then
         write(unit,*)'ZZZ Linear Solver Used: TFQMR'
      elseif (options(AZ_solver+1) .eq. AZ_bicgstab) then
         write(unit,*)'ZZZ Linear Solver Used: BICGSTAB'
      elseif (options(AZ_solver+1) .eq. AZ_lu) then
         write(unit,*)'ZZZ Linear Solver Used: LU'
      end if

      if (options(AZ_scaling+1) .EQ. AZ_none) then
         write(unit,*)'ZZZ Scaling method: No Scaling'
      elseif (options(AZ_scaling+1) .EQ. AZ_Jacobi) then
         write(unit,*)'ZZZ Scaling method: Point Jacobi'
      elseif (options(AZ_scaling+1) .EQ. AZ_BJacobi) then
         write(unit,*)'ZZZ Scaling method: Block Jacobi'
      elseif (options(AZ_scaling+1) .EQ. AZ_row_sum) then
         write(unit,*)'ZZZ Scaling method: Row Sum (scaled to be 1)'
      elseif (options(AZ_scaling+1) .EQ. AZ_sym_diag) then
         write(unit,*)'ZZZ Scaling method: Sym Scaling (diag = 1)'
      elseif (options(AZ_scaling+1) .EQ. AZ_sym_row_sum) then
         write(unit,*)'ZZZ Scaling method: Symmetric Row Sum'
      end if

      if (options(AZ_precond+1) .EQ. AZ_none) then
         write(unit,*)'ZZZ Preconditioner: None '
      elseif (options(AZ_precond+1) .EQ. AZ_Jacobi) then
         write(unit,*)'ZZZ Preconditioner: Block Jacobi'
         write(unit,*)'ZZZ    with number of step:', &
     &                 options(AZ_poly_ord)
      elseif (options(AZ_precond+1) .EQ. AZ_Neumann) then
         write(unit,*)'ZZZ Preconditioner: Neumann Series Polynomial'
         write(unit,*)'ZZZ    with number of step:', &
     &                 options(AZ_poly_ord)
      elseif (options(AZ_precond+1) .EQ. AZ_ls) then
         write(unit,*)'ZZZ Preconditioner: Least-squares Polynomial'
         write(unit,*)'ZZZ    with number of step:',&
     &                  options(AZ_poly_ord)

      elseif (options(AZ_precond+1) .EQ. AZ_dom_decomp) then

         write(unit,*)'ZZZ Preconditioner: Domain Decomposition'

         if (options(AZ_type_overlap+1) .EQ. AZ_standard) then
            write(unit,*)'ZZZ    with overlap type: Standard'
         elseif (options(AZ_type_overlap+1) .EQ. AZ_symmetric) then
            write(unit,*)'ZZZ    with overlap type: Symmetric'
         end if 
         if (options(AZ_overlap+1) .EQ. AZ_diag) then
            write(unit,*)'ZZZ    and augmenting with block diagonal'
         else
            write(unit,*)'ZZZ    and size of overlap:',&
     &                    options(AZ_overlap+1)
         end if
         if (options(AZ_subdomain_solve) .EQ. AZ_lu) then
            write(unit,*)'ZZZ    and subdomain solver: LU'
         elseif (options(AZ_subdomain_solve+1) .EQ. AZ_ilut) then
            write(unit,*)'ZZZ    and subdomain solver: ILUT'
         elseif (options(AZ_subdomain_solve+1) .EQ. AZ_ilu) then
            write(unit,*)'ZZZ    and subdomain solver: ILU'
         elseif (options(AZ_subdomain_solve+1) .EQ. AZ_rilu) then
            write(unit,*)'ZZZ    and subdomain solver: RILU'
         elseif (options(AZ_subdomain_solve+1) .EQ. AZ_bilu) then
            write(unit,*)'ZZZ    and subdomain solver: BILU'
         elseif (options(AZ_subdomain_solve+1) .EQ. AZ_icc) then
            write(unit,*)'ZZZ    and subdomain solver: ICC'
         end if
         if (options(AZ_reorder+1) .EQ. 0) then
            write(unit,*)'ZZZ    with RCM reordering'
         elseif (options(AZ_reorder+1) .EQ. 1) then
            write(unit,*)'ZZZ    without RCM reordering'
         end if 
      end if

      if (options(AZ_conv+1) .EQ. AZ_r0) then
         write(unit,*)'ZZZ Residual norm: ||r||2 / ||r(0)||2'
      elseif (options(AZ_conv+1) .EQ. AZ_rhs) then
         write(unit,*)'ZZZ Residual norm: ||r||2 / ||b||2'
      elseif (options(AZ_conv+1) .EQ. AZ_Anorm) then
         write(unit,*)'ZZZ Residual norm: ||r||2 / ||A||inf'
      elseif (options(AZ_conv+1) .EQ. AZ_noscaled) then
         write(unit,*)'ZZZ Residual norm: ||r||2'
      elseif (options(AZ_conv+1) .EQ. AZ_sol) then
         write(unit,*)'ZZZ Residual norm: &
     &               ||r||inf / (||A||inf * ||x||1 + ||b||inf)'
      elseif (options(AZ_conv+1) .EQ. AZ_weighted) then
         write(unit,*)'ZZZ Residual norm: ||r||WRMS (see Aztec UG)'
      end if

      write(unit,*)'ZZZ Max. number of iterations:',&
     &              options(AZ_max_iter+1)
      write(unit,*)'ZZZ Tolerance:',params(AZ_tol+1) 

      return
      end

!RSOLVEINFOCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine rsolveinfo
      USE AZ_ME_para
      USE Solver_Parameters
      USE solver_ps
      IMPLICIT NONE
      include "az_aztecf.h"

      character*20 param, value
      integer ivalue
      real*8 rvalue
      logical done,EX

      call AZ_defaults(options, params)

!     Default settings for use of Aztec
      options(AZ_conv+1) = AZ_rhs
      options(AZ_scaling+1) = AZ_BJacobi
      options(AZ_max_iter+1) = 500
      Params(AZ_tol+1) = 1.0e-6
!      if(CG_convergence_crit>1.0e-13) Params(AZ_tol+1) = CG_convergence_crit
      options(AZ_solver+1) = AZ_bicgstab
      options(AZ_precond+1) = AZ_dom_decomp
      options(AZ_subdomain_solve+1) = AZ_ilut
      options(AZ_output+1) = AZ_none
      options(AZ_overlap+1) = 0
 
      metisopts(0) = 1 
      metisopts(1) = 3 
      metisopts(2) = 1 
      metisopts(3) = 3
!     metisopts(3) Default value for metisopts(3) depend choice of
!     on partitioning algorithm

      EE_partitioner = 'METIS_Kway'


       INQUIRE(FILE='PARAL.prm',EXIST=EX)
       IF(EX) then
        metisopts(3) = -1
      open(56,file='PARAL.prm', status='old')
 !read through first line
       read(56,*) EE_output,EE_output,EE_output          
       EE_output=200

      done = .FALSE.
      do while (.NOT. done)
 
         read(56,*)param, value

         if (param .eq. 'END') then

            done = .TRUE.

         elseif (param .eq. 'AZ_solver') then

            if (value .eq. 'AZ_cg') then
               options(AZ_solver+1) = AZ_cg
            elseif (value .eq. 'AZ_gmres') then
               options(AZ_solver+1) = AZ_gmres
            elseif (value .eq. 'AZ_cgs') then
               options(AZ_solver+1) = AZ_cgs
            elseif (value .eq. 'AZ_tfqmr') then
               options(AZ_solver+1) = AZ_tfqmr
            elseif (value .eq. 'AZ_bicgstab') then
               options(AZ_solver+1) = AZ_bicgstab
            elseif (value .eq. 'AZ_lu') then
               options(AZ_solver+1) = AZ_lu
            else
               write(36,*)'Unrecognized value:',value
            end if

         elseif (param .eq. 'AZ_scaling') then

            if (value .eq. 'AZ_none') then
               options(AZ_scaling+1) = AZ_none
            elseif (value .eq. 'AZ_Jacobi') then
               options(AZ_scaling+1) = AZ_Jacobi
            elseif (value .eq. 'AZ_BJacobi') then
               options(AZ_scaling+1) = AZ_BJacobi
            elseif (value .eq. 'AZ_row_sum') then
               options(AZ_scaling+1) = AZ_row_sum
            elseif (value .eq. 'AZ_sym_diag') then
               options(AZ_scaling+1) = AZ_sym_diag
            elseif (value .eq. 'AZ_sym_row_sum') then
               options(AZ_scaling+1) = AZ_sym_row_sum
            else
               write(36,*)'Unrecognized value:',value
            end if

         else if (param .eq. 'AZ_precond') then

            if (value .eq. 'AZ_none') then
               options(AZ_precond+1) = AZ_none
            elseif (value .eq. 'AZ_Jacobi') then
               options(AZ_precond+1) = AZ_Jacobi
            elseif (value .eq. 'AZ_Neumann') then
               options(AZ_precond+1) = AZ_Neumann
            elseif (value .eq. 'AZ_ls') then
               options(AZ_precond+1) = AZ_ls
            elseif (value .eq. 'AZ_sym_GS') then
               options(AZ_precond+1) = AZ_sym_GS
            elseif (value .eq. 'AZ_dom_decomp') then
               options(AZ_precond+1) = AZ_dom_decomp
            else
               write(36,*)'Unrecognized value:',value
            end if

         elseif (param .eq. 'AZ_type_overlap') then

            if (value .eq. 'AZ_standard') then
               options(AZ_type_overlap+1) = AZ_standard
            elseif (value .eq. 'AZ_symmetric') then
               options(AZ_type_overlap+1) = AZ_symmetric
            else
               write(36,*)'Unrecognized value:',value
            end if

         elseif (param .eq. 'AZ_overlap') then

            if (value .eq. 'AZ_diag') then
               options(AZ_overlap+1) = AZ_diag
            else
               BACKSPACE 56
               read(56,*)param, ivalue
               options(AZ_overlap+1) = ivalue
            end if

         elseif (param .eq. 'AZ_subdomain_solve') then

            if (value .eq. 'AZ_lu') then
               options(AZ_subdomain_solve+1) = AZ_lu
            elseif (value .eq. 'AZ_ilut') then
               options(AZ_subdomain_solve+1) = AZ_ilut
            elseif (value .eq. 'AZ_ilu') then
               options(AZ_subdomain_solve+1) = AZ_ilu
            elseif (value .eq. 'AZ_rilu') then
               options(AZ_subdomain_solve+1) = AZ_rilu
            elseif (value .eq. 'AZ_bilu') then
               options(AZ_subdomain_solve+1) = AZ_bilu
            elseif (value .eq. 'AZ_icc') then
               options(AZ_subdomain_solve+1) = AZ_icc
            else
               write(36,*)'Unrecognized value:',value
            end if

         elseif (param .eq. 'AZ_conv') then

            if (value .eq. 'AZ_r0') then
               options(AZ_conv+1) = AZ_r0
            elseif (value .eq. 'AZ_rhs') then
               options(AZ_conv+1) = AZ_rhs
            elseif (value .eq. 'AZ_Anorm') then
               options(AZ_conv+1) = AZ_Anorm
            elseif (value .eq. 'AZ_noscaled') then
               options(AZ_conv+1) = AZ_noscaled
            elseif (value .eq. 'AZ_sol') then
               options(AZ_conv+1) = AZ_sol
            elseif (value .eq. 'AZ_weighted') then
               options(AZ_conv+1) = AZ_weighted
            else
               write(36,*)'Unrecognized value:',value
            end if

         elseif (param .eq. 'AZ_reorder') then

            if (value .eq. '0') then
               options(AZ_reorder+1) = 0
            elseif (value .eq. '1') then
               options(AZ_reorder+1) = 1
            else
               write(36,*)'Unrecognized value:',value
            end if
          
         elseif (param .eq. 'AZ_pre_calc') then

            if (value .eq. 'AZ_calc') then
               options(AZ_pre_calc+1) = AZ_calc
            elseif (value .eq. 'AZ_recalc') then
               options(AZ_pre_calc+1) = AZ_recalc
            elseif (value .eq. 'AZ_reuse') then
               options(AZ_pre_calc+1) = AZ_reuse
            end if

         elseif (param .eq. 'AZ_output') then

            if (value .eq. 'AZ_all') then
               options(AZ_output+1) = AZ_all
            elseif (value .eq. 'AZ_none') then
               options(AZ_output+1) = AZ_none
            elseif (value .eq. 'AZ_warnings') then
               options(AZ_output+1) = AZ_warnings
            elseif (value .eq. 'AZ_last') then
               options(AZ_output+1) = AZ_last
            else
               BACKSPACE 56
               read(56,*)param, ivalue
               options(AZ_output+1) = ivalue
             end if

         elseif (param .eq. 'AZ_max_iter') then

            BACKSPACE 56
            read(56,*)param, ivalue
            options(AZ_max_iter+1) = ivalue
  
         elseif (param .eq. 'AZ_tol') then

            BACKSPACE 56
            read(56,*)param, rvalue
            params(AZ_tol+1) = rvalue

         elseif (param .eq. 'EE_partitioner') then

            if (value .eq. 'METIS_Kway') then
               EE_partitioner = 'METIS_Kway'
               if (metisopts(3) .EQ. -1) metisopts(3) = 3
            elseif (value .eq. 'METIS_VKway') then
               EE_partitioner = 'METIS_VKway'
               if (metisopts(3) .EQ. -1) metisopts(3) = 1
            elseif (value .eq. 'METIS_Recursive') then
               EE_partitioner = 'METIS_Recursive'
               if (metisopts(3) .EQ. -1) metisopts(3) = 1
            else
               write(36,*)'Unrecognized value:',value
            end if

         elseif (param .eq. 'METIS_opt1') then
             
            BACKSPACE 56
            read(56,*)param, ivalue
            metisopts(1) = ivalue

         elseif (param .eq. 'METIS_opt3') then
             
            BACKSPACE 56
            read(56,*)param, ivalue
            metisopts(3) = ivalue

         elseif (param .eq. 'EE_output') then

            BACKSPACE 56
            read(56,*)param, ivalue
            EE_output = ivalue
         elseif (param .eq. 'icoley') then
            BACKSPACE 56
            read(56,*)param, ivalue
            icoley = ivalue
         else

            write(36,*)'Parameter is not recognized:',param

         end if

      end do
      close(56)
      end if
      return
      end

!BCAST_CONSTCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      
      subroutine BCAST_CONST
!     When called by all processors, the array size and other constants are replicated 
!     on all processors by broadcasting from proc 0.
      USE MADIM
      USE MPICOM
      USE Basic_Parameters
      IMPLICIT none
      include "mpif.h"

      call MPI_BCAST(MNEL,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(MNCON,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
      NumElemTot=MNEL
      NumElem=MNEL
      Max_NumConx=MNCON
      NumConx=MNCON
      call MPI_BCAST(NumSS,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(NumMedia,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(NumActiveDimensions,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Active2ndDimension,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(Max_NumGasComp,1,MPI_INTEGER,iMaster,MPI_COMM_WORLD,ierr)
      
      Max_NumSS=1  
      IF(NumSS>0) Max_NumSS=NumSS
      Max_NumMedia=1
      IF(NumMedia>0) Max_NumMedia=NumMedia


      return
      end

!QCKSRTONEKEYCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE QCKSRTONEKEY(N,KEY1,INDEX)
      IMPLICIT NONE
      CHARACTER(LEN=*),DIMENSION(N)          :: KEY1
      CHARACTER(LEN=LEN(KEY1))            :: A
      INTEGER N,INDEX,ISTACK,NSTACK,M,L,IR,J,I,IQ	
      PARAMETER(NSTACK=1000)
      INTEGER JSTACK,II
      DOUBLE PRECISION    FM,FA,FC,FMI,FX
      PARAMETER (M=7,FM=7875.D0,FA=211.D0,FC=1663.D0)
      DIMENSION INDEX(N),ISTACK(NSTACK)
      FX=0.0
      FMI = 1.D0 / FM
      JSTACK=0
      L=1
      IR=N
10    IF(IR-L.LT.M)THEN
        DO 13 J=L+1,IR
          II = INDEX(J)
          A = KEY1(INDEX(J))
          DO 11 I=J-1,1,-1
            IF(KEY1(INDEX(I)).LT.A)GO TO 12
            INDEX(I+1) = INDEX(I)
11        CONTINUE
          I=0
12        INDEX(I+1) = II
13      CONTINUE
        IF(JSTACK.EQ.0)RETURN
        IR=ISTACK(JSTACK)
        L=ISTACK(JSTACK-1)
        JSTACK=JSTACK-2
      ELSE
        I=L
        J=IR
        FX=MOD(FX*FA+FC,FM)
        IQ=L+(IR-L+1)*(FX/FM)
        A = KEY1(INDEX(IQ))
        II = INDEX(IQ)
        INDEX(IQ) = INDEX(L)
20      CONTINUE
21        IF(J.GT.0)THEN
            IF (A .LT. KEY1(INDEX(J))) THEN
              J=J-1
              GO TO 21
            ENDIF
          ENDIF
          IF(J.LE.I)THEN
            INDEX(I) = II
            GO TO 30
          ENDIF
          INDEX(I) = INDEX(J)
          I=I+1
22        IF(I.LE.N) THEN
            IF( A.GT.KEY1(INDEX(I))) THEN
              I=I+1
              GO TO 22
            ENDIF
          ENDIF
          IF(J.LE.I)THEN
            INDEX(J) = II
            I=J
            GO TO 30
          ENDIF
          INDEX(J) = INDEX(I)
          J=J-1
        GO TO 20
30      JSTACK=JSTACK+2
        IF(JSTACK.GT.NSTACK) THEN
           WRITE(36,100)
           RETURN
        END IF
        IF(IR-I.GE.I-L)THEN
          ISTACK(JSTACK)=IR
          ISTACK(JSTACK-1)=I+1
          IR=I-1
        ELSE
          ISTACK(JSTACK)=I-1
          ISTACK(JSTACK-1)=L
          L=I+1
        ENDIF
      ENDIF
      GO TO 10
 100  FORMAT(1X,'SORT FAILED WORK AREA TOO SMALL')
      END

!BSEARCHCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE BSEARCH(N, ELEM, INDEX, KEY, ANS)
      IMPLICIT NONE
      INTEGER ANS, N
      INTEGER, DIMENSION(N)          :: INDEX
      CHARACTER(LEN=*), DIMENSION(N) :: ELEM
      CHARACTER(LEN=*)                :: KEY
      INTEGER i, j, k
      logical Print
      Print = .false.
!      if(KEY(1:3).EQ.'TOP')Print=.true.
      i = 1
      j = N
      do while(.true.)
        k = (i+j) / 2

        if (k==0) then
          ANS=0
          exit
         end if

        if(print) then
           write(36,*)i,j,k,KEY,ELEM(INDEX(K))
        end if
        if(KEY .EQ. ELEM(INDEX(K))) then
          ANS = INDEX(K)
          exit
        else if(i .GT. j) then
          ANS = 0
          exit
        end if
        if(KEY .GT. ELEM(INDEX(K))) then
          i = k + 1
        else
          j = k - 1
        end if
      end do
      return
      end

!QCKSRTONEKEY_iCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE QCKSRTONEKEY_i(N,KEY1,INDEX)
      IMPLICIT NONE
      integer,DIMENSION(N)          :: KEY1
      integer                       :: A
      INTEGER N,INDEX,ISTACK,NSTACK,M,L,IR,J,I,IQ	
      PARAMETER(NSTACK=1000)
      INTEGER JSTACK,II
      DOUBLE PRECISION    FM,FA,FC,FMI,FX
      PARAMETER (M=7,FM=7875.D0,FA=211.D0,FC=1663.D0)
      DIMENSION INDEX(N),ISTACK(NSTACK)
      FX=0.0
      FMI = 1.D0 / FM
      JSTACK=0
      L=1
      IR=N
10    IF(IR-L.LT.M)THEN
        DO 13 J=L+1,IR
          II = INDEX(J)
          A = KEY1(INDEX(J))
          DO 11 I=J-1,1,-1
            IF(KEY1(INDEX(I)).LT.A)GO TO 12
            INDEX(I+1) = INDEX(I)
11        CONTINUE
          I=0
12        INDEX(I+1) = II
13      CONTINUE
        IF(JSTACK.EQ.0)RETURN
        IR=ISTACK(JSTACK)
        L=ISTACK(JSTACK-1)
        JSTACK=JSTACK-2
      ELSE
        I=L
        J=IR
        FX=MOD(FX*FA+FC,FM)
        IQ=L+(IR-L+1)*(FX/FM)
        A = KEY1(INDEX(IQ))
        II = INDEX(IQ)
        INDEX(IQ) = INDEX(L)
20      CONTINUE
21        IF(J.GT.0)THEN
            IF (A .LT. KEY1(INDEX(J))) THEN
              J=J-1
              GO TO 21
            ENDIF
          ENDIF
          IF(J.LE.I)THEN
            INDEX(I) = II
            GO TO 30
          ENDIF
          INDEX(I) = INDEX(J)
          I=I+1
22        IF(I.LE.N) THEN
            IF( A.GT.KEY1(INDEX(I))) THEN
              I=I+1
              GO TO 22
            ENDIF
          ENDIF
          IF(J.LE.I)THEN
            INDEX(J) = II
            I=J
            GO TO 30
          ENDIF
          INDEX(J) = INDEX(I)
          J=J-1
        GO TO 20
30      JSTACK=JSTACK+2
        IF(JSTACK.GT.NSTACK) THEN
           WRITE(36,100)
           RETURN
        END IF
        IF(IR-I.GE.I-L)THEN
          ISTACK(JSTACK)=IR
          ISTACK(JSTACK-1)=I+1
          IR=I-1
        ELSE
          ISTACK(JSTACK)=I-1
          ISTACK(JSTACK-1)=L
          L=I+1
        ENDIF
      ENDIF
      GO TO 10
 100  FORMAT(1X,'SORT FAILED WORK AREA TOO SMALL')
      END

!BSEARCH_iCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE BSEARCH_i(N, ELEM, INDEX, KEY, ANS)
      IMPLICIT NONE
      INTEGER ANS, N
      INTEGER, DIMENSION(N)          :: INDEX
      integer, DIMENSION(N)          :: ELEM
      integer                        :: KEY
      INTEGER i, j, k
      logical Print

      Print = .false.
      i = 1
      j = N
      do while(.true.)
        k = (i+j) / 2
   
        if (k==0) then
          ANS=0
          exit
         end if

        if(print) then
           write(36,*)i,j,k,KEY,ELEM(INDEX(K))
        end if

        if(KEY .EQ. ELEM(INDEX(K))) then
          ANS = INDEX(K)
          exit
        else if(i .GE. j) then
          ANS = 0
          exit       
        end if
        if(KEY .GT. ELEM(INDEX(K))) then
          i = k + 1
        else
          j = k - 1
        end if
      end do
      return
      end


!OPFILECCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE OPFILE(MYID,FN1,FileN)
!
!     ******************************************************************
!     OPEN A FILE FOR THIS MPI PROCESS TO HOLD OBSERVATION DATA FROM this process
!     ******************************************************************
!
      IMPLICIT NONE
      integer::MYID,iHun,iThou,iTen,iOne
      integer::FileN,i
      CHARACTER*1 DIGIT(0:9)
      CHARACTER*5 PSUF
      CHARACTER*17 FN1 
      CHARACTER*22 FN
      
      DATA (DIGIT(I),I=0,9)/'0','1','2','3','4','5','6','7','8','9'/
      PSUF = 'P.000'
      IF (MYID.GT.0) THEN
        iThou=MYID/1000
        iHun=MYID/100
        ITEN=MYID/10
        IONE=MOD(MYID,10)
        PSUF(3:3)=DIGIT(iHun)
        PSUF(4:4)=DIGIT(iTen)
        PSUF(5:5)=DIGIT(IONE)
       if (iThou>0) PSUF(1:1)=DIGIT(iThou)
      ENDIF
      FN = FN1//PSUF
        OPEN(MYID+FileN,FILE=FN,FORM='FORMATTED',STATUS='UNKNOWN')

      RETURN
      END




         







