
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                                                                     >
!>         T_Executive.f95: Executive code unit including the          >
!>          executive routines common to all TFx simulations           >
!>                                                                     >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      SUBROUTINE Simulation_Cycle
! 
! ...... Modules to be used 
! 
         USE EOS_Routine_Selector
         USE Geomechanical_Process_Selector
! 
         USE Basic_Parameters
         USE General_Control_Parameters
         USE General_External_File_Units
! 
         USE Grid_Geometry
         USE Element_Attributes
! 
         USE Solution_Matrix_Arrays
         USE Subdomain_Definitions
         USE MPICOM
         USE MADIM
         USE AZ_ME_para
         USE solver_ps
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          EXECUTIVE ROUTINE THAT PERFORMS THE SIMULATION             * 
!*                     WHILE ADVANCING IN TIME                         *
!*                                                                     *
!*                  Version 1.0 - January 9, 2004                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
      include 'mpif.h'
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: CPU_time, Dt_solv, T_dd
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: n, NSTL, KIT, IGOOD_tem,i,j
      INTEGER :: n_t, n_t0, i_n, NIT, NIT1
!
      REAL(KIND = 8):: inRERM(2), outRERM(2)
      REAL(KIND = 8),ALLOCATABLE, DIMENSION(:)::rWork
      INTEGER:: from,stat(MPI_STATUS_SIZE)
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: ConvergenceFailure, DtCut_Flag, PrintNow_Flag
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Simulation_Cycle
!
!
      IF(myid==iMaster) WRITE(VERS_Unit,6000)
! 
! 
!***********************************************************************
!*                                                                     *
!*                           INITIALIZATIONS                           *
!*                                                                     *
!***********************************************************************
! 
! 
      NumTimeSteps = 0
! 
      IGOOD = 0
      No_Update_PS=1
! 
      time = TimeShift
! 
      NewTimeStep = InitialTimeStep
! 
      NSTL = (TrackElemNum-1)*NumComPlus1
! 
      NumNRIterations = 0
! 
      KIT  = 0
! 
      convergence         = .FALSE.    ! Flag denoting convergence of the Newton-Raphson iteration - Simulation continuing 
! 
      ConvergenceFailure  = .FALSE.    ! Flag denoting convergence failure leading to abrupt simulation end
      NumLinSolve=0

      Allocate(rWork(NumElem*NumComPlus1))
! 
! ----------
! ... Initialize thermophysical properties        
! ----------
!
      CALL Equation_of_State                  ! Calling the Equation-of-State routines
! 
      CALL Wettability(NumElemTot,NumEqu)     ! Calling the wettability properties routine
! 
! ... Call code unit for geomechanical computations
!
      IF(Flag_CoupledGeomechanics) CALL Geomechanical_Processes(Geomechanics_Flag,'N',1)
! 
! ... Computing changes to porosity, permeability and capillary pressure 
! 

      CALL Hydraulic_Property_Changes    

! 
      CALL Mass_and_Energy_Balance(0)  ! Compute mass and heat balance
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                       MAIN TIME STEP LOOP                           >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!

      eetime1=MPI_WTIME()
      n_t0 = Tot_NumTimeSteps + 1          ! Beginning value of the cumulative number of time steps (including restarts)
!
      DO_TimeSteps: DO n_t = n_t0,Max_NumTimeSteps
!
         Tot_NumTimeSteps = n_t            ! Update <Tot_NumTimeSteps>
! 
         NumTimeSteps = NumTimeSteps + 1   ! Update the number of time steps (new time step)
!
         IF(Option_Print_CyclingInfo /= 0) THEN
            IF(myid==iMaster) WRITE(36,6001) NumTimeSteps, Tot_NumTimeSteps, NewTimeStep, time  ! Print explanatory messsage
         END IF

! 
         NumTimeStepCuts = 0   ! Initializing the timestep reduction counter 
! 

!***********************************************************************
!*                                                                     *
!*                 INITIALIZATIONS FOR NEW TIME STEP                   *
!*                                                                     *
!***********************************************************************
! 
! 
 2000    NumNRIterations = 0       ! The counter of the Newtonian iterations in this time step
!  
         PrintOutNow   = .FALSE.   ! Flag indicating print-out at a requested time
! 
         PrintNow_Flag = .FALSE.   ! Flag indicating immediate print-out 
! 
! -------------
! ...... Set the time-step (Dt) size & adjust related variables
! -------------
! 

         IF(NumPrintTimes /= 0) CALL Adjust_Timestep_for_Printout  ! Adjust Dt to coincide with desired print-out times
!
         TimeStep = NewTimeStep                          ! Set Dt for the ensueing computations

         convergence = .FALSE.                           ! Set the convergence flag
! 
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*                   THE NEWTONIAN ITERATION LOOP                      *
!*                                                                     *
!***********************************************************************
!***********************************************************************
! 
! 
         DO_NRIterations: DO i_n = 1,Max_NumNRIterations+1
! 
! ----------------
! ......... Initialize changes in the primary variables for this iteration
! ----------------
! 
            IF(NumNRIterations == 0) DX = 0.0d0  ! Whole array operation
! 
! ----------------
! ......... Initialize the number of phase changes in this iteration
! ----------------
! 
            ElemState%changes = 0_1   ! Whole array operation
! 
! ----------------
! ......... Update counters
! ----------------
! 
            NumNRIterations     = NumNRIterations + 1      ! Update the Newtonian iteration counter in this time step
            Tot_NumNRIterations = Tot_NumNRIterations + 1  ! Update the cumulative Newtonian iteration counter since t = 0
! 
            TimeIncrement = W_implicitness*TimeStep        ! Compute the implicitness-weighted time increment
! 
! 
!***********************************************************************
!*                                                                     *
!*        Compute the accumulation, source/sink and flow terms         *
!*                    SET UP THE JACOBIAN EQUATIONS                    *
!*                                                                     *
!***********************************************************************
! 
            CALL JACOBIAN_SetUp
!
            IF(IGOOD_Needed==1) THEN
             IGOOD_tem = IGOOD
             call MPI_Allreduce(IGOOD_tem, IGOOD, 1, MPI_INTEGER,MPI_MAX, MPI_COMM_WORLD, ierr)
             IF(IGOOD == 3) THEN
!
               CALL Timestep_Reduction(KIT,DtCut_Flag,ConvergenceFailure)  ! Cut Dt if necessary
               IF(DtCut_Flag .EQV. .TRUE.) GO TO 2000                      ! If so, restart the Newtonian iteration loop
!
             END IF
            END IF
! 
!     Modification to find out the global value for RERM and
!     in which equation this value have been found.
!     A partial step is to find out which processor that have
!     found the global RERM.

      inRERM(1) = MaxResidual
      inRERM(2) = myid*1.d0
      call MPI_Allreduce(inRERM, outRERM, 1, MPI_2DOUBLE_PRECISION,&
     &                   MPI_MAXLOC, MPI_COMM_WORLD, ierr)
      MaxResidual = outRERM(1)
      from = int(outRERM(2)+0.01)

      if (myid .eq. iMaster) then
         if (from .eq. iMaster) then
!           Maximum residual found on master processor 
            ELEMNER = ELEM(MaxResElemNum)%name
         else
!           Maximum residual found on processor "from"
            call MPI_RECV(MaxResEquNum, 1, MPI_INTEGER, from,901, MPI_COMM_WORLD, stat, ierr)        
            call MPI_RECV(ELEMNER, 8, MPI_CHARACTER, from,902, MPI_COMM_WORLD, stat, ierr)
         end if
      else
!        If max residual found on myid, send KER and ELEM(NER) to master proc
         if (from .eq. myid) then
            call MPI_SEND(MaxResEquNum, 1, MPI_INTEGER, iMaster,901, MPI_COMM_WORLD, ierr)         
            call MPI_SEND(ELEM(MaxResElemNum)%name, 8, MPI_CHARACTER, iMaster,902, MPI_COMM_WORLD, ierr)
         end if
      end if
! 
!***********************************************************************
!*                                                                     *
!*                        UPON CONVERGENCE ...                         *
!*                                                                     *
!***********************************************************************
! 
! 
            IF_Convrg: IF(MaxResidual <= rel_convergence_crit) THEN
! 
               CALL Post_Convergence_Activities(NSTL,KIT,ConvergenceFailure,PrintNow_Flag,EOS_Specific_Flag)
! 
               IF(PrintNow_Flag .EQV. .TRUE.) THEN  ! If the print-out flag is set, ...
                  CALL Print_Standard_Output        ! ...  produce a print-out and ...
                  CALL Mass_and_Energy_Balance(0)   ! ...  compute mass and heat balances
               END IF
! 
! ............ Update properties in coupled geomechanics simulations 
! 
               IF(Flag_CoupledGeomechanics .AND. (PrintNow_Flag .EQV. .TRUE.)) CALL Geomechanical_Processes(Geomechanics_Flag,'Y',0)
! 
               EXIT DO_NRIterations  ! End NR iterations, and continue timestepping
! 
            END IF IF_Convrg
! 
! 
!***********************************************************************
!*                                                                     *
!*               IF CONVERGENCE IS NOT YET ATTAINED ...                *
!*                                                                     *
!***********************************************************************
! 
! 
            IF(Option_Print_NRIterationInfo /= 0) THEN
! 
! ............ Print basic information for MOP(1) /= 0
! 
               IF(myid==iMaster) WRITE(36,6006) Tot_NumTimeSteps, NumNRIterations, TimeStep, MaxResidual, &
          &    ELEMNER, MaxResEquNum
            END IF
! 
! ----------------
! ......... Print extra information for <Option_Print_NRIterationInfo> > 2
! ----------------
!
            IF_Mop1: IF(Option_Print_NRIterationInfo >= 2) THEN
! 
               IF(TrackElemNum /= 0) THEN
                  NIT = TrackElemNum
               ELSE
                  NIT = MaxResElemNum
               END IF
! 
               NIT1 = (NIT-1)*NumComPlus1
! 
               IF (myid==iMaster) WRITE(36,6008) elem(nit)%name, DX(NIT1+1), DX(NIT1+2),            &
     &          ElemState(nit)%temp(current),  ElemState(nit)%pres(current), ElemProp(nit,0)%satur(1)
               IF (myid/=iMaster .and. TrackElemNum /= 0) THEN
                   WRITE(Track_Unit,6008) elem(nit)%name, DX(NIT1+1), DX(NIT1+2),            &
     &          ElemState(nit)%temp(current),  ElemState(nit)%pres(current), ElemProp(nit,0)%satur(1)
               END IF  
! 
            END IF IF_Mop1
! 
! ----------------
! ......... Check if the maximum number of Newtonian iterations has been reached
! ----------------
! 
            IF(NumNRIterations > Max_NumNRIterations) THEN
!
               CALL Timestep_Reduction(KIT,DtCut_Flag,ConvergenceFailure)  ! Cut Dt if necessary
               IF(DtCut_Flag .EQV. .TRUE.) GO TO 2000                      ! If so, restart the Newtonian iteration loop
!
            END IF
! 
! 
!***********************************************************************
!*                                                                     *
!*                         SOLVE THE JACOBIAN                          *
!*                                                                     *
!***********************************************************************
! 
!
            CALL CPU_Elapsed_Time(0,Dt_solv,T_dd)   ! Set timer 
!
            CALL Solve_Jacobian_Matrix_Equation
            NumLinSolve=NumLinSolve+1
!
            CALL CPU_Elapsed_Time(1,Dt_solv,T_dd)   ! Determine execution time 
!
            CPU_MatrixSolTime = CPU_MatrixSolTime + Dt_solv
!
            call rexchexternal(myid, data_org, R, rwork, NumEqu, 5000)
            DO j=1,NumElem                  ! the part between NumElem and NumElemtot will be updated in eos
             DO i=1,NumEqu
              DX(LOCP(j)+i)=DX(LOCP(j)+i)+relaxf*R(Loc(j)+i)
             END DO
            END DO
!
!kz            IF(IGOOD  /= 0) THEN
!
!kz               CALL Timestep_Reduction(KIT,DtCut_Flag,ConvergenceFailure)  ! Cut Dt if necessary
!kz               IF(DtCut_Flag .EQV. .TRUE.) GO TO 2000                      ! If so, restart the Newtonian iteration loop
!
!kz            END IF
! ----------------
! ......... Update the thermophysical properties
! ----------------
! 
            CALL Equation_of_State
! 
! ----------------
! ......... If unrealistic conditions are encountered, reduce time step
! ----------------
!
             IGOOD_tem = IGOOD
             if(PressureLimitsExceeded) IGOOD_tem=IGOOD_tem+1000

              call MPI_Allreduce(IGOOD_tem, IGOOD, 1, MPI_INTEGER,MPI_MAX, MPI_COMM_WORLD, ierr)

             if(IGOOD>999) goto 999
!
            IF(IGOOD /= 0) THEN
!
               CALL Timestep_Reduction(KIT,DtCut_Flag,ConvergenceFailure)  ! Cut Dt if necessary
               IF(DtCut_Flag .EQV. .TRUE.) GO TO 2000                      ! If so, restart the Newtonian iteration loop
!
            END IF
! 
! ----------------
! ......... Update the wettability properties
! ----------------
! 
            CALL Wettability(NumElemTot,NumEqu)   
! 
! ----------------
! ......... Call code unit for geomechanical computations
! ----------------
! 
            IF(Flag_CoupledGeomechanics) CALL Geomechanical_Processes(Geomechanics_Flag,'N',i_n)
! 
! ----------------
! ......... Computing changes to porosity, permeability and capillary pressure 
! ----------------
! 
            CALL Hydraulic_Property_Changes        
! 
! ----------------
! ......... Check if print-out is needed
! ----------------
! 
            IF(Option_OutputAmount >= 10) CALL Print_Standard_Output
!
! <<<<<<                          
! <<<<<< End of the NEWTONIAN ITERATION LOOP         
! <<<<<<    
!
         END DO DO_NRIterations
! 
! 
!***********************************************************************
!*                                                                     *
!*          CHECK IF ANY OF THE VARIOUS TERMINATION CRITERIA           *
!*                         HAVE BEEN FULFILLED                         *
!*                                                                     *
!***********************************************************************
! 
! 
         CALL CPU_Timing_Routine(CPU_time)                                        ! Check the CPU time up to this point
!

   999   IF (IGOOD>999) THEN
          CALL Print_Standard_Output
          IGOOD=1000
         END IF

          
         IF(  (ConvergenceFailure) .OR.                                                   &  ! The termination flag is invoked
     &        (IGOOD>999) .OR.                                                            &  ! The well pressure limit (max or min) is exceeded
     &        (SimulationTimeEnd /= 0.0d0 .AND. time >= SimulationTimeEnd) .OR.           &  ! The simulation time is exceeded
     &        (Tot_NumTimeSteps >= Max_NumTimeSteps) .OR.                                 &  ! The number of time steps is exceeded
     &        (CPU_MaxTime /= 0.0d0 .AND. (CPU_time-CPU_InitialTime) > CPU_MaxTime)       &  ! The alloted CPU time is exceeded
     &     ) THEN 
! 
            CALL Write_File_SAVE                        ! Write results into file SAVE
! 
            IF(Flag_CoupledGeomechanics) CALL Geomechanical_Processes(Geomechanics_Flag,'Y',1)
! 
            IF(Flag_SemianalyticHeatExchange) THEN         ! When the semi-analytical heat exchange solution is used
               REWIND (UNIT = TABLE_Unit)                  !    Open the file TABLE  
               WRITE(36,6002)                               !    Print a heading
               WRITE(TABLE_Unit,6005) (AI(n),n=1,NumElem)  ! Write the heat exchange coefficients into TABLE
            END IF
            IF(iDoSave .GE. 0) CALL SAVE_HYD_INFO
            IF(G_ObsConx_Flag .EQV. .TRUE.) CALL SAVE_Conx_Time_Series
            IF(G_ObsSS_Flag .EQV. .TRUE.) CALL SAVE_SS_Time_Series
            IF(Flag_MonitorSubdomains .EQV. .TRUE.) CAll SAVE_SubDomain_Time_Series

            CALL Print_Parallel_Info
! 
            EXIT                                ! Done!  Exit the subroutine                
! 
         END IF
!
! <<<                      
! <<< End of the TIME STEP LOOP         
! <<<
!
      END DO DO_TimeSteps
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Simulation_Cycle',T50,'[v1.0,  18 August    2006]',/,   &
     &         ':::::   Executive routine that performs the simulation while marching in time')
!
!
!
 6001 FORMAT(/,' Simulation_Cycle :::  <NumTimeSteps> = ',I6,' <Tot_NumTimeSteps> =',I6, &
     &         '  <TimeStep> = ',1pE13.6,'  <time> = ',1pE13.6,/)
!
 6002 FORMAT(' Write coefficients for semi-analytical heat loss calculation onto file "TABLE"')
!
 6005 FORMAT(4E20.13)
 6006 FORMAT(' ...ITERATING...  AT [',I6,',',I2,']: TimeStep = ',1pE13.6,  &
     &       '   MAX{Residual} = ',1pE13.6,'  at element "',A8,'", equation ',I2)
!
 6008 FORMAT(31X,'     AT "',A8,'" ...   DX1= ',1pE12.5,' DX2= ',1pE12.5,' T = ',F7.3,' P = ',F9.0,' S = ',1pE12.5)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Simulation_Cycle
!
!
      RETURN
! 
      END SUBROUTINE Simulation_Cycle
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Timestep_Reduction(k_it,DtCut_Flag,ConvergenceFailure)
! 
! ...... Modules to be used 
! 
         USE Geomechanical_Process_Selector
! 
         USE Basic_Parameters
         USE General_External_File_Units
         USE General_Control_Parameters
! 
         USE Grid_Geometry, ONLY: AI
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*               ROUTINE FOR REDUCING THE TIME-STEP SIZE               *
!*                                                                     *
!*                  Version 1.00, January 11, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: k_it
! 
      INTEGER :: n
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL, INTENT(OUT) :: ConvergenceFailure, DtCut_Flag
! 
      LOGICAL  :: First_call = .TRUE.
! 
! -------
! ... Saving variables     
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Timestep_Reduction
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(myid==iMaster)  WRITE(VERS_Unit,6000)
      END IF
! 
! -------
! ... Reduce time-step size  
! -------
! 
      NewTimeStep = TimeStep/DtReductionFactor
!
      IGOOD  = 0               ! Reset the IGOOD flag
!
      DtCut_Flag = .FALSE.     ! Initialize the flag for transfer of control on exit
!
!***********************************************************************
!*                                                                     *
!*  Check on number of previous time steps that converged on ITER = 1  *
!*                                                                     *
!***********************************************************************
!
      IF(k_it > 2 .AND. DoublingDtCriterion /= 0) THEN
         IF(myid==iMaster) WRITE(36,6001) Tot_NumTimeSteps, TimeStep        ! Print explanatory comment
         ConvergenceFailure = .TRUE.                     ! Set flag to stop execution after following Dt
      END IF
! 
      IF(myid==iMaster) WRITE(36,6002) Tot_NumTimeSteps, NumNRIterations, NewTimeStep   ! Print explanatory comment 
! 
! -------
! ... Determine the number of Dt cutbacks  
! -------
! 
      NumTimeStepCuts = NumTimeStepCuts + 1
!
!***********************************************************************
!*                                                                     *
!*             STOP AFTER 25 ATTEMPTS TO REDUCE TIME STEP              *
!*                                                                     *
!***********************************************************************
!
      IF_IhafMax: IF(NumTimeStepCuts > 25) THEN
! 
         IF(myid==iMaster) WRITE(36,6003) TimeStep     ! Print warning message
         CALL Print_Standard_Output       ! Write output before stopping
         CALL Write_File_SAVE             ! Write the SAVE file before stopping
! 
! ----------
! ...... For semianalytical estimation of heat loss to bounding layers 
! ----------
! 
         IF(Flag_SemianalyticHeatExchange) THEN
! 
            REWIND (UNIT = TABLE_Unit)                  ! Open file TABLE
            WRITE(36,6004)                               ! Print a warning message
            WRITE(TABLE_Unit,6005) (AI(n),n=1,NumElem)  ! Write the semianalytical heat ...
!                                                       ! ... exchange parameters into TABLE
         END IF                      
! 
         CALL STOP_ALL                            ! Stop the simulation
! 
      ELSE
! 
         NumNRIterations = 0       ! Reset the counter of Newtonian iterations for new Dt
!
         CALL Equation_of_State                        ! Re-estimate thermophysical properties
!
         CALL Wettability(NumElemTot,NumEqu)           ! Re-estimate wettability properties
! 
         IF(Flag_CoupledGeomechanics) CALL Geomechanical_Processes(Geomechanics_Flag,'N',0) ! Call code unit for geomechanical computations
! 
         CALL Hydraulic_Property_Changes   ! Computing changes in hydraulic properties        
!
         DtCut_Flag = .TRUE.               ! Set the flag for a new NI iteration with a shorter Dt on exit
!                                
      END IF IF_IhafMax
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Timestep_Reduction',T50,'[v1.0,  18 December  2006]',/, &
     &         ':::::   Routine to reduce the timestep size') 
!
 6001 FORMAT(/,' >>>>>>>>  CONVERGENCE FAILURE on time step #',I6,   &
     &         ' with Dt = ',1pE13.6,' seconds, following two steps that converged on <NumNRIterations> = 1',/,   &
     &         '      ==>  STOP EXECUTION AFTER NEXT TIME STEP')
 6002 FORMAT(' >>>>>  REDUCE TIME STEP AT (',I6,',',I2,') ==>  New timestep = ',1pE13.6)
!
 6003 FORMAT(//,' NO CONVERGENCE AFTER 25 REDUCTIONS OF TIME STEP SIZE',/,   &
     &          ' LAST TIME STEP WAS ',1pE13.6,' SECONDS ',//,   &
     &          ' !!!!!!!!!!  S T O P   E X E C U T I O N  !!!!!!!!!!')
 6004 FORMAT(' Write coefficients for semi-analytical heat loss calculation onto file "TABLE"')
!
 6005 FORMAT(4E20.13)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>    End of Timestep_Reduction
!
!
      RETURN
! 
      END SUBROUTINE Timestep_Reduction
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
      SUBROUTINE Upon_Convergence(NSTL,KIT,ConvergenceFailure,PrintNow_Flag,EOS_Specific_Flag)
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE EOS_Default_Parameters
         USE General_External_File_Units
! 
         USE General_Control_Parameters
         USE Time_Series_Parameters
! 
         USE Grid_Geometry, ONLY: elem
         USE Element_Attributes
         USE Sources_and_Sinks
! 
         USE Geologic_Media_Properties
         USE Solution_Matrix_Arrays
         USE MPICOM
         USE MADIM
         USE temp_arrays
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE FOR UPDATING PRIMARY VARIABLES AND SELECTING         *
!*          AFTER SUCCESSFUL CONVERGENCE OF THE NI ITERATIONS          *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
      INCLUDE 'mpif.h'
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(NumSS) :: previous_rate_factor
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)  :: NSTL
      INTEGER, INTENT(OUT) :: KIT
! 
      INTEGER :: N_up, EOS_Specific_Flag, n,item(5)
      REAL(KIND = 8)::rTem
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL, INTENT(INOUT) :: ConvergenceFailure
      LOGICAL, INTENT(OUT)   :: PrintNow_Flag
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call, N_up
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Upon_Convergence
!
      IF(First_call) THEN
         First_call = .FALSE.
         N_up       = (NumElem-1)*NumComPlus1 + 1
         IF(myid==iMaster)WRITE(UNIT = VERS_Unit, FMT = 6000)
      END IF
! 
! -------
! ... Reset the convergence flag to indicate convergence 
! -------
! 
      convergence = .TRUE.           
! 
! -------
! ... Initialize the convergence factors before adjustment 
! -------
! 
      previous_rate_factor(1:NumSS) = SS(1:NumSS)%RateFactor         
! 
! -------
! ... Compute semianalytical heat losses through bounding layers  
! -------
! 
      IF( Flag_SemianalyticHeatExchange ) CALL Analytical_Heat_Exchange
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                       ACTIVE ELEMENT LOOP                           >
!>       Compute Changes In Properties, Parameters And Variables       >
!>                        After Convergence                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! 
! ----------
! ... Compute changes in porosity: Array operations            
! ----------
! 
      ElemMedia(1:NumElemTot,previous)%porosity = ElemMedia(1:NumElemTot,current)%porosity  
! 
! ----------
! ... Update element pressures and temperatures
! ----------
!
      DO n = 1,NumElemTot
         ElemState(n)%pres(previous) = ElemState(n)%pres(current) 
         ElemState(n)%temp(previous) = ElemState(n)%temp(current) 
      END DO
! 
! ----------
! ... Check if well rates are to be internally adjusted
! ----------
!
      DO_SSCheck: DO n = 1,NumSS
!
! ...... Check if the source/sink has no pressure limits
!
         IF( SS(n)%RateResponse == 'NONE' .OR. SS(n)%RateResponse == 'None' .OR. SS(n)%RateResponse == 'none' ) CYCLE DO_SSCheck
!
         IF( SS(n)%RateFactor == 0.0d0 ) CYCLE DO_SSCheck
!
! ...... Otherwise ...
!
         IF(SS(n)%rate > 0) THEN
            CO(n) = SS(n)%PresLimit - ElemState(SS(n)%ElemNum)%pres(current)
         ELSE
            CO(n) = ElemState(SS(n)%ElemNum)%pres(current) - SS(n)%PresLimit
         END IF
!
! ...... Check if pressure exceeds the imposed limit
!
         IF_PressureLimits: IF( CO(n) < 0 ) THEN
!
!
            AdjustQ: SELECT CASE(SS(n)%RateResponse)
!
! ............ Check if the SS rate response is set to <STOP>
!
               CASE('STOP', 'Stop', 'stop') 
!
                  PressureLimitsExceeded = .TRUE.
                  iTem(1:1)             = n
                  GO TO 1000
!
! ............ Set updated rate to zero if the SS rate response is set to <ZERO>
!
               CASE('ZERO', 'Zero', 'zero') 
!
                  SS(n)%RateFactor = 0.0d0
!
! ............ Adjust rate if the SS rate response is set to <ADJU>
!
               CASE('ADJU', 'Adju', 'adju') 
!
                  IF(SS(n)%rate > 0) THEN
                     SS(n)%RateFactor = MIN(ABS(SS(n)%RateLimit), (SS(n)%RateFactor + ABS(SS(n)%RateStepChange)) )
                  ELSE
                     SS(n)%RateFactor = MAX(ABS(SS(n)%RateLimit), (SS(n)%RateFactor - ABS(SS(n)%RateStepChange)) )
                  END IF
!
            END SELECT AdjustQ        
!
!
         END IF IF_PressureLimits      
! <<<
! <<<
! <<<
      END DO DO_SSCheck

! 
! ----------
! ... Increment primary variables: CAREFUL! Array operations!   
! ----------
! 
 1000 X(1:NumUnknowns) = X(1:NumUnknowns) + DX(1:NumUnknowns)
!
!
!***********************************************************************
!*                                                                     *
!*             Store the state index in case of Dt cutback             *
!*                                                                     *
!***********************************************************************
! 
      ElemState%index(previous) = ElemState%index(current)   ! CAREFUL! Whole array operation
!
! -------
! ... Udpate current time
! -------
!
      time = time + TimeStep    ! The new current time
! 
! 
!***********************************************************************
!*                                                                     *
!*             SET NEW TIMESTEP; UPDATE OTHER PARAMETERS               *
!*                                                                     *
!***********************************************************************
! 
! 
      IF(SimulationTimeEnd /= 0.0d0 .AND. time == SimulationTimeEnd) THEN
         PrintOutNow = .TRUE.                                      
      END IF
! 
! ----------
! ... For internally-determined time step sizes      
! ----------
! 
      IF((NumUserTimeSteps == 0) .OR. (NumTimeSteps+1 > 8*NumUserTimeSteps .OR. UserTimeStep(NumTimeSteps+1) == 0.0d0)) THEN
! 
         IF_DtDouble: IF(NumNRIterations <= DoublingDtCriterion) THEN   ! If convergence within <DoublingDtCriterion> iterations, ...
            NewTimeStep = 2.0d0*TimeStep                                !    ... double time step, ...
         ELSE                                                           !    ... otherwise, ...
            NewTimeStep = TimeStep                                      !    ... use old time step 
         END IF IF_DtDouble
! 
! ----------
! ... For given (predetermined) time step sizes                   
! ----------
! 
      ELSE
         NewTimeStep = UserTimeStep(NumTimeSteps+1)           ! Use specified time step
      END IF
! 
! ----------
! ... Adjust Dt to ensure conformance to the maximum simulation period <SimulationTimeEnd>                  
! ----------
! 
      IF(SimulationTimeEnd > 0.0d0) NewTimeStep = MIN( NewTimeStep, SimulationTimeEnd-time )
! 
! ----------
! ... Adjust Dt to ensure conformance with max{Dt} = MaxTimeStep                
! ----------
! 
      IF(MaxTimeStep > 0.0d0) NewTimeStep = MIN(NewTimeStep,MaxTimeStep)
!
!***********************************************************************
!*                                                                     *
!*            Determine flags and counters for print-outs              *
!*                                                                     *
!***********************************************************************
!
      IF(NumNRIterations == 1) THEN    ! Count number of consecutive time steps that converge on num_NR_iter = 1
         KIT = KIT+1                      
      ELSE 
         KIT = 0                       ! For NumNRIterations > 1, do not consider
      END IF
! 
! -------
! ... Print messages if KIT >= 10  
! -------
! 
      IF(KIT >= 10) THEN  
         IF(myid==iMaster) WRITE(36,6001)
         ConvergenceFailure = .TRUE.   ! Set the flag indicating convergence failure
      END IF
!
!***********************************************************************
!*                                                                     *
!*             Print out important data upon convergence               *
!*                                                                     *
!***********************************************************************
!
      IF_Conver: IF(convergence .EQV. .TRUE.) THEN
! 
         IF_NST: IF(TrackElemNum /= 0) THEN
! 
            n=Track_Unit+myid
            IF(myid==iMaster) n=36

            WRITE(n, FMT = 6002) elem(TrackElemNum)%name, Tot_NumTimeSteps, NumNRIterations, time,    &
     &                           TimeStep, DX(NSTL+1), DX(NSTL+2),                                    &
     &                           ElemState(TrackElemNum)%temp(0), ElemState(TrackElemNum)%pres(0),    &
     &                           ElemProp(TrackElemNum,0)%satur(GasPhase)
! 
         ELSE
! 
            IF(myid==iMaster) THEN
            WRITE(36, FMT = 6002) elem(MaxResElemNum)%name, Tot_NumTimeSteps, NumNRIterations, time,                      &
     &                           TimeStep,DX( (MaxResElemNum-1)*NumComPlus1+1 ),DX( (MaxResElemNum-1)*NumComPlus1+2 ),   & 
     &                           ElemState(MaxResElemNum)%temp(0), ElemState(MaxResElemNum)%pres(0),                     &
     &                           ElemProp(MaxResElemNum,0)%satur(GasPhase) 
            END IF
! 
         END IF IF_NST
!
!***********************************************************************
!*                                                                     *
!*            Print out time-series data upon convergence              *
!*                                                                     *
!***********************************************************************
!
         IF_TimeS: IF( (G_ObsElem_Flag .EQV. .TRUE.) .OR. (G_ObsConx_Flag .EQV. .TRUE.) &
      &  .OR. (G_ObsSS_Flag .EQV. .TRUE.) ) THEN
             CALL Tabulate_TimeSeries_Data
         END IF IF_TimeS
! 
      END IF IF_Conver
!
!***********************************************************************
!*                                                                     *
!*              Determine whether printout is required                 *
!*                                                                     *
!***********************************************************************
!
      IF( (ConvergenceFailure) .OR.                                                   &
!kz     &    (PressureLimitsExceeded) .OR.                                               &  ! The well pressure limit (max or min) is exceeded
     &    ( (PrintOutNow) .AND. (convergence) ) .OR.                                  &
     &    ( (convergence) .AND. (MOD(Tot_NumTimeSteps, output_frequency) == 0) ) .OR. &
     &    (Tot_NumTimeSteps >= Max_NumTimeSteps) .OR.                                 &  
     &    (Option_OutputAmount >= 10)                                                 &
     &  ) PrintNow_Flag = .TRUE.
!
!***********************************************************************
!*                                                                     *
!*         Print warning prior to updating the rate factors            *
!*                                                                     *
!***********************************************************************
!
      DO_AdjustQ: DO n=1,NumSS
!
         IF( SS(n)%RateResponse == 'NONE' .OR. SS(n)%RateResponse == 'None' .OR. SS(n)%RateResponse == 'none' .OR. &
     &       SS(n)%RateResponse == 'STOP' .OR. SS(n)%RateResponse == 'Stop' .OR. SS(n)%RateResponse == 'stop'  ) CYCLE DO_AdjustQ
!
         SELECT CASE(SS(n)%RateResponse)
!
         CASE('ZERO', 'Zero', 'zero')
!
            IF_UpdateQ1: IF(previous_rate_factor(n) /= SS(n)%RateFactor) THEN
!
               IF(SS(n)%rate > 0.0d0) THEN
                  WRITE(36, FMT = 6005) elem(SS(n)%ElemNum)%name, SS(n)%PresLimit, SS(n)%name
               ELSE
                  WRITE(36, FMT = 6006) elem(SS(n)%ElemNum)%name, SS(n)%PresLimit, SS(n)%name
               END IF
!
               NewTimeStep = MAX(1.0d-1, NewTimeStep*1.0d-3)  ! Upon continuation, the new timestep is very small to avoid a possible shock 
!
            END IF IF_UpdateQ1
!
         CASE('ADJU', 'Adju', 'adju')
!
            IF( SS(n)%RateFactor == SS(n)%RateLimit ) SS(n)%RateResponse = 'STOP' ! If the rate reaches the limit, change response
!
            IF_UpdateQ2: IF(previous_rate_factor(n) /= SS(n)%RateFactor) THEN
!
               IF(SS(n)%rate > 0.0d0) THEN
                  WRITE(36, FMT = 6015) elem(SS(n)%ElemNum)%name, SS(n)%PresLimit, SS(n)%name, &
     &                                 previous_rate_factor(n), SS(n)%RateFactor
               ELSE
                  WRITE(36, FMT = 6016) elem(SS(n)%ElemNum)%name, SS(n)%PresLimit, SS(n)%name, &
     &                                 previous_rate_factor(n), SS(n)%RateFactor
               END IF
!
               NewTimeStep = MAX(1.0d-1, NewTimeStep*1.0d-3)  ! Upon continuation, the new timestep is very small to avoid a possible shock 
!
            END IF IF_UpdateQ2
!
         END SELECT
!
      END DO DO_AdjustQ
!
      rTem=NewTimeStep
      call MPI_ALLREDUCE(rTem, NewTimeStep, 1, MPI_DOUBLE_PRECISION,MPI_MIN, MPI_COMM_WORLD, ierr)
!
!
!***********************************************************************
!*                                                                     *
!*        Print warning prior to stopping if the well pressure         *
!*                       exceeds preset limits                         *
!*                                                                     *
!***********************************************************************
!
     IF(PressureLimitsExceeded) THEN
        IF(SS(iTem(1))%rate > 0.0d0) THEN
           WRITE(36, FMT = 6025) elem(SS(iTem(1))%ElemNum)%name, SS(iTem(1))%PresLimit, SS(iTem(1))%name
        ELSE
           WRITE(36, FMT = 6026) elem(SS(iTem(1))%ElemNum)%name, SS(iTem(1))%PresLimit, SS(iTem(1))%name
        END IF
     END IF
!
!***********************************************************************
!*                                                                     *
!*             Determine whether to update the SAVE file,              *
!*           as well as important computational parameters             *
!*                                                                     *
!***********************************************************************
!
      IF(SAVE_frequency > 0) THEN
!
         IF_CheckPrint: IF(MOD(Tot_NumTimeSteps, SAVE_frequency) == 0) THEN
!
! ......... First, print an interim SAVE file
!
            CALL Write_File_SAVE
! 
! -------------
! ......... Modify simulation control parameters without stopping execution
! -------------
! 
            IF(No_Update_PS>0) CALL Update_Simulation_Parameters
!
            IF( (Tot_NumTimeSteps >= Max_NumTimeSteps) .OR. (time >= SimulationTimeEnd) ) PrintNow_Flag = .TRUE.
!
         END IF IF_CheckPrint
!
      END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Upon_Convergence',T50,'[v1.0,  29 March     2007]',/,   &
     &         ':::::   Update primary variables and select timestep after convergence of the NI iterations (generic)') 
!
 6001 FORMAT(' !!!!!!!!!  FOR 10 CONSECUTIVE TIME STEPS HAVE CONVERGENCE ON <NumNRIterations> = 1',/,   &
     &       '       ==>  WRITE OUT CURRENT DATA, THEN STOP EXECUTION')
!
 6002 FORMAT(1X,A8,'(',I6,',',I2,')',' ST =',1pE12.5,' DT =',1pE12.5,' DX1= ',1pE12.5,' DX2= ',1pE12.5,' T = ',1pE11.4, &
     &             ' P = ',1pE13.6,' S = ',1pE12.5)
!
 6005 FORMAT(/,' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! ..........  The pressure in cell <',a8,'> exceeds the maximum level (=',1pE13.6,  &
     &         ' Pa) defined for source/sink <',a5,'>',/, &
     &         ' ! ..........  The rate factor is reset to zero, and the simulation continues (SS_response = <ZERO>)',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/)
!
 6006 FORMAT(/,' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! ..........  The pressure in cell <',a8,'> is below the minimum level (=',1pE13.6,  &
     &         ' Pa) defined for source/sink <',a5,'>',/, &
     &         ' ! ..........  The rate factor is reset to zero, and the simulation continues (SS_response = <ZERO>)',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/)
!
 6015 FORMAT(/,' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! ..........  The pressure in cell <',a8,'> exceeds the maximum level (=',1pE13.6,  &
     &         ' Pa) defined for source/sink <',a5,'>',/, &
     &         ' ! ..........  The rate factor is adjusted from',1pE13.6,' to',1pE13.6, &
     &         ', and the simulation continues (SS_response = <ADJU>)',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/)
!
 6016 FORMAT(/,' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! ..........  The pressure in cell <',a8,'> is below the minimum level (=',1pE13.6,  &
     &         ' Pa) defined for source/sink <',a5,'>',/, &
     &         ' ! ..........  The rate factor is adjusted from',1pE13.6,' to',1pE13.6, &
     &         ', and the simulation continues (SS_response = <ADJU>)',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/)
!
 6025 FORMAT(/,' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! ..........  The pressure in cell <',a8,'> exceeds the maximum level (=',1pE13.6,  &
     &         ' Pa) defined for source/sink <',a5,'>',/, &
     &         ' ! ..........  The results at the end of this time step are printed,', &
     &         ' and the simulation is halted (SS_response = <STOP>)',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/)
!
 6026 FORMAT(/,' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! ..........  The pressure in cell <',a8,'> is below the minimum level (=',1pE13.6,  &
     &         ' Pa) defined for source/sink <',a5,'>',/, &
     &         ' ! ..........  The results at the end of this time step are printed,', &
     &         ' and the simulation is halted (SS_response = <STOP>)',/, &
     &         ' ! >>>>>>>>>>  ',/, &
     &         ' ! >>>>>>>>>>  ',/)
!
 6200 FORMAT(8(1pe15.8,1x))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Upon_Convergence
!
!
      RETURN
! 
      END SUBROUTINE Upon_Convergence
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
      SUBROUTINE Update_Simulation_Parameters
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
         USE General_Control_Parameters
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE TO ALLOW MODIFICATION OF SIMULATION PARAMETERS        *
!*                 WITHOUT INTERRUPTION OF EXECUTION                   *
!*                                                                     *
!*                  Version 1.0 - February 15, 2007                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
      include 'mpif.h'
! 
!
! -------
! ... Integer variables
! -------
!
      INTEGER :: n,iDoUpdate
!
! -------
! ... Character variables
! -------
!
      CHARACTER (LEN = 28) :: UpdateHeader
      CHARACTER (LEN = 28) :: FirstLineUnit, FirstLine
!
! -------
! ... Logical variables
! -------
!
      LOGICAL :: First_call = .TRUE., exists
!
! -------
! ... Real arrays
! -------
!
      REAL(KIND = 8), DIMENSION(12) :: UpdatedInputs
!
! -------
!
! -------
! ... Namelists
! -------
!
      NAMELIST/Real_Parameters_To_Update/ SimulationTimeEnd, MaxTimeStep, rel_convergence_crit, abs_convergence_crit, &
     &                                    P_overshoot, T_overshoot, S_overshoot
!
      NAMELIST/Integer_Parameters_To_Update/ Max_NumTimeSteps, Max_NumNRIterations, DoublingDtCriterion, &
     &                                       SAVE_frequency, TimeSeriesPrint_frequency
!
! -------
! ... Saving variables
! -------
!
      SAVE First_call

! 
! 
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Update_Simulation_Parameters
!
!
      iDoUpdate=0
      IF(First_call) THEN
!
         First_call = .FALSE.
         NumParamUpdates = 0
!
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
!
      END IF
!
      IF(myid==iMaster) THEN
!
! -------
! ... Determine the status of the file <Parameter_Update_File>
! -------
! 
      INQUIRE(FILE='Parameter_Update_File', EXIST = exists)         
!
      IF(exists) THEN                                                   ! If it already exists, ...
         OPEN(Parameter_Update_Unit, FILE='Parameter_Update_File')      !    open it as an old file and ...
      ELSE                                                              ! Otherwise, ...
         GOTO 888                               
      END IF     
!
! -------
! ... Check if important simulation parameters are to be updated
! -------
!
      READ(UNIT = Parameter_Update_Unit, FMT = *, IOSTAT = n) UpdateHeader
!
      IF(n < 0) THEN
         CLOSE (UNIT = Parameter_Update_Unit)
         GOTO 888  
       END IF
!
! ... Check heading first 
!
      IF_Update: IF(UpdateHeader == 'Update_Simulation_Parameters' .OR. UpdateHeader == 'update_simulation_parameters' .OR. &
     &              UpdateHeader == 'UPDATE_SIMULATION_PARAMETERS') THEN
!
! ...... Read real parameters 
!
         READ(UNIT = Parameter_Update_Unit, NML = Real_Parameters_To_Update, IOSTAT = n)
!
         IF(n < 0) THEN
            CLOSE (UNIT = Parameter_Update_Unit)                                       
            GOTO 888 
         END IF
         iDoUpdate=1
         UpdatedInputs(1)= SimulationTimeEnd
         UpdatedInputs(2)= MaxTimeStep
         UpdatedInputs(3)= rel_convergence_crit
         UpdatedInputs(4)= abs_convergence_crit
         UpdatedInputs(5)= P_overshoot
         UpdatedInputs(6)= T_overshoot
         UpdatedInputs(7)= S_overshoot
!
! ...... Read integer parameters 
!
         READ(UNIT = Parameter_Update_Unit, NML = Integer_Parameters_To_Update, IOSTAT = n)
!
         CLOSE (UNIT = Parameter_Update_Unit)                                       
!
         IF(n < 0) THEN
          GOTO 888 
         END IF
         iDoUpdate=2
         UpdatedInputs(8)= Max_NumTimeSteps*1.0
         UpdatedInputs(9)= Max_NumNRIterations*1.0
         UpdatedInputs(10)= DoublingDtCriterion*1.0
         UpdatedInputs(11)= SAVE_frequency*1.0
         UpdatedInputs(12)= TimeSeriesPrint_frequency*1.0
!
         WRITE(36, FMT = 6010) NumParamUpdates,                                                            &
     &                        SimulationTimeEnd, MaxTimeStep, rel_convergence_crit, abs_convergence_crit, &
     &                        Max_NumTimeSteps, Max_NumNRIterations, DoublingDtCriterion, SAVE_frequency, TimeSeriesPrint_frequency
!
! ...... Modify the update file as a DIRECT access file (allows over-writing info in the file)
!
         OPEN(Parameter_Update_Unit, FILE='Parameter_Update_File', ACCESS = 'DIRECT', RECL = 28)
!
         WRITE(UNIT = FirstLineUnit, FMT = 6005)  NumParamUpdates  !    Use internal file to store the modified 1st line as Character + Integer ...
!
         READ (UNIT = FirstLineUnit, FMT = 6006)  FirstLine        !    Retrieve the 1st line as a character variable ...
!
! ...... Modify the top line of the update file to avoid accidentally affecting future or continuation simulations
!
         WRITE (UNIT = Parameter_Update_Unit, REC = 1) FirstLine
!
      END IF IF_Update
!
      CLOSE (UNIT = Parameter_Update_Unit)
! 
     END IF                          !myid==iMaster
!
  888   call MPI_BCAST(iDoUpdate, 1, MPI_INTEGER, iMaster, MPI_COMM_WORLD, ierr)
        IF(iDoUpdate==0)   RETURN  
        IF(iDoUpdate==1) THEN
         call MPI_BCAST(UpdatedInputs(1:7), 7, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr) 
        END IF
        IF(iDoUpdate==2) THEN
         call MPI_BCAST(UpdatedInputs, 12, MPI_DOUBLE_PRECISION, iMaster, MPI_COMM_WORLD, ierr)
        END IF
         IF(UpdatedInputs(1) > 1.0d-16) SimulationTimeEnd    = UpdatedInputs(1)
         IF(UpdatedInputs(2) > 1.0d-16) MaxTimeStep          = UpdatedInputs(2)
         IF(UpdatedInputs(3) > 1.0d-16) rel_convergence_crit = UpdatedInputs(3)
         IF(UpdatedInputs(4) > 1.0d-16) abs_convergence_crit = UpdatedInputs(4)
         IF(UpdatedInputs(5) > 1.0d-16) P_overshoot=UpdatedInputs(5)
         IF(UpdatedInputs(6) > 1.0d-16) T_overshoot=UpdatedInputs(6)
         IF(UpdatedInputs(7) > 1.0d-16) S_overshoot=UpdatedInputs(7)
!
        IF(iDoUpdate==2) THEN
         IF(UpdatedInputs(8)>0.1)     Max_NumTimeSteps=int(UpdatedInputs(8)+0.1)
         IF(UpdatedInputs(9)>0.1)     Max_NumNRIterations=int(UpdatedInputs(9)+0.1)
         IF(UpdatedInputs(10)>0.1)    DoublingDtCriterion=int(UpdatedInputs(10)+0.1)
         IF(UpdatedInputs(11)>0.1)    SAVE_frequency=int(UpdatedInputs(11)+0.1)
         IF(UpdatedInputs(12)>0.1)    TimeSeriesPrint_frequency=int(UpdatedInputs(12)+0.1)
       END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Update_Simulation_Parameters',T50,'[v1.0,  15 February  2007]',/,   &
     &         ':::::   Update simulation contol parameters without interrupting execution')
!
 6005 FORMAT('==> Completed Update #',i3,'   ')
 6006 FORMAT(a28)
!
 6010 FORMAT(//,' ! >>>>>>>>>>  ',/, &
     &          ' ! >>>>>>>>>>  ',/, &
     &          ' ! ..........  UPDATING SIMULATION CONTROL PARAMETERS - Update #',i3, /, &
     &          ' ! ..........  ',/,   &
     &          ' ! ..........       Length of simulation period <SimulationTimeEnd>              =',1pe12.5,/,   &
     &          ' ! ..........       Maximum allowable timstep size <MaxTimeStep>                 =',1pe12.5,/,   &
     &          ' ! ..........       Relative convergence criterion <rel_convergence_crit>        =',1pe12.5,/,   &
     &          ' ! ..........       Absolute convergence criterion <abs_convergence_crit>        =',1pe12.5,/,   &
     &          ' ! ..........       P-overshooting allowed to trigger phase change <P_overshoot> =',1pe12.5,' (fraction)',/, &
     &          ' ! ..........       T-overshooting allowed to trigger phase change <T_overshoot> =',1pe12.5,' (fraction)',/, &
     &          ' ! ..........       S-overshooting allowed to trigger phase change <S_overshoot> =',1pe12.5,' (fraction)',/, &
     &          ' ! ..........  ',/,   &
     &          ' ! ..........       Maximum number of timesteps <Max_NumTimeSteps>                              =',i7,/,   &
     &          ' ! ..........       Maximum number of NR iterations per timestep <Max_NumNRIterations>          =',i7,/,   &
     &          ' ! ..........       Number of NR iteration to convergence for doubling Dt <DoublingDtCriterion> =',i7,/,   &
     &          ' ! ..........       Printing frequency of SAVE file <SAVE_frequency>                            =',i7,/,   &
     &          ' ! ..........       Printing frequency of time series files <TimeSeries_frequency>              =',i7,/,   &
     &          ' ! ..........  ',/,   &
     &          ' ! >>>>>>>>>>  ',/, &
     &          ' ! >>>>>>>>>>  ',//)

!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Update_Simulation_Parameters
!
!
      RETURN
! 
      END SUBROUTINE Update_Simulation_Parameters
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Adjust_Timestep_for_Printout
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
         USE General_Control_Parameters
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*        ROUTINE MODIFYING THE TIME-STEP SIZE TO CONFORM WITH         *
!*     USER-SUPPLIED INPUT TIMES AT WHICH PRINT-OUTS ARE DESIRED       *
!*                                                                     *
!*                   Version 1.0 - January 4, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: i
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Adjust_Timestep_for_Printout
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
! 
! -------
! ... Check if the current time exceeds the maximum print-out time 
! -------
! 
      IF(OutputTimes(NumPrintTimes) <= time)  RETURN     ! Done ! Exit the routine
! 
! 
!***********************************************************************
!*                                                                     *
!*                        PRINT-OUT TIME LOOP                          *
!*                                                                     *
!***********************************************************************
! 
! 
      DO_NPTimes: DO  i=1,NumPrintTimes
! 
! ----------
! ...... If the desired print-out time is reached
! ----------
! 
         IF_ChkTIS: IF(OutputTimes(i) == time) THEN
! 
            IF(TimeStepMax_PostPrint > 0.0d0) NewTimeStep = MIN(NewTimeStep,TimeStepMax_PostPrint)  ! Obtain an estimate for the new Dt
!
! ......... Check the Dt estimate against the next print-out time
!
            IF( OutputTimes(i+1) > OutputTimes(i) + NewTimeStep ) THEN             ! If Dt < OutputTimes(I+1) - OutputTimes(I)
               RETURN                                                              !    Done ! Exit the routine
            ELSE                                                                   ! Otherwise
               NewTimeStep = MIN( NewTimeStep, OutputTimes(i+1)-OutputTimes(i) )   !    Readjust Dt
            END IF
!
            PrintOutNow = .TRUE.                  ! Reset the print-out flag                     
!
            RETURN                                ! Done ! Exit the routine
! 
! ----------
! ...... If the desired print-out time has already been passsed
! ----------
! 
         ELSE IF(OutputTimes(i) < time) THEN
! 
            CYCLE                                 ! Continue the loop                       
! 
! ----------
! ...... If the desired print-out time has not been passsed
! ----------
! 
         ELSE 
!
            IF(time + NewTimeStep >= OutputTimes(i)) THEN   ! If Dt results in current t > PrintTime(I), ...
               NewTimeStep = OutputTimes(i) - time          ! ... Dt is adjusted and
               PrintOutNow = .TRUE.                         ! ... the print-out flag is reset
            END IF
!
            RETURN                           ! Done ! Exit the routine
! 
         END IF IF_ChkTIS
!
! <<<                      
! <<< End of the PRINT-OUT TIME LOOP         
! <<<
!
      END DO DO_NPTimes
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Adjust_Timestep_for_Printout',T50,'[v1.0,   4 November  2006]',/,   &
     &         ':::::   Adjust time steps to conform with user-defined times for a print-out')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Adjust_Timestep_for_Printout
!
!
      RETURN
! 
      END SUBROUTINE Adjust_Timestep_for_Printout
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
      SUBROUTINE Solve_Jacobian_Matrix_Equation
!
! ...... Modules to be used
!
      USE Basic_Parameters
      USE General_External_File_Units
      USE Solution_Matrix_Arrays
      USE General_Control_Parameters
      USE MADIM
      USE VBR_Matrix
      USE UPDATEINFO
      USE AZ_ME_para
      USE MPICOM
      USE solver_ps        
!
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE CALLING THE SOLVERS FOR THE JACOBIAN MATRIX EQUATION    *
!*                                                                     *
!*           PARALLEL COMPUTING  Version 1.0 - May  24, 2007           *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
      include 'mpif.h'
      include "az_aztecf.h"

!-----THIS SUBROUTINE CALLS THE PARALLEL LINEAR EQUATION SOLVER.
!     IT HAS LOGIC TO HANDLE FAILURES IN LINEAR EQUATION SOLUTION.
!
!     AFTER SOLUTION, LATEST UPDATED ITERATES ARE OBTAINED FOR
!     ALL PRIMARY DEPENDENT VARIABLES.
!
      integer itemp,i,j
      real*8 status(100),dxm(10)
      real*8:: HDMAX, rTemp,EPSLN1,EPSLN2,ES,DSMAX,TS,TSS,TT
      INTEGER :: ICALL = 0
      SAVE ICALL
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Solve_Jacobian_Matrix_Equation
!
!
      ICALL = ICALL+1
!
      IF(ICALL == 1) THEN
!
      itotitr=0
      IF(myid==iMaster)   WRITE(VERS_Unit,6000)
!
      END IF

      RELAXF=1.0

!     Initialize xr for each processor
        xr = 0.0d0
!
         IF(Option_Print_SolverInfo /= 0) CALL CPU_Elapsed_Time(0,TS,TT)
!
      call AZ_solve(xr, b, options, params, indx, bindx,      & 
     &       rpntr, cpntr, bpntr, aval, data_org,             &
     &       status, proc_config)

!
         IF(Option_Print_SolverInfo /= 0) THEN
            CALL CPU_Elapsed_Time(1,TSS,TT)
            IF(myid==iMaster) WRITE(36,6040)  TSS
         END IF

           itotitr = itotitr+status(AZ_its+1)

!      if (status(AZ_why+1) .eq. AZ_breakdown) then
!         if (myid .eq. iMaster) then
!            write(36,*)'*** Numerical breakdown in AZ_SOLVE ***'
!        end if
!      end if

      iteru = status(AZ_its+1)

! ----------
! ...... Update and print iteration statistics
! ----------
!
         iteruc = iteruc+iteru
!
         IF(myid==iMaster) WRITE(LINEQ_Unit,6045) Tot_NumTimeSteps, NumNRIterations, TimeStep, iteru, iteruc 
!
      itemp = 0
      R=0.0
      do i=1, NumElem
        do j=1, NumEqu
          itemp = itemp + 1
          R( itemp ) =  xr( cpntr(i) + j )
        enddo
      enddo

! ... for underrelaxation scheme, ysw 12/1/95

      if(icoley.eq.1) then

      HDMAX=MAXVAL(R)
      rTemp=MINVAL(R)
      if ((rtemp+HDMAX)<0.0) HDMAX=rTemp      

          EPSLN2=HDMAX
          IF( NumNRIterations==1) EPSLN1=EPSLN2
!.......  RESET RELAXATION FACTOR, RELAXF
          ES=1.
          IF(ABS(EPSLN1).GE.1.E-10) ES=EPSLN2/(EPSLN1*RELAXF)
            IF(ES.LT.-1.0) THEN
              RELAXF=0.5/(ABS(ES))
            ELSE
              RELAXF=(3.+ES)/(3.+ABS(ES))
            ENDIF
          EPSLN1=EPSLN2
          RELAXF=min(RELAXF,1.d0)
      elseif(icoley.eq.2) then
        do i=1,NumComPlus1 
          dxm(i)=0.0
        enddo
!   search for largest changes
        do i=1, NumElem 
          do j=1, NumEqu
            rtemp =  xr( cpntr(i) + j )
            dxm(j) = max(dxm(j),abs(rtemp))
          enddo
        enddo
!
          dsmax=max(dxm(2),1.d-10)
          RELAXF=DSTNOM/DSMAX
          RELAXF=min(RELAXF,1.d0)
      endif

      if ((icoley==1).or. (icoley==2)) then
       rTemp=RELAXF
      call MPI_Allreduce(rTemp, RELAXF, 1, MPI_DOUBLE_PRECISION,   &
     &                   MPI_MIN, MPI_COMM_WORLD, ierr)
      end if 

!***********************************************************************
!
!
 6000 FORMAT(/,'Solve_Jacobian_Matrix_Equation',T50,'[v2.0   May 24  2007]',/,   &
     &         ':::::   Solves the Jacobian matrix equation through an interface with linear equation solvers ',/,   &
     &         '        Can call AZTEC parallel iterative solvers')
!
 6040 FORMAT('      SOLUTION TIME = ',1PE12.5,'  SECONDS')
 6045 FORMAT(T5,' At [',I4,',',I3,']',' <NewTimeStep> =',1pE12.5,' IT=',I5,' ITC=',I10)
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Solve_Jacobian_Matrix_Equation
!
!
      RETURN
!
      END SUBROUTINE Solve_Jacobian_Matrix_Equation
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************!
!
!
      SUBROUTINE Print_Secondary_Variables
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
         USE General_Control_Parameters, ONLY: Option_Print_EOSInfo
!
         USE Grid_Geometry, ONLY: elem
         USE Element_Attributes
         USE Solution_Matrix_Arrays, ONLY: DELX
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*          ROUTINE FOR PRINTING ALL THE SECONDARY PARAMETERS          *
!*                                                                     *
!*                  Version 1.00, September 23, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision local arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(NumSecondaryVar) :: DP
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: NLOC,n,k,m,i,kk
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Print_Secondary_Variables
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         if(myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
!
! ... Write a heading
!
      WRITE(36,6001)                                                 
! 
! 
!***********************************************************************
!*                                                                     *
!*                          MAIN ELEMENT LOOP                          *
!*                                                                     *
!***********************************************************************
! 
! 
      DO_NoElem: DO n = 1,NumElemTot                                                     
! 
! -------------
! ......... Print parameters at state point        
! -------------
! 
         WRITE(36,6002)  elem(n)%name,   &
     &                 (ElemProp(n,0)%satur(i),                                   &
     &                  ElemProp(n,0)%density(i),                                 &
     &                  ElemProp(n,0)%enthalpy(i),                                &
     &                 (ElemProp(n,0)%MassFrac(m,i),m=1,NumCom),i=1,NumPhases),   &             
     &                 (ElemProp(n,0)%RelPerm(i),                                 &
     &                  ElemProp(n,0)%viscosity(i),                               &
     &                  ElemProp(n,0)%CapPres(i),i=1,NumMobPhases),               &             
     &                  ElemState(n)%temp(0),                                     &           
     &                 (ElemProp(n,0)%MoreData(i),i=1,NumAuxParam)           
!
! -------------
!  ........ Print incremented parameters        
! -------------
! 
         IF_MOPx: IF(Option_Print_EOSInfo == 8) THEN                                   
!                                                                       
            DO_NEq_1: DO k=2,NumEquPlus1                                                  
               WRITE(36,6003)  (ElemProp(n,k-1)%satur(i),                                  &
     &                         ElemProp(n,k-1)%Density(i),                                 &
     &                         ElemProp(n,k-1)%enthalpy(i),                                &
     &                        (ElemProp(n,k-1)%MassFrac(m,i),m=1,NumCom),i=1,NumPhases),   &             
     &                        (ElemProp(n,k-1)%RelPerm(i),                                 &
     &                         ElemProp(n,k-1)%viscosity(i),                               &
     &                         ElemProp(n,k-1)%CapPres(i),i=1,NumMobPhases),               &             
     &                         ElemState(n)%temp(k-1),                                     &
     &                        (ElemProp(n,k-1)%MoreData(i),i=1,NumAuxParam)            
            END DO DO_NEq_1                                                      
! 
! -------------
!  ........ Print derivatives        
! -------------
! 
         ELSE                                                         
!                                                                       
            NLOC = Locp(n)    ! = (n-1)*NumComPlus1                                                  
!                                                                       
            DO_1: DO k=2,NumEquPlus1                                                  
!                                                                     
               kk = 0
!                                                                     
! ............ Basic parameters                                                                     
!                                                                     
               DO_2: DO i=1,NumPhases                                               
!                                                                     
                  kk = kk+1
                  DP(kk) = ( ElemProp(n,k-1)%satur(i) - ElemProp(n,0)%satur(i) )/DELX(NLOC+K-1)
!                                                                     
                  kk = kk+1
                  DP(kk) = ( ElemProp(n,k-1)%density(i) - ElemProp(n,0)%density(i) )/DELX(NLOC+K-1)
!                                                                     
                   kk = kk+1
                   DP(kk) = ( ElemProp(n,k-1)%enthalpy(i) - ElemProp(n,0)%enthalpy(i) )/DELX(NLOC+K-1)
! ............... Parameters related to mass fractions of components in phases                                                                  
!                                                                     
                  DO_2a: DO m=1,NumCom                                               
                     kk = kk+1
                     DP(kk) = ( ElemProp(n,k-1)%MassFrac(m,i) - ElemProp(n,0)%MassFrac(m,i) )/DELX(NLOC+K-1)
                  END DO DO_2a                                                      
!
               END DO DO_2                                                      
!                                                                     
! ............ Parameters related to phase mobility                                                                   
!                                                                     
               DO_3: DO i=1,NumMobPhases                                               
!                                                                     
                  kk = kk+1
                  DP(kk) = ( ElemProp(n,k-1)%RelPerm(i) - ElemProp(n,0)%RelPerm(i) )/DELX(NLOC+K-1)
!                                                                     
                  kk = kk+1
                  DP(kk) = ( ElemProp(n,k-1)%viscosity(i) - ElemProp(n,0)%viscosity(i) )/DELX(NLOC+K-1)
!                                                                     
                  kk = kk+1
                  DP(kk) = ( ElemProp(n,k-1)%CapPres(1) - ElemProp(n,0)%CapPres(1) )/DELX(NLOC+K-1)
!                                                                     
! ............... Diffusion-related parameters                                                                    
!                                                                     
                  IF_BinDif: IF(Flag_BinaryDiffusion) THEN
!                                                                     
                     kk = kk+1
                     DP(kk) = ( ElemProp(n,k-1)%DiffCoA(i) - ElemProp(n,0)%DiffCoA(i) )/DELX(NLOC+K-1)
!                                                                     
                     kk = kk+1
                     DP(kk) = ( ElemProp(n,k-1)%DiffCoB(i) - ElemProp(n,0)%DiffCoB(i) )/DELX(NLOC+K-1)
!                                                                     
                  END IF IF_BinDif
!
               END DO DO_3                                                     
!                                                                     
! ............ Temperature                                                                    
!                                                                     
               kk = kk+1
               DP(kk) = ( ElemState(n)%temp(k-1) - ElemState(n)%temp(0) )/DELX(NLOC+K-1)
!                                                                     
! ............ Additional parameters                                                                      
!                                                                     
               DO_4: DO i=1,NumAuxParam                                               
                  kk = kk+1
                  DP(kk) = ( ElemProp(n,k-1)%MoreData(i) - ElemProp(n,0)%MoreData(i) )/DELX(NLOC+K-1)
               END DO DO_4                                                      
!                                                                       
               WRITE(36,6003) (DP(m),m=1,kk)     !NumSecondaryVar)                          
!                                                                       
            END DO DO_1                                                      
!                                                                       
         END IF IF_MOPx                                                         
!                                                                       
      END DO DO_NoElem                                                        
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
 6000 FORMAT(/,'Print_Secondary_Variables',T50,'[v1.0,  23 August    2006]',/,   &
     &         ':::::   Routine for printing all secondary parameters (stored in the derived-type array "cell")')
!                                                                       
 6001 FORMAT(/,' Secondary parameters')                                  
 6002 FORMAT(/,' Element ',A8,/,(10(1X,1pE12.5)))                          
 6003 FORMAT(/,(10(1X,1pE12.5)))                                         
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Print_Secondary_Variables
!
!
      RETURN
!
      END SUBROUTINE Print_Secondary_Variables
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
      SUBROUTINE Write_File_SAVE
! 
! ...... Modules to be used 
! 
         USE EOS_Default_Parameters
         USE Basic_Parameters
         USE General_External_File_Units

         USE General_Control_Parameters
! 
         USE Grid_Geometry
         USE Element_Attributes
! 
         USE Geologic_Media_Properties
         USE HydraulicAndThermal_Properties
!
         USE Sources_and_Sinks
         USE Solution_Matrix_Arrays, ONLY: X
!
         USE MPICOM
         USE MADIM
         USE TagN
         USe temp_arrays
         USE UPDATEINFO
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE GENERATING A FILE <SAVE> TO BE USED FOR RESTARTING      *
!*                                                                     *
!*                   Version 1.0 - January 5, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
      include "mpif.h"
      REAL(KIND = 8) :: solid_phase_sat, perm_factor
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: n,i,n2,pid,iJ
      integer ::stat(MPI_STATUS_SIZE)
      integer EQ, NN1, locNEL
 
      character*8, dimension(:), allocatable:: locELEM
      real*8, dimension(:), allocatable:: locX,locPHI
      real*8, dimension(:,:), allocatable:: locPerm

      real*8::rTem(6)
      integer(KIND=1),dimension(:), allocatable:: locST_indx,gloST_indx 
      integer(KIND=2)::NMat
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Write_File_SAVE
!
!
      IF(myid==iMaster) THEN
        allocate(ELEM0(MNEL))
        allocate(por0(MNEL))
        allocate(perm0(3,MNEL))
        allocate(xx0(MNEL*NumComPlus1))
        allocate(gloST_indx(MNEL))
        allocate(indx0(MaxNEL))
        allocate(locELEM(MaxNEL))
        allocate(locPHI(MaxNEL))
        allocate(locPerm(3,MaxNEL))
        allocate(locX(MaxNEL*NumComPlus1))
        allocate(locST_indx(MaxNEL))
      ELSE
        allocate(indx0(NumElem))
        allocate(locELEM(NumElem))
        allocate(locPHI(NumElem))
        allocate(locPerm(3,NumElem))
        allocate(locX(NumElem*NumComPlus1))
        allocate(locST_indx(NumElem))
      END IF

         IF(ElemNameLength /= 8) THEN
           iJ=6004
         ELSE
           iJ=6003
         END IF
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
! 
      IF(myid==iMaster) WRITE(36,6001) Tot_NumTimeSteps,time      ! Write time and timestep into main output file
! 
! -------
! ... Associate and print data into file SAVE
! -------
! 
      IF(myid==iMaster) THEN
       REWIND (UNIT = SAVE_Unit)                      ! Open file SAVE
       WRITE(SAVE_Unit,6002) MNEL,time          ! Print headings
      END IF

       DO n=1,NumElemTot
        DO i=1,NumComPlus1
         IF(abs(X((n-1)*NumComPlus1+i))<1.0d-60) X((n-1)*NumComPlus1+i)=0.0
        END DO
       END DO
! 
! -------
! ... Print primary variables into file SAVE
! -------
!
!    Create index: Global number for each element
      DO NN1 = 1, NumElem
         N = update_index(NN1)+1
        do EQ = 1, NumComPlus1
         locX((NN1-1)*NumComPlus1+EQ) = X((N-1)*NumComPlus1+EQ)
         if (abs(locX((NN1-1)*NumComPlus1+EQ))<1.0d-50) locX((NN1-1)*NumComPlus1+EQ)=0.d0
        end do
         locELEM(NN1) = elem(n)%name
         locPHI(NN1) = ElemMedia(n,current)%porosity
         locST_indx(NN1) = ElemState(n)%Index(current)
         indx0(NN1) = update(NN1) + 1
!
            IF( element_by_element_properties .OR. porosity_perm_dependence ) THEN
!
! ............ Adjust the printed permeability to remove the effect of the solid phases in EPM
!
               IF_EPM1: IF(Option_SolidPhasesEffect /= 0 .AND. Option_SolidPhasesEffect /= 9) THEN
                  solid_phase_sat = SUM(ElemProp(n,0)%satur(NumMobPhases+1:NumPhases))
                  NMat=elem(n)%MatNum 
                  perm_factor     = Permeability_Factor(Option_SolidPhasesEffect, NMat, solid_phase_sat, VERS_Unit)
                  locPerm(1:3,NN1)= ElemMedia(n,current)%Perm(1:3)/(perm_factor + 1.0d-50)
               ELSE
                  locPerm(1:3,NN1)= ElemMedia(n,current)%Perm(1:3)
               END IF IF_EPM1
            END IF
      END DO

          call MPI_Reduce(CumValue(1:6), rTem(1:6), 6, &
     &    MPI_DOUBLE_PRECISION,MPI_SUM, iMaster, MPI_COMM_WORLD, ierr)
          
!
      if (myid .NE. iMaster) then
!        Send X and ELEM and the correct global indices to proc 0
 
         call MPI_SEND(NumElem,1, MPI_INTEGER, iMaster,tag(1)+myid, MPI_COMM_WORLD, ierr)
         call MPI_SEND(indx0, NumElem, MPI_INTEGER,iMaster,tag(2)+myid, MPI_COMM_WORLD, ierr)
         call MPI_SEND(locELEM, NumElem*8, MPI_CHARACTER,iMaster,tag(3)+myid, MPI_COMM_WORLD, ierr)
         call MPI_SEND(locPHI, NumElem, MPI_DOUBLE_PRECISION,iMaster,tag(4)+myid, MPI_COMM_WORLD, ierr) 
         call MPI_SEND(locX, NumElem*NumComPlus1, MPI_DOUBLE_PRECISION,iMaster,tag(5)+myid, MPI_COMM_WORLD, ierr) 
         call MPI_SEND(locST_indx, NumElem, MPI_BYTE,iMaster,tag(6)+myid, MPI_COMM_WORLD, ierr)
         call MPI_SEND(locPerm, NumElem*3, MPI_DOUBLE_PRECISION,iMaster,tag(7)+myid, MPI_COMM_WORLD, ierr)         

      else
!        Store X and ELEM in globX and globELEM for processor 0
         DO N = 1, NumElem
            DO EQ = 1,NumComPlus1 
               xx0((indx0(N)-1)*NumComPlus1 + EQ) = locX((N-1)*NumComPlus1 + EQ)
            END DO
            ELEM0(indx0(N)) = locELEM(N)
            por0(indx0(N)) = locPHI(N)
            gloST_indx(indx0(N)) = locST_indx(N) 
            perm0(1:3,indx0(N)) = locPerm(1:3,N)
         END DO
!         RECEIVE X, ELEM and their global indices from each
!         processor and store X and ELEM at correct global indices of
!         globX and globELEM.
         DO pid = 0, nprocs-1
          IF(pid /=iMaster) THEN
            call MPI_RECV(locNEL,1, MPI_INTEGER, pid,tag(1)+pid,MPI_COMM_WORLD, stat, ierr)
            call MPI_RECV(indx0,locNEL, MPI_INTEGER, pid,tag(2)+pid,MPI_COMM_WORLD, stat, ierr)
            call MPI_RECV(locELEM,locNEL*8, MPI_CHARACTER, pid,tag(3)+pid,MPI_COMM_WORLD, stat, ierr)
            call MPI_RECV(locPHI,locNEL, MPI_DOUBLE_PRECISION, pid,tag(4)+pid,MPI_COMM_WORLD, stat, ierr)
            call MPI_RECV(locX, locNEL*NumComPlus1,MPI_DOUBLE_PRECISION, pid, tag(5)+pid,MPI_COMM_WORLD, stat,ierr)
            call MPI_RECV(locST_indx,locNEL, MPI_BYTE, pid,tag(6)+pid,MPI_COMM_WORLD, stat, ierr)
            call MPI_RECV(locPerm(1:3,1:locNEL),locNEL*3,MPI_DOUBLE_PRECISION,pid,tag(7)+pid,MPI_COMM_WORLD,stat,ierr)
 
            DO N = 1, locNEL
               DO EQ = 1, NumComPlus1
                  xx0((indx0(N)-1)*NumComPlus1+EQ) = locX((N-1)*NumComPlus1+EQ)
               END DO
               ELEM0(indx0(N)) = locELEM(N)
               por0(indx0(N)) = locPHI(N)
               gloST_indx(indx0(N)) = locST_indx(N)
               perm0(1:3,indx0(N))=locPerm(1:3,N)
           END DO
          END IF
         END DO
 
       DO_NumEle: DO n=1,MNEL
         n2 = gloST_indx(n)
            IF( element_by_element_properties .OR. porosity_perm_dependence ) THEN
         IF( IJ==6003) WRITE(SAVE_Unit,6013)  ELEM0(n), por0(n), State_name(n2), EOS_variables(n2),(perm0(i,n),i=1,3), &
     &                         (xx0((n-1)*NumComPlus1+i),i=1,NumComPlus1)
         IF( IJ==6004) WRITE(SAVE_Unit,6014)  ELEM0(n), por0(n), State_name(n2), EOS_variables(n2),(perm0(i,n),i=1,3), &
     &                         (xx0((n-1)*NumComPlus1+i),i=1,NumComPlus1)
            ELSE
         IF( IJ==6003) WRITE(SAVE_Unit,6003)  ELEM0(n), por0(n), State_name(n2), EOS_variables(n2), &
     &                         (xx0((n-1)*NumComPlus1+i),i=1,NumComPlus1)
         IF( IJ==6004) WRITE(SAVE_Unit,6004)  ELEM0(n), por0(n), State_name(n2), EOS_variables(n2), &
     &                         (xx0((n-1)*NumComPlus1+i),i=1,NumComPlus1)
           END IF

       END DO DO_NumEle
! -------
! ... Print continuation data into file SAVE using a namelist
! -------
!
      WRITE(UNIT = SAVE_Unit, FMT = 6350)     ! Print continuation keyword ':::'
!
      WRITE(UNIT = SAVE_Unit, FMT = 6400, IOSTAT = ierr)  Tot_NumTimeSteps, Tot_NumNRIterations, NumMedia, TimeOrigin, time, &
     &                                                  (rTem(n), SavedVariable(n), n=1,6)
!
! ... Stop if there is a problem writing the namelist
!
      IF( ierr /= 0 ) THEN
         WRITE (36, FMT = 6500)
         CALL STOP_ALL
      END IF
!
      WRITE(UNIT = SAVE_Unit, FMT = 6450)     ! Print continuation keyword ':::'
!
! -------
! ... Write the source/sink rate adjustment factors in a namelist format (Useful, but NOT read in data block or subroutine <INCON>)
! -------
!
      IF_UpdateQ: IF(NumSS > 0) THEN
!
         WRITE(UNIT = SAVE_Unit, FMT = 6505)
!
         WRITE(UNIT = SAVE_Unit, FMT = 6510) (i, SS(i)%RateFactor, i = 1,NumSS)
!
      END IF IF_UpdateQ
!
      ENDFILE (UNIT = SAVE_Unit)  ! Close the SAVE file
!
       DEALLOCATE(ELEM0,por0,xx0,gloST_indx,perm0)
      END IF
!
      DEALLOCATE(indx0,locELEM,locPHI,locX,locST_indx,locPerm)    
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Write_File_SAVE',T50,'[v1.0,  14 January   2008]',/,   &
     &         ':::::   At the completion of a TOUGH+ run, write primary variables into file "SAVE"')
!
 6001 FORMAT(/,' >>>>> Write file "SAVE" after ',i7,' time steps  ==>  The time is ',1pe13.6,' seconds'/)
 6002 FORMAT('INCON: Initial conditions for ',i7,' elements at time ',1pE14.7)
!
 6003 FORMAT(A8, 7X,1pE15.8,2x,a3,a35,/,6(1pE20.13))
 6004 FORMAT(A5,10X,1pE15.8,2x,a3,a35,/,6(1pE20.13))
!
 6013 FORMAT(A8, 7X,1pE15.8,2x,a3,a35,1x,3(e15.8),/,6(1pE20.13))
 6014 FORMAT(A5,10X,1pE15.8,2x,a3,a35,1x,3(e15.8),/,6(1pE20.13))
!
 6350 FORMAT(':::  ')
!
 6400 FORMAT(' &Data_For_Continuation_Run',/, &
     &       '       timesteps_to_this_point     = ',i10,',',/, &
     &       '       NR_iterations_to_this_point = ',i10,',',/, &
     &       '       number_of_detected_media    = ',i10,',',/, &
     &       '       origin_of_time              = ',1pe21.14,', ! [sec]',/, &
     &       '       time_to_this_point          = ',1pe21.14,', ! [sec]',/, &
     &       '       accumulated_quantities      = ',1pe21.14,', ! ',a50,/,  &
     &     5('                                     ',1pe21.14,', ! ',a50,/), &
     &       '                                   /' )
!
 6450 FORMAT('<<<  ')
!
 6500 FORMAT(//,22('ERROR-'),//,T33,  &
     &             '       S I M U L A T I O N   A B O R T E D',/,                              &
     &         T23,'There is a problem writing the namelist <Data_For_Continuation_Run> in the <SAVE> data file', &
     &       /,T32,'               CORRECT AND TRY AGAIN',                                      &
     &       //,22('ERROR-'))
!
 6505 FORMAT(///,'Updated_Source_Sink_Rates - The rate factors at the end of the simulation period are (for Master  &
     &      processor only):')
 6510 FORMAT(T5,5('RF(',i4,') = ',1pe13.6,',',1x))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Write_File_SAVE
!
!
      RETURN
!
      END SUBROUTINE Write_File_SAVE
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Write_Difusive_Flows
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units

         USE General_Control_Parameters
         USE Diffusion_Parameters
! 
         USE Grid_Geometry, ONLY: conx
         USE MPICOM
         USE MADIM
         USE UPDATEINFO
         USE tagN
         USE temp_arrays

!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE GENERATING A PRINTOUT OF DIFFUSIVE FLOW RATES         *
!*                                                                     *
!*                    Version 1.0 - June 22, 2003                      *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
      include 'mpif.h'
!
! -------
! ... Double precision automatic local arrays
! -------
!
      REAL(KIND = 8), DIMENSION(:,:,:),ALLOCATABLE:: TEMX
      CHARACTER*8, DIMENSION(:,:), ALLOCATABLE:: TEME0
!kz      INTEGER, DIMENSION(:), ALLOCATABLE:: up_T
      INTEGER, DIMENSION(:,:),ALLOCATABLE::NEX_T
      INTEGER(KIND = 4) :: NCON,stat(MPI_STATUS_SIZE)
      INTEGER(KIND = 4) ::SENDOK
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: i,j,n,kk,np,iI
! 
! -------
! ... Character arrays
! -------
! 
      CHARACTER(LEN = 3), PARAMETER, DIMENSION(5) :: IJ = (/'-1-','-2-','-3-','-4-','-5-'/)
! 
! -------
! ... Character parameters
! -------
! 
      CHARACTER(LEN = 3), PARAMETER  :: ijnph = 'all'
! 
      CHARACTER(LEN = 12), PARAMETER :: PHAC  = '  PHASE COMP'
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of OUT_DifusFlux
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
!
      IF(myid==iMaster) THEN
        ALLOCATE(TEMX(LMNCON,NumMobPhases,NumCom),STAT=i)
        ALLOCATE(TEME0(2,LMNCON),STAT=i)
!        ALLOCATE(up_T(LMNCON),STAT=i)
        ALLOCATE(NEX_T(2,LMNCON),STAT=i)
      ELSE
        ALLOCATE(TEMX(NumConx,NumMobPhases,NumCom),STAT=i)
        ALLOCATE(TEME0(2,NumConx),STAT=i)
        ALLOCATE(NEX_T(2,NumConx),STAT=i)
      END IF
      call Alloc_Message(i)
!
      DO n=1,NumConx
       NEX_T(1,n)=conx(n)%n1
       NEX_T(2,n)=conx(n)%n2
       TEME0(1,n)=conx(n)%name1
       TEME0(2,n)=conx(n)%name2
       TEMX(n,1:NumMobPhases,1:NumCom)=DiffusiveFlow(n,1:NumMobPhases,1:NumCom)
      END DO
!
       IF (myid/=iMaster) THEN
         call MPI_RECV(SENDOK, 1, MPI_INTEGER, iMaster, tag(1)+myid, MPI_COMM_WORLD, stat, ierr)
         call MPI_SEND(NumConx,1, MPI_INTEGER, iMaster, tag(2)+myid, MPI_COMM_WORLD, ierr)
!kz         call MPI_SEND(NEXIND,NumConx , MPI_INTEGER, iMaster, tag(3)+myid, MPI_COMM_WORLD, ierr)
         call MPI_SEND(TEMX, NumConx*NumMobPhases*NumCom, MPI_DOUBLE_PRECISION, iMaster, tag(4)+myid, MPI_COMM_WORLD, ierr)
         call MPI_SEND(TEME0,NumConx*8*2, MPI_CHARACTER, iMaster, tag(5)+myid, MPI_COMM_WORLD, ierr)
         call MPI_SEND(NEX_T,NumConx*2 , MPI_INTEGER, iMaster, tag(6)+myid, MPI_COMM_WORLD, ierr)
       ELSE
         do i = 0, nprocs-1
            IF (i /= iMaster) then
               call MPI_SEND(SENDOK, 1, MPI_INTEGER, i, tag(1)+i, MPI_COMM_WORLD, ierr)
               call MPI_RECV(NCON,1, MPI_INTEGER, i,tag(2)+i, MPI_COMM_WORLD, stat,ierr)
!kz               call MPI_RECV(up_T(1:NCON), NCON, MPI_INTEGER, i,tag(3)+i, MPI_COMM_WORLD, stat,ierr)
               call MPI_RECV(TEMX(1:NCON,1:NumMobPhases,1:NumCom), NCON*NumMobPhases*NumCom, MPI_DOUBLE_PRECISION,    &
     &          i,tag(4)+i,MPI_COMM_WORLD, stat, ierr)
               call MPI_RECV(TEME0(:,1:NCON),NCON*8*2, MPI_CHARACTER, i,tag(5)+i,MPI_COMM_WORLD, stat, ierr)
               call MPI_RECV(NEX_T(:,1:NCON), NCON*2, MPI_INTEGER, i,tag(6)+i, MPI_COMM_WORLD, stat,ierr)
            ELSE
              NCON=NumConx
!kz              up_T(1:NumConx)=NEXIND(1:NumConx)
            END IF
! 
! -------
! ... Print a short header
! -------
! 
      IF(i==0) WRITE(37,6002) ' ', title, Tot_NumTimeSteps, Tot_NumNRIterations, time
! 
! 
!***********************************************************************
!*                                                                     *
!*          For Option_InterfaceDiffusiveFluxes = 'DECOupled',         *
!*            print diffusive fluxes (Phases and components)           *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_DifPrint: IF(Option_InterfaceDiffusiveFluxes == 'DECO') THEN
! 
! ----------
! ...... Print table caption
! ----------
! 
         IF(i==0) THEN
          WRITE(37,6004) ' ',((PHAC,iI=1,NumCom),j=1,NumPhases)
          WRITE(37,6005) ((IJ(j),IJ(iI),iI=1,NumCom),j=1,NumPhases)
         END IF
! 
! ----------
! ...... Print diffusive fluxes (components and phases)
! ----------
! 
         DO_NumCon1: DO n=1,NCON
            IF(NEX_T(1,n) == 0 .OR. NEX_T(2,n) == 0) CYCLE DO_NumCon1

            WRITE(37,6006)  ADJUSTR(TEME0(1,n)), ADJUSTR(TEME0(2,n)),((TEMX(n,np,kk),kk=1,NumCom),np=1,NumMobPhases)                 
!   
         END DO DO_NumCon1
!
         WRITE(37,6010)
! 
! 
!***********************************************************************
!*                                                                     *
!*           For Option_InterfaceDiffusiveFluxes = 'COUPled',          *
!*               print diffusive fluxes (components only)              *
!*                                                                     *
!***********************************************************************
! 
! 
      ELSE IF(Option_InterfaceDiffusiveFluxes == 'COUP') THEN
! 
! ----------
! ...... Print table caption
! ----------
! 
         IF(i==0) THEN
         WRITE(37,6004) ' ',(PHAC,iI=1,NumCom)
         WRITE(37,6005) (IJnph,IJ(iI),iI=1,NumCom)
         END IF
! 
! ----------
! ...... Print diffusive fluxes (components only)
! ----------
! 
         DO_NumCon2: DO n=1,NCON
            IF(NEX_T(1,n) == 0 .OR. NEX_T(2,n) == 0) CYCLE DO_NumCon2
!
               WRITE(37,6006) ADJUSTR(TEME0(1,n)), ADJUSTR(TEME0(2,n)), (TEMX(n,NumMobPhases,kk), kk=1,NumCom)
!   
         END DO DO_NumCon2
!
         WRITE(37,6010)
!
!
      END IF IF_DifPrint
      END DO                  ! i=0, nprocs-1
      END IF                  ! myid/=iMaster
!      
      DEALLOCATE(NEX_T,TEME0,TEMX)
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Write_Difusive_Flows',T50,'[v1.0,  22 June      2006]',/,  &
     &         ':::::   Print out interblock diffusive flow rates (for Master Processor Only)')
!
 6002 FORMAT(A1,/,10X,A120,//,74X,'Timesteps = ',I6,' :|: NR iterations = ',I7,' :|: TIME = ',1pE13.6,/,   &
     &       ' MASS FLOW RATES (kg/s) FROM DIFFUSION'/)
!
 6004 FORMAT(A1,'ELEM1 ELEM2',10A12)
 6005 FORMAT(12X,10('   ',A3,2X,A3,' ')/)
!
 6006 FORMAT(1X,A5,1X,A5,(10(1X,1pE12.5)))
 6010 FORMAT(' ',131('@'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Write_Difusive_Flows
!
!
      RETURN
! 
! 
! 
      END SUBROUTINE Write_Difusive_Flows
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Analytical_Heat_Exchange
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
         USE General_Control_Parameters
!
         USE Grid_Geometry
         USE Element_Attributes
         USE Connection_Attributes
! 
         USE Solution_Matrix_Arrays,  ONLY : R
! 
         USE Geologic_Media_Properties
         USE VBR_Matrix 
         USE MPICOM
!
!*********************************************************************
!*********************************************************************
!*                                                                   *
!*    COMPUTES A SEMI-ANALYTICAL APPROXIMATION FOR HEAT EXCHANGE     *     
!*      WITH CONFINING LAYERS WITH UNIFORM INITIAL TEMPERATURE       *
!*                                                                   *
!*   Uses the method of Vinsome & Westerveld, J. Can. Petr. Tech.,   *     
!*                  July-September 1980, pp. 87-90                   *     
!*                                                                   *
!*                 Version 1.00, September 02, 2003                  *     
!*                                                                   *
!*********************************************************************
!*********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: HTL,HTLC,T00,DIF,D_i,DIFDT
      REAL(KIND = 8) :: TCUR,THETA,THETAK,PNUM,PDEN,PP,QNK1,QQ,FLOH
      REAL(KIND = 8) :: DPPDT,DQK1DT,cvalue, rvalue
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: N,n_m,NLOC,NLK1,N1KL
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call,n_m,T00,DIF
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Analytical_Heat_Exchange
!
!
!***********************************************************************
!*                                                                     *
!*           In the 1st call of <Analytical_Heat_Exchange>,            *
!*                  compute the thermal diffusivity                    *
!*                                                                     *
!***********************************************************************
!
!
      IF_1stCall: IF(First_call) THEN
!
         First_call = .FALSE.
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
!
         HTL  = 0.0d0
         HTLC = 0.0d0
! 
! ----------
! ...... Whole array operation: CAREFUL !
! 
! ...... Array 'AI' holds one parameter per element which characterizes the
! ......   temperature distribution in the confining layer. On initialization
! ......   all AI=0. At the end of a run the parameters 'AI' are written onto
! ......   the disk file 'TABLE' (needed for a restart)
! ----------
! 
         AI(1:NumElem) = 0.0d0
!
! ...... Read from files
!
         IF(myid==iMaster) THEN
         REWIND (UNIT = TABLE_Unit)
         READ   (UNIT = TABLE_Unit, FMT = 5001, END = 100 ) (AI(n),n=1,NumElem)
         END IF
!
! ----------
! ...... Assign initial temperature and diffusivity of confining layers
! ----------
!
  100    T00 = ElemState(NumElemTot)%temp(previous)
         n_m = elem(NumElemTot)%MatNum
!
         DIF = media(n_m)%KThrW/( media(n_m)%DensG*media(n_m)%SpcHt )
!
               IF(myid==iMaster) WRITE(36, FMT = 6002) T00,media(n_m)%KThrW, media(n_m)%DensG, media(n_m)%SpcHt
!
      END IF IF_1stCall
!
! -------
! ... Assign conduction distance
! -------
!
      HTL   = 0.0d0
      D_i   = SQRT(DIF*(time+TimeStep))/2.0d0
      DIFDT = DIF*TimeStep
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                           ELEMENT LOOP                              >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumEle: DO n=1,NumElem
!
!
!
         IF_AHTPos: IF(AHT(n) /= 0.0d0) THEN
!
! ......... Determine locations in the X array
!
            NLOC = Locp(n)             ! = (n-1)*NumEqu
            NLK1 = NLOC + NumComPlus1
!
! ...... Compute intermediate variables
!
            TCUR   = ElemState(n)%temp(current) 
!      
            THETA  = TCUR - T00
            THETAK = ElemState(n)%temp(previous) - T00
            PNUM   = DIFDT*THETA/D_i-(THETA-THETAK)*D_i*D_i*D_i/DIFDT
!
            IF(Tot_NumTimeSteps >= 2) PNUM = PNUM+AI(n)
!
            PDEN = 3.0d0*D_i*D_i+DIFDT
            PP   = PNUM/PDEN
!
            QNK1 = media(n_m)%KThrW*(THETA/D_i-PP)*TimeStep
!
            QQ   = ( (THETA-THETAK)/DIFDT - THETA/(D_i*D_i) + 2.0d0*PP/D_i )/2.0d0
!
            IF_Converge: IF(convergence .EQV. .TRUE.) THEN 
               AI(N) = THETA*D_i+PP*D_i*D_i+2.0d0*QQ*D_i*D_i*D_i
               FLOH  = -QNK1*AHT(n)/TimeStep
            ELSE
!
               FLOH    = -QNK1*AHT(n)/TimeStep
!kz             R(NLK1) = R(NLK1) + QNK1*AHT(n)/elem(n)%vol

              rvalue= QNK1*AHT(n)/elem(n)%vol
              R(NLK1)=R(NLK1)+rvalue
              call add2b(b, nbmxc, cpntr, NumElemTot, n, NumComPlus1, rvalue)
!
! ............ Determine locations of matrix elements corresponding
! ............ to the energy equation in the Jacobian 
!
               N1KL = (n-1)*NumEquSquare + NumEqu*NumCom
!
! ............ Compute derivative of lateral heat flux 
! ............ with respect to temperature 
!
               DPPDT  = ( DIFDT/D_i - D_i*D_i*D_i/DIFDT )/( 3.0d0*D_i*D_i + DIFDT )
!
               DQK1DT = media(n_m)%KThrW*TimeStep*( 1.0d0/D_i - DPPDT )
!
! ............ Adjust the Jacobian matrix element CO  
!
!kz             CO( N1KL + NumComPlus1 ) = CO( N1KL + NumComPlus1 ) - DQK1DT*AHT(n)/elem(n)%vol
               cvalue=-DQK1DT*AHT(n)/elem(n)%vol
               call add2a(n, n, NumEqu, NumComPlus1, cvalue,rpntr, cpntr, bpntr, bindx, indx, aval)
!
            END IF IF_Converge
!
            HTL = HTL-FLOH
!
         END IF IF_AHTPos
!
! <<<                      
! <<< End of the ELEMENT LOOP         
! <<<
!
      END DO DO_NumEle
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(4E20.13)
!
 6000 FORMAT(/,'Analytical_Heat_Exchange',T50,'[v1.0,   2 September 2003]',/,   &
     &         ':::::   Perform semi-analytical calculation for heat exchange with confining beds')
!
 6002 FORMAT(//,' ',131('='),/,   &
     &       ' PERFORM SEMI-ANALYTICAL HEAT EXCHANGECALCULATION',/,   &
     &       '      THERMAL PARAMETERS ARE:',/,   &
     &       '      TEMPERATURE = ',1pE12.5,'   HEAT CONDUCTIVITY = ',1pE12.5,  &
     &       '   DENSITY = ',1pE12.5,'   SPECIFIC HEAT = ',1pE12.5,/, &
     &       '      DIFFUSIVITY = ',1pE12.5/' ',131('='),//)
!
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Analytical_Heat_Exchange
!
!
      RETURN
!
      END SUBROUTINE Analytical_Heat_Exchange
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE SourceSink_Equation_Terms
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units

         USE General_Control_Parameters
         USE Solver_Parameters
! 
         USE Grid_Geometry, ONLY: elem
         USE Sources_and_Sinks
! 
         USE Solution_Matrix_Arrays
         USE MPICOM
         USE VBR_Matrix
! 
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE COMPUTING ALL TERMS ARISING FROM SINKS AND SOURCES      * 
!*                                                                     *
!*                  Version 1.00 - November 18, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision array
! -------
! 
      REAL(KIND = 8), DIMENSION(NumComPlus1 , NumEquPlus1) :: Q_contr
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: FAC,Gn,EGn
      REAL(KIND = 8) :: rvalue, cvalue
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: n,j,k,m,lnk,jkm,np
      INTEGER :: JLOC,JLOCP,MN,JMN,JNK1,TableLength
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 1) :: ITABA
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of SourceSink_Equation_Terms
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF (myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
!
! ... Print additional info
!
      IF(myid==iMaster) THEN 
      IF(Option_Print_SourceSinkInfo >= 1) WRITE(*, FMT = 6002) Tot_NumTimeSteps, Tot_NumNRIterations
      END IF   
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>                         SOURCE/SINK LOOP                            >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_NumSS: DO n=1,NumSS
!
         j = SS(n)%ElemNum
         IF(j == 0 .OR. j > NumElem) GO TO 1100
         FAC = TimeIncrement/elem(j)%vol
!
         JLOC  = (j-1)*NumEqu
         JLOCP = (j-1)*NumComPlus1
!
         mn   = SS(n)%TypeIndx
         JMN  = JLOC+MN
         JNK1 = JLOC+NumComPlus1
!
         TableLength = ABS(SS(n)%Table_Length)
         ITABA       = SS(n)%type(5:5)
! 
! 
!***********************************************************************
!*                                                                     *
!*             PRODUCTION OR INJECTION OF A MASS COMPONENT             *
!*  THE EFFECTS OF HEAT OF INJECTED/PRODUCED COMPONENT ARE CONSIDERED  *
!*                                                                     *
!***********************************************************************
! 
! 
         IF_MassC: IF(mn <= NumCom) THEN     ! Mass components only
! 
! 
! >>>>>>>>>>>>>>>>>>
! .........
! ......... Injection/production at a prescribed fixed rate (TableLength <= 1) 
! ......... 
! >>>>>>>>>>>>>>>>>>
! 
            IF_QFixed: IF(TableLength <= 1) THEN
!
               Gn = SS(n)%rate * SS(n)%RateFactor
! 
! -------------------
! ............ Mass injection (GN > 0), or 
! ............    production (GN < 0) with prescribed enthalpy
! -------------------
! 
               IF_Enth1: IF( Gn >= 0.0d0 .OR. (GN <  0.0d0 .AND. itaba /= ' ') ) THEN
!
                  rvalue=-fac*Gn
                  R(JMN)=R(JMN)+rvalue
                  call add2b(b, nbmxc, cpntr, NumElem, j, MN, rvalue)
! 
                  IF(NumEqu == NumComPlus1) THEN 
                   rvalue=-FAC*SS(n)%Rate*SS(n)%enth
                   R(JNK1)=R(JNK1)+rvalue
                   call add2b(b, nbmxc, cpntr, NumElem, j, NumComPlus1, rvalue)
                  END IF
! 
                  GO TO 1100
! 
! -------------------
! ............ Mass production (GN < 0), with enthalpy to be determined 
! ............    from conditions in producing block
! -------------------
! 
               ELSE
!
                  CALL Source_Phase_EnthComp(n,j,gn,fac,Q_contr) ! Calculate phase composition and flowing enthalpy of multiphase source fluid
! 
                  GO TO 1000                                     ! Go to modify the Jacobian
!
               END IF IF_Enth1
! 
! 
! >>>>>>>>>>>>>>>>>>
! .........
! ......... Time-dependent injection/production at a rate determined from  
! ......... tabular data of time, and corresponding rate and enthalpy
! ......... 
! >>>>>>>>>>>>>>>>>>
! 
! 
            ELSE
!
               CALL SS_Rate_Interpolation(n,TableLength,itaba,Gn,EGn)  ! Determine Gn,Egn from table
               IF(igood /= 0) RETURN                                 ! Exit the routine if a problem is encountered
!
               Gn         = Gn * SS(n)%RateFactor
               SS(n)%rate = Gn
! 
! -------------------
! ............ Mass injection (GN > 0), or 
! ............    production (GN < 0) with prescribed enthalpy
! ----------------
! 
               IF_Enth2: IF(itaba /= ' ') THEN
! 
                  SS(n)%enth = egn

                  rvalue=-fac*Gn
                  R(JMN)=R(JMN)+rvalue
                  call add2b(b, nbmxc, cpntr, NumElem, j, MN, rvalue)
! 
                  IF(NumEqu == NumComPlus1) THEN 
                   rvalue=-FAC*Gn * EGn
                   R(JNK1)=R(JNK1)+rvalue
                   call add2b(b, nbmxc, cpntr, NumElem, j, NumComPlus1, rvalue)
                  END IF
!kz                  r(jmn)     = r(jmn) - fac*gn
! 
!kz                  IF(NumEqu == NumComPlus1) R(JNK1) = R(JNK1) - FAC*SS(n)%MassRate*SS(n)%enth
! 
                  GO TO 1100
! 
               ELSE
! 
                  CALL Source_Phase_EnthComp(n,j,gn,fac,Q_contr)  ! Calculate phase composition and flowing enthalpy of multiphase source fluid
! 
                  GO TO 1000                                      ! Go to modify the Jacobian
!
               END IF IF_Enth2
!                          
! <<<<<<<<<                          
! <<<<<<<<< End of the Fixed Rate IF         
! <<<<<<<<<   
!                          
            END IF IF_QFixed
!
! <<<<<<                          
! <<<<<< End of the Mass Component IF        
! <<<<<<    
!
         END IF IF_MassC
! 
! 
!***********************************************************************
!*                                                                     *
!*                 DIRECT HEAT INJECTION/PRODUCTION                    *
!*                                                                     *
!***********************************************************************
! 
! 
         IF_Heat: IF(mn == NumComPlus1) THEN
! 
! 
! >>>>>>>>>>>>>>>>>>
! .........
! ......... Heat injection/production at a prescribed fixed rate 
! ......... 
! >>>>>>>>>>>>>>>>>>
! 
! 
            IF(NumEqu == NumComPlus1) THEN            ! Ignore heat sinks/sources when not solving the energy equation
! 
                   rvalue= - fac*SS(n)%rate
                  R(JMN)=R(JMN)+rvalue
                  call add2b(b, nbmxc, cpntr, NumElem, j, MN, rvalue)
!                                                                    
!kz               r(jmn) = r(jmn) - fac*SS(n)%MassRate   ! Adjusting the RHS
! 
            END IF
!
            GO TO 1100
!
! <<<<<<                          
! <<<<<< End of the Mass Component IF        
! <<<<<<    
!
         END IF IF_Heat
! 
! 
!***********************************************************************
!*                                                                     *
!*                           OTHER SCENARIOS                           *
!*                                                                     *
!***********************************************************************
! 
! 
         lnk = mn - NumComPlus1
!
!
!
         CASE_SSOption: SELECT CASE(lnk)
! 
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! ......
! ...... Deliverability option: Production well with specified downhole pressure      
! ......
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! 
         CASE (1)
!
! ......... Optionally perform simple gravity correction for flowing
! ......... downhole pressure of multi-feed-zone wells 
!
!kzhang, the following subroutine will work ok only when partitions all gridblocks in the well to the same subdomain 
            IF(NumNRIterations == 1 .AND. TableLength > 1) CALL Gravity_Correction(n,TableLength)
!
! ......... Obtain source rate, phase composition, flowing enthalpy
!
            CALL Phase_EnthComp_Deliverability(n,jlocp,j,fac,Q_contr)
!
!            Gpo(n) = G(n)
!
            GO TO 1000
! 
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! ......
! ...... Production well with specified wellhead pressure, and      
! ...... flowing wellbore pressure correction       
! ......
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! 
         CASE (2)
!
! ......... On 1st call, initialize starting guess for well rate
!
            IF(First_call) SS(n)%TempRate = 10.0d0
!
! ......... Obtain source rate, phase composition, flowing enthalpy
!
            CALL BottomHole_PXH_for_WellheadP(n,jlocp,j,fac,Q_contr)
!
!            Gpo(n) = G(n)
!
            GO TO 1000
! 
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! ......
! ...... DEFAULT
! ......
! >>>>>>>>>>>>>
! >>>>>>>>>>>>>
! 
         CASE DEFAULT 
!
            CONTINUE
!
         END SELECT CASE_SSOption
! 
! 
!***********************************************************************
!*                                                                     *
!*                MODIFY THE JACOBIAN MATRIX EQUATION                  *
!*                                                                     *
!***********************************************************************
! 
!
 
 1000    DO_NeqOuter: DO k=1,NumEqu
!
            R(JLOC+k) = R(JLOC+k) + Q_contr(k,1)     ! Adjusting the RHS
            call add2b(b, nbmxc, cpntr, NumElem, j, k, Q_contr(k,1))
!
            DO_NeqInner: DO m=1,NumEqu
             jkm     = (j-1)*NumEquSquare + (k-1)*NumEqu + m                           ! Locating the CO index
             cvalue=- ( Q_contr(k,m+1) - Q_contr(k,1) )/DELX(JLOCP+m)       
             call add2a(j, j, k, m, cvalue,rpntr, cpntr, bpntr, bindx, indx, aval)
!kz          CO(jkm) = CO(jkm) - ( Q_contr(k,m+1) - Q_contr(k,1) )/DELX(JLOCP+m)       ! Adjusting the CO coefficient
            END DO DO_NeqInner
!
         END DO DO_NeqOuter
! 
! -------
! ... Print supporting information (when Option_Print_SourceSinkInfo >=2 only)
! -------
! 
 1100    IF(Option_Print_SourceSinkInfo >= 2) WRITE(36, FMT = 6004) SS(n)%ElemName, SS(n)%name, SS(n)%rate*SS(n)%RateFactor, &
     &                                                             SS(n)%enth, (SS(n)%PhaseFracFlow(np), np=1,NumMobPhases)
!
! <<<                      
! <<<                      
! <<< End of the SOURCE/SINK LOOP         
! <<<
! <<<                      
!
      END DO DO_NumSS
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'SourceSink_Equation_Terms',T50,'[v1.0,  18 November  2003]',/,   &
     &         ':::::   Assembling all source and sink terms in the equations (generic version)',/,   &
     &         '        Rigorous  step rate capability for MOP(12) = 2, and capability for flowing wellbore pressure corrections')
!
!
 6002 FORMAT(/,' ==============> SUBROUTINE QU <==============   [Timestep, NR-iterations] = [',I6,',',I7,']')
!
 6004 FORMAT(' ELEMENT ',A8,'   SOURCE ',A5,'   :::   FLOW RATE = ',1pE13.6,'   SPECIFIC ENTHALPY = ',1pE13.6,/,   &
     &       ' FNP1 = ',1pE13.6,'   FNP2 = ',1pE13.6,'   FNP3 = ',1pE13.6,'   FNP4 = ',1pE13.6)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of SourceSink_Equation_Terms
!
!
      RETURN
! 
!
      END SUBROUTINE SourceSink_Equation_Terms
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE SS_Rate_Interpolation(n,TableLength,itaba,Gx,EGx)
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
         USE General_Control_Parameters
! 
         USE Sources_and_Sinks
         USE Utility_Functions
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE INTERPOLATING SINK/SOURCE RATES AND ENTHALPIES        *
!*                        FROM TABULAR DATA                            *
!*                                                                     *
!*                   Version 1.0 - January 4, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(OUT) :: Gx,EGx
! 
      REAL(KIND = 8) :: t_i,t_f,V_dn,V_up,Q1,Q2,qdt,hqdt
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,TableLength
! 
      INTEGER :: N_up,Lo,Hi,m
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 1), INTENT(IN) :: itaba
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call, Lo, Hi
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Table_Interpolation
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF (myid==iMaster) WRITE(VERS_Unit,6000)
         Lo = 1
         Hi = 1
      END IF
! 
! 
!***********************************************************************
!*                                                                     *
!*                           INITIALIZATIONS                           *
!*                                                                     *
!***********************************************************************
! 
! 
      t_i  = time                         ! Time at beginning of time step
      t_f  = time + TimeStep              ! Time at end of time step
!
      N_up = SS(n)%Table_Length       ! # of points in the table
      V_dn = SS(n)%TimeColumn(1)      ! First time value in table
      V_up = SS(n)%TimeColumn(N_up)   ! Last time value in table
! 
! 
!***********************************************************************
!*                                                                     *
!*      Determine the time interval in the generation time table:      *
!*          First the time at the beginning of the time step           *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_RangeD: IF(t_i < V_dn .OR. t_i > V_up) THEN                            ! When outside the table range, ...
                    WRITE(36,6001) t_i,V_dn,V_up                                 ! ... print warning, ... 
                    WRITE(36,6002) Tot_NumTimeSteps,SS(n)%ElemName,SS(n)%name    ! ... print additional info, ... 
                    IGOOD = 3                                                   ! ... set up for Dt cutback
                    RETURN                                                      ! Done ! Exit routine
                 ELSE
                    Lo = Table_Interval(t_i, SS(n)%TimeColumn, Lo)              ! Determine the interval #
                 END IF IF_RangeD
! 
! 
!***********************************************************************
!*                                                                     *
!*      Determine the time interval in the generation time table:      *
!*                The time at the end of the time step                 *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_RangeU: IF(t_f < V_dn .OR. t_f > V_up) THEN
                    WRITE(36,6001) t_f,V_dn,V_up
                    WRITE(36,6002) Tot_NumTimeSteps,SS(n)%ElemName,SS(n)%name
                    IGOOD = 3
                    RETURN
                 ELSE
                    Hi = Table_Interval(t_f, SS(n)%TimeColumn, Hi)         ! Determine the interval #
                 END IF IF_RangeU
! 
! 
!***********************************************************************
!*                                                                     *
!*              DETERMINATION OF WELL RATE AND ENTHALPY                *
!*                                                                     *
!***********************************************************************
! 
! 
      SELECT_Int: SELECT CASE(Option_SSRateInterpolation)
! 
! ----------
! ... 1st Option: Q is computed through linear interpolation           
! ----------
! 
      CASE(0)
!      
         Q1 =     SS(n)%RateColumn(Lo)           &                           ! Q at the begining of the current time-step ...
     &        + ( t_i - SS(n)%TimeColumn(Lo) )   &                           ! ... is obtained from table interpolation
     &         *( SS(n)%RateColumn(Lo+1) - SS(n)%RateColumn(Lo) )   &
     &         /( SS(n)%TimeColumn(Lo+1) - SS(n)%TimeColumn(Lo) )
!      
         Q2 =     SS(n)%RateColumn(Hi)           &                           ! Q at the end of the current time-step ...
     &        + ( t_f - SS(n)%TimeColumn(Hi) )   &                           ! ... is obtained from table interpolation
     &         *( SS(n)%RateColumn(Hi+1) - SS(n)%RateColumn(Hi) )   &
     &         /( SS(n)%TimeColumn(Hi+1) - SS(n)%TimeColumn(Hi) )
!      
         Gx = 5.0d-1*(Q1+Q2)                                                 ! Average Q
! 
! ...... 1st Option: H is computed through linear interpolation           
! 
         IF(ITABA /= ' ') THEN
!      
            Q1 =     SS(n)%EnthColumn(Lo)           &                        ! H at the begining of the current time-step ...
     &           + ( t_i - SS(n)%TimeColumn(Lo) )   &                        ! ... is obtained from table interpolation 
     &            *( SS(n)%EnthColumn(Lo+1) - SS(n)%EnthColumn(Lo) )   &
     &            /( SS(n)%TimeColumn(Lo+1) - SS(n)%TimeColumn(Lo) )
!      
            Q2 =     SS(n)%EnthColumn(Hi)           &                        ! H at the end of the current time-step ...
     &           + ( t_f - SS(n)%TimeColumn(Hi) )   &                        ! ... is obtained from table interpolation
     &            *( SS(n)%EnthColumn(Hi+1) - SS(n)%EnthColumn(Hi) )   &
     &            /( SS(n)%TimeColumn(Hi+1) - SS(n)%TimeColumn(Hi) )
!      
            EGx = 5.0d-1*(Q1+Q2)                                             ! Average H
         ELSE
            EGx = 0.0d0
         END IF
! 
! ----------
! ... 2nd Option: Q,H are computed assuming a step function distribution of tabular data       
! ----------
! 
      CASE(1)
!      
         Gx = 5.0d-1*(  SS(n)%RateColumn(Lo) + SS(n)%RateColumn(Hi) )        ! Q averaged 
!      
         IF(ITABA /= ' ') THEN
            EGx = 5.0d-1*(  SS(n)%EnthColumn(Lo) + SS(n)%EnthColumn(Hi) )    ! H is averaged similarly
         ELSE
            EGx = 0.0d0
         END IF
! 
! ----------
! ... Q,H are computed assuming a rigorous adherence to a step function distribution        
! ----------
! 
      CASE(2)
!
! ...... When the current time-step boundaries fall within a single table interval
!
         IF_Rig: IF(Lo == Hi) THEN
!
            Gx = SS(n)%RateColumn(Lo)
            IF(ITABA /= ' ') EGx = SS(n)%EnthColumn(Lo)
!
! ...... When the current time-step boundaries span across several table intervals
!
         ELSE
!
            qdt =   SS(n)%RateColumn(Lo)*( SS(n)%TimeColumn(Lo+1) - t_i )   &
     &            + SS(n)%RateColumn(Hi)*( t_f - SS(n)%TimeColumn(Hi) )
!      
            IF(ITABA /= ' ') THEN
               hqdt =  SS(n)%EnthColumn(Lo)*SS(n)%RateColumn(Lo)*( SS(n)%TimeColumn(Lo+1) - t_i ) &
     &               + SS(n)%EnthColumn(Hi)*SS(n)%RateColumn(Hi)*( t_f - SS(n)%TimeColumn(Hi) )
            END IF
!
! ......... Compute intermediate parameters
!
            IF_Rig2: IF(Hi > Lo + 1) THEN
!      
               DO_mLoop: DO m = Lo+1, Hi-1
!      
                  qdt = qdt + SS(n)%RateColumn(m)*( SS(n)%TimeColumn(m+1) - SS(n)%TimeColumn(m) )
                  IF(ITABA /= ' ') THEN
                     hqdt = hqdt + SS(n)%EnthColumn(m) * SS(n)%RateColumn(m)*( SS(n)%TimeColumn(m+1) - SS(n)%TimeColumn(m) )
                  END IF
!      
               END DO DO_mLoop
!      
            END IF IF_Rig2
!
! ......... Finally, compute Q,H
!
            Gx = qdt/TimeStep                           ! Determine Q
!
            IF(ITABA /= ' ' .AND. qdt /= 0.0d0) THEN    ! Determine H
               EGx = hqdt/qdt
            ELSE
               EGx = 0.0d0
            END IF
!      
         END IF IF_Rig
!      
      END SELECT SELECT_Int
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'SS_Rate_Interpolation',T50,'[v1.0,  15 January   2007]',/,   &
     &         ':::::   Interpolate sink/source rates and enthalpies from tabular data')
!
 6001 FORMAT(T2,'In <SS_Rate_Interpolation>, the time value ',1pE14.7,' is outside the range  (',1pE14.7,',',1pE14.7,')')
 6002 FORMAT(T2,'At <Tot_NumTimeSteps> = ',I4,' the time exceeds the maximum value in the generation time table at element',A5,  &
     &          ' (source ',A5,') ==> will reduce timestep')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of SS_Rate_Interpolation
!
!
      RETURN
!
      END SUBROUTINE SS_Rate_Interpolation
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Well_Deliverability(i_n,kf)
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
!
         USE General_Control_Parameters
! 
         USE Sources_and_Sinks
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*    ROUTINE FOR READING FROM F-FILES DATA ON WELL DELIVERABILITY     *
!*                                                                     *
!*                  Version 1.00, January 19, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN)    :: i_n
      INTEGER, INTENT(INOUT) :: kf
! 
      INTEGER :: i,j,k,nG,nH,ier
! 
! -------
! ... Character variables
! -------
! 
      CHARACTER(LEN = 5)  :: DLV_File
      CHARACTER(LEN = 10) :: Header
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: EX
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Well_Deliverability
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         WRITE(VERS_Unit,6000)
      END IF
! 
! 
!***********************************************************************
!*                                                                     *
!*        DETERMINE THE FILE NAMES WITH THE DELIVERABILITY DATA        *
!*                                                                     *
!***********************************************************************
! 
! 
      DO_NumCh: DO i = 1,5
! 
         IF(SS(i_n)%type(i:i) == ' ') THEN
            DLV_File = SS(i_n)%type(1:i-1)     ! The file where the data are stored
            EXIT
         ELSE
            IF(i == 5) DLV_File = SS(i_n)%type ! The file where the data are stored
         END IF
! 
      END DO DO_NumCh
! 
! ----------
! ... Determine if such a file is available      
! ----------
! 
      INQUIRE(FILE=DLV_File,EXIST=EX)
! 
      IF(EX) THEN                                          ! If the file exists ...
         WRITE(36,6001) DLV_File                            !    print info 
         OPEN(DELV_Unit, FILE=DLV_File, STATUS='OLD')      !    open as an old file
      ELSE                                                 ! If the file does not exist ...
         WRITE(36,6002) DLV_File                           !    print warning and ignore the item
         SS(i_n)%TypeIndx  = 0                             ! Reset the index describing well type
         RETURN                                            ! Done ! Exit the routine
      END IF    
! 
! 
!***********************************************************************
!*                                                                     *
!*        For a valid F-type file with deliverability data, ...        *
!*                                                                     *
!***********************************************************************
! 
! 
      SS(i_n)%TypeIndx = NumComPlus1+2       ! Set index describing well type
! 
      SS(i_n)%ProdIndx = SS(i_n)%rate        ! Set the well productivity index
! 
! ----------
! ... If the current wellbore pressure file has already been read      
! ----------
! 
      IF_ExPF: IF(kf >= 1) THEN
! 
         DO k=1,kf
! 
            IF(DLV_File == SS(k)%DelivFileName) THEN
! 
               WDel(i_n)%idTab = k                   ! Assign index of previously read wellbore 
                                                     !    pressure file to current GENER item
               RETURN                                ! Done ! Exit the routine
! 
            END IF       
! 
         END DO       
! 
      END IF IF_ExPF 
! 
! ----------
! ... Read new wellbore pressure file                 
! ----------
! 
      kf                   = kf+1          ! Set the new variables
      SS(kf)%DelivFileName = DLV_File
! 
      WDel(i_n)%idTab = kf
! 
      READ(DELV_Unit,5001) Header        ! Read the title
! 
      READ(DELV_Unit,5002) nG,nH         ! Read the number of data points
! 
      WDel(kf)%NumG = nG          ! Read the number of generation curves
      WDel(kf)%NumH = nH          ! Read the number of enthalpy curves
! 
! ----------
! ... Allocate memory to the array including the bottomhole pressure data                
! ----------
! 
!kzhang      ALLOCATE(WDel(i_n)%p_bhp(nG+nH+nG*nH), STAT = ier)
      ALLOCATE(WDel(kf)%p_bhp(nG+nH+nG*nH), STAT = ier) 
! 
! ... Print explanatory comments  
! 
      IF(ier == 0) THEN
         WRITE(MemoryAlloc_Unit,6101) 
      ELSE
         WRITE(36,6102)
         WRITE(MemoryAlloc_Unit,6102) 
         STOP
      END IF
! 
      READ(DELV_Unit,5003)  (WDel(kf)%p_bhp(i),i=1,nG)     ! Read the generation data (row headings)
! 
      READ(DELV_Unit,5003)  (WDel(kf)%p_bhp(nG+j),j=1,nH)  ! Read the enthalpy data (column headings)
! 
      READ(DELV_Unit,5003)  ((WDel(kf)%p_bhp(nG+nH+(i-1)*nH+j),j=1,nH),i=1,nG) ! Read the pressure data
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 5001 FORMAT(A10)
 5002 FORMAT(2I5)
 5003 FORMAT(8E10.4)
! 
 6000 FORMAT(/,'Well_Deliverability',T50,'[v1.0, 19 January   2006]',/,  &
     &         ':::::   Read from F-files data on well deliverability ') 
! 
 6001 FORMAT(' FILE "',a5,'" EXISTS: => OPEN AS AN OLD FILE')
 6002 FORMAT(T2,'Invalid F-type GENER item:  File "',A5,'" is not available')
! 
 6101 FORMAT(T2,'Memory allocation of "WDel(i_n)%p_bhp" in subroutine "Well_Deliverability" was successful')
 6102 FORMAT(//,20('ERROR-'),//,   &
     &       T2,'Memory allocation of "WDel(i_n)%p_bhpt" in subroutine "Well_Deliverability" was unsuccessful',//,  &
     &       T2,'!!!!!!!    THE EXECUTION WAS STOPPED    !!!!!!!',   &
     &       //,20('ERROR-'))
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Well_Deliverability
!
!
      RETURN
! 
      END SUBROUTINE Well_Deliverability
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Gravity_Correction(n,TableLength)
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
!
         USE General_Control_Parameters
! 
         USE Element_Attributes, ONLY: ElemProp
         USE Sources_and_Sinks
         USE Solution_Matrix_Arrays, ONLY: X
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*      ROUTINE PROVIDING A SIMPLE GRAVITY CORRECTION TO FLOWING       *
!*   BOTTOMHOLE PRESSURE OF MULTI-FEED ZONE WELLS ON DELIVERABILITY    *
!*                                                                     *
!*                   Version 1.00, February 4, 2004                    *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: pind
      REAL(KIND = 8) :: Mob_Gas,Mob_Aqu,QVG,QVW
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,TableLength
! 
      INTEGER :: NLTAB1,ND,JD,JDLOCP
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Gravity_Correction
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
!
! ... Determine index
!
      NLTAB1 = n + TableLength - 1                
!
!***********************************************************************
!*                                                                     *
!*                 LOOP OVER ALL LAYERS FOR THIS WELL                  *
!*                                                                     *
!***********************************************************************
!
      DO_NumLayrs: DO nd=n,NLTAB1      ! ND is the generation index
!
         jd     =  SS(nd)%ElemNum      ! Determine positions in the PAR array
         JDLOCP = (jd-1)*NumComPlus1
!
         pind   = SS(nd)%ProdIndx      ! The well productivity index
! 
! -------------
! ...... Compute gas phase mobilities
! -------------
! 
         IF(ElemProp(jd,0)%viscosity(1) /= 0.0d0) THEN
            Mob_Gas = ElemProp(jd,0)%RelPerm(1)/ElemProp(jd,0)%viscosity(1)
         ELSE
            Mob_Gas = 0.0d0
         END IF
! 
! -------------
! ...... Compute aqueous phase mobilities
! -------------
! 
         IF(NumPhases >= 2 .AND. ElemProp(jd,0)%viscosity(2) /= 0.0d0) THEN
            Mob_Aqu = ElemProp(jd,0)%RelPerm(2)/ElemProp(jd,0)%viscosity(2)
         ELSE
            Mob_Aqu = 0.0d0
         END IF
! 
! -------------
! ...... Compute volumetric production rates for individual phases
! -------------
! 
         QVG = pind*Mob_Gas*X(JDLOCP+1)    ! Gas
         QVW = pind*Mob_Aqu*X(JDLOCP+1)    ! Aqueous
! 
! -------------
! ...... Assign values to arrays for cumulative phase rates
! -------------
! 
         IF(nd == n) THEN
            SS(nd)%PhaseVolRate(1) = QVG
            SS(nd)%PhaseVolRate(2) = QVW
         ELSE
            SS(nd)%PhaseVolRate(1) = SS(nd-1)%PhaseVolRate(1) + QVG
            SS(nd)%PhaseVolRate(2) = SS(nd-1)%PhaseVolRate(2) + QVW
         END IF
! 
! -------------
! ...... Assign values to array of gradients
! -------------
! 
         SS(nd)%grad = (  SS(nd)%PhaseVolRate(1)*ElemProp(jd,0)%density(1)   &
     &                  + SS(nd)%PhaseVolRate(2)*ElemProp(jd,0)%density(2)   &
     &                 )*9.80665d0                                           &
     &                /( SS(nd)%PhaseVolRate(1) + SS(nd)%PhaseVolRate(2) )
! 
! 
! 
      END DO DO_NumLayrs
! 
! 
!***********************************************************************
!*                                                                     *
!*   Perform loop going from top layer down: starting from the layer   *
!*            just below the top (index N+TableLength-2)               *
!*                   to the bottom layer (index N)                     *
!*                                                                     *
!*                  CAREFUL! Whole array operations                    *
!*                                                                     *
!***********************************************************************
! 
! 
      SS(NLTAB1-1:N:-1)%BtmHoleP = SS(NLTAB1:N+1:-1)%BtmHoleP   &
     &                            +(  SS(NLTAB1:N+1:-1)%LayerZ  &
     &                               *SS(NLTAB1:N+1:-1)%grad    &
     &                              + SS(NLTAB1-1:N:-1)%LayerZ  & 
     &                               *SS(NLTAB1-1:N:-1)%grad    &
     &                             )*5.0d-1
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************/
!
!
 6000 FORMAT(/,'Gravity_Correction',T50,'[v1.0,   4 February  2006]',6X,   &
     &         ':::::   Perform simple gravity correction for flowing bottomhole pressure')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Gravity_Correction
!
!
      RETURN
! 
      END SUBROUTINE Gravity_Correction
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Source_Phase_EnthComp(n,j,Gn,fac,Q_contr)
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
!
         USE General_Control_Parameters
! 
         USE Grid_Geometry
         USE Element_Attributes
! 
         USE Sources_and_Sinks
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*  ROUTINE DETERMINING PHASE COMPOSITION & ENTHALPY OF SOURCE FLUID   *
!*                                                                     *
!*                   Version 1.00 - April 27, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Quad precision array
! -------
! 
      REAL(KIND = 8), INTENT(OUT), DIMENSION(NumEqu, NumEquPlus1) :: Q_contr
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: Gn,fac
! 
      REAL(KIND = 8) :: FFS
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,j
! 
      INTEGER :: ic,m,np
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Source_Phase_EnthComp
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF (myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
! 
! -------
! ... Assignment of parameter values
! -------
! 
      ic = Option_ProducedFluidComposition + 1
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>              LOOP OVER NumEqu+1 SETS OF PARAMETERS                  >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_Neq1: DO m=1,NumEquPlus1
! 
! ----------
! ...... Initialize the block Q_contr(j,j)
! ----------
! 
         Q_contr(1:NumComPlus1,m) = 0.0d0  ! CAREFUL - Whole array operation
! 
! ----------
! ...... Initialization
! ----------
! 
         FFS        = 0.0d0   ! = SUM(mobility*density) in all phases
         SS(n)%enth = 0.0d0   ! Flow enthalpy  
! 
! 
!***********************************************************************
!*                                                                     *
!*           COMPUTE FRACTIONAL FLOWS IN THE VARIOUS PHASES            *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase1: DO np=1,NumMobPhases
!      
            SS(n)%PhaseFracFlow(np) = 0.0d0   ! Initialization of fractional flows
!
            IF(ic == 1 .AND. ElemProp(j,m-1)%viscosity(np) /= 0.0d0) THEN
!
               SS(n)%PhaseFracFlow(np) = ElemProp(j,m-1)%RelPerm(np) * ElemProp(j,m-1)%density(np)   &
     &                                  /ElemProp(j,m-1)%viscosity(np)
            END IF
!
!
            IF(ic == 2) THEN
               SS(n)%PhaseFracFlow(np) = ElemProp(j,m-1)%satur(np) * ElemProp(j,m-1)%density(np)
            END IF
! 
! -------------
! ......... Adjsust the sum in FFS
! -------------
! 
            FFS = FFS + SS(n)%PhaseFracFlow(np)
! 
! -------------
! ......... For Option_Print_SourceSinkInfo >= 5, print info
! -------------
!
            IF(Option_Print_SourceSinkInfo >= 5) WRITE(36, FMT = 6002) m, np, ElemProp(j,m-1)%RelPerm(np),     &
     &                                                                       ElemProp(j,m-1)%viscosity(np),   &
     &                                                                       ElemProp(j,m-1)%density(np),     &
     &                                                                       SS(n)%PhaseFracFlow(np), FFS
!
         END DO DO_NumPhase1

! 
! 
!***********************************************************************
!*                                                                     *
!*           RENORMALIZE FRACTIONAL FLOWS TO 1, AND COMPUTE            *
!*               CONTRIBUTIONS OF COMPONENTS IN PHASES                 *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase2: DO np=1,NumMobPhases
!      
! ......... Normalization of fractional flows      
!      
            IF(FFS /= 0.0d0) SS(n)%PhaseFracFlow(np) = SS(n)%PhaseFracFlow(np)/FFS
!      
! ......... Contribution of components to phase enthalpy     
!      
            SS(n)%enth =  SS(n)%enth + SS(n)%PhaseFracFlow(np) * ElemProp(j,m-1)%enthalpy(np)
!      
! ......... Adjustment of the Jacobian submatrices:  CAREFUL! Whole array operation     
!      
            Q_contr(1:NumCom , m) = Q_contr(1:NumCom , m) - FAC*GN*SS(n)%PhaseFracFlow(np) * ElemProp(j,m-1)%MassFrac(1:NumCom,np)
!
         END DO DO_NumPhase2
! 
! ----------
! ...... Adjust the heat-related Jacobian submatrices
! ----------
! 
         Q_contr(NumComPlus1,m) = Q_contr(NumComPlus1,m) - FAC*GN*SS(n)%enth
!
! <<<                      
! <<< End of the NumEquPlus1 LOOP         
! <<<
!
      END DO DO_Neq1
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Source_Phase_EnthComp',T50,'[v1.0,  27 April     2006]',/,   &
     &         ':::::   Determine phase composition and enthalpy of source fluid')
! 
 6002 FORMAT(' M=',I2,' NP=',I1,' K=',1pE13.6,' VIS=',1pE13.6,   &
     &       ' D=',1pE13.6,' FF=',1pE13.6,' FFS=',1pE13.6)
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Source_Phase_EnthComp
!
!
      RETURN
! 
!
      END SUBROUTINE Source_Phase_EnthComp
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Phase_EnthComp_Deliverability(n,jlocp,j,fac,Q_contr)
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
!
         USE General_Control_Parameters
! 
         USE Element_Attributes
! 
         USE Sources_and_Sinks
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*  ROUTINE DETERMINING PHASE COMPOSITION & ENTHALPY OF SOURCE FLUID   *
!*                    FOR WELLS ON DELIVERABILITY                      *
!*                                                                     *
!*                   Version 1.00 - April 27, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Quad precision array
! -------
! 
      REAL(KIND = 8), INTENT(OUT), DIMENSION(NumEqu, NumEquPlus1) :: Q_contr
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN) :: fac
! 
      REAL(KIND = 8) :: DELP,pin,Gn,FFS,P_cap
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,j,jlocp
! 
      INTEGER :: ic,m,np
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Phase_EnthComp_Deliverability
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
! 
! -------
! ... Assignment of parameter values
! -------
! 
      ic  = Option_ProducedFluidComposition + 1
      pin = SS(n)%ProdIndx
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>               LOOP OVER NumEqu+1 SETS OF PARAMETERS                 >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_Neq1: DO m=1,NumEquPlus1
! 
! ----------
! ...... Initialize the block Q_contr(j,j)
! ----------
! 
         Q_contr(1:NumComPlus1,M) = 0.0d0  ! CAREFUL - Whole array operation
! 
! ----------
! ...... Initializations
! ----------
! 
         FFS            = 0.0d0     ! = SUM(mobility*density) in all phases
         SS(n)%Rate = 0.0d0         ! Mass flow rate 
         SS(n)%enth     = 0.0d0     ! Enthalpy rate
! 
! 
!***********************************************************************
!*                                                                     *
!*           COMPUTE FRACTIONAL FLOWS IN THE VARIOUS PHASES            *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase1: DO np=1,NumMobPhases
!      
            SS(n)%PhaseFracFlow(np) = 0.0d0   ! Initialization of fractional flows
! 
! 
            IF(ic == 1 .AND. ElemProp(j,m-1)%viscosity(np) /= 0.0d0) THEN
! 
               SS(n)%PhaseFracFlow(np) = ElemProp(j,m-1)%RelPerm(np) * ElemProp(j,m-1)%density(np)   &
     &                                  /ElemProp(j,m-1)%viscosity(np)
            END IF
!
!
            IF(ic == 2) SS(n)%PhaseFracFlow(np) = ElemProp(j,m-1)%satur(np) * ElemProp(j,m-1)%density(np)
! 
! 
!***********************************************************************
!*                                                                     *
!*         COMPUTE FLOW RATES FOR EACH PHASE, AND TOTAL RATE           *
!*                    FOR WELLS ON DELIVERABILITY                      *
!*                                                                     *
!***********************************************************************
! 
! 
            IF(np == 1) THEN
               P_cap = 0.0d0
            ELSE 
               P_cap = ElemProp(j,m-1)%CapPres(np-1)
            END IF
!
            DELP = MAX( 0.0d0, ( ElemState(j)%pres(m-1) + P_cap - SS(n)%BtmHoleP ) )
! 
! -------------
! ......... Compute fractional flows (adjustment)
! -------------
! 
            SS(n)%PhaseFracFlow(np) = SS(n)%PhaseFracFlow(np)*pin*DELP
! 
! -------------
! ......... Compute mass flow rate
! -------------
! 
            SS(n)%rate = SS(n)%rate - SS(n)%PhaseFracFlow(np)
! 
! -------------
! ......... Augment the sum in FFS
! -------------
! 
            FFS = FFS+SS(n)%PhaseFracFlow(np)
! 
!
! -------------
! ......... For Option_Print_SourceSinkInfo >= 5, print info
! -------------
!
            IF(Option_Print_SourceSinkInfo >= 5) WRITE(36, FMT = 6002) m, np, ElemProp(j,m-1)%RelPerm(np),     &
     &                                                                       ElemProp(j,m-1)%viscosity(np),   &
     &                                                                       ElemProp(j,m-1)%density(np),     &
     &                                                                       SS(n)%PhaseFracFlow(np), FFS

         END DO DO_NumPhase1
! 
! 
!***********************************************************************
!*                                                                     *
!*           RENORMALIZE FRACTIONAL FLOWS TO 1, AND COMPUTE            *
!*               CONTRIBUTIONS OF COMPONENTS IN PHASES                 *
!*                                                                     *
!***********************************************************************
! 
! 
         Gn = SS(n)%rate    ! Initialization
!
!
!
         DO_NumPhase2: DO np=1,NumMobPhases
!      
! ......... Normalization of fractional flows        
!      
            IF(FFS /= 0.0d0) SS(n)%PhaseFracFlow(np) = SS(n)%PhaseFracFlow(np)/FFS
!      
! ......... Contribution of components to phase enthalpy     
!      
            SS(n)%enth =  SS(n)%enth + SS(n)%PhaseFracFlow(np) * ElemProp(j,m-1)%enthalpy(np)
!      
! ......... Adjustment of the Jacobian submatrices:  CAREFUL! Whole array operation     
!      
            Q_contr(1:NumCom , m) = Q_contr(1:NumCom , m) - FAC*GN * SS(n)%PhaseFracFlow(np) * ElemProp(j,m-1)%MassFrac(1:NumCom,np)
!
         END DO DO_NumPhase2
! 
! ----------
! ...... Adjust the heat-related Jacobian submatrices
! ----------
! 
         Q_contr(NumComPlus1,m) = Q_contr(NumComPlus1,m) - FAC*GN*SS(n)%enth
!
! <<<                      
! <<< End of the NEQ1 LOOP         
! <<<
!
      END DO DO_Neq1
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Phase_EnthComp_Deliverability',T50,'[v1.0,  27 April     2006]',/,   &
     &         ':::::   Determine phase composition and enthalpy of source fluid for wells on deliverability')
! 
 6002 FORMAT(' M=',I2,' NP=',I1,' K=',1pE13.6,' VIS=',1pE13.6,   &
     &       ' D=',1pE13.6,' FF=',1pE13.6,' FFS=',1pE13.6)
!
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Phase_EnthComp_Deliverability
!
!
      RETURN
! 
!
      END SUBROUTINE Phase_EnthComp_Deliverability
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE BottomHole_PXH_for_WellheadP(n,jlocp,j,fac,Q_contr)
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
!
         USE General_Control_Parameters
! 
         USE Element_Attributes
! 
         USE Sources_and_Sinks
         USE Solution_Matrix_Arrays, ONLY: X, DX, DELX
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*   ROUTINE DETERMINING BOTTOMHOLE PRESSURE, PHASE COMPOSITION AND    *
!*              ENTHALPY OF SOURCE FLUID FOR PRODUCTION                *
!*                 WITH SPECIFIED WELLHEAD PRESSURE                    *
!*                                                                     *
!*                   Version 1.00 - April 26, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Quad precision array
! -------
! 
      REAL(KIND = 8), INTENT(OUT), DIMENSION(NumEqu, NumEquPlus1) :: Q_contr
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: fac
! 
      REAL(KIND = 8) :: gw,gw0,pin,Gn,FFS,pwbn,gres,dpdg,rg,pj,drgdg
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n,j,jlocp
! 
      INTEGER :: ic,m,np,ig
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: NRI_Convergence
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of BottomHole_PXH_for_WellheadP
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
! 
! -------
! ... Assignment of parameter values
! -------
! 
      ic  = Option_ProducedFluidComposition + 1
      pin = SS(n)%ProdIndx
      gw0 = SS(n)%TempRate
!
!
!
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>                                                                     >
!>             LOOP OVER NumEqu + 1 SETS OF PARAMETERS                 >
!>                                                                     >
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
!
!
!
      DO_Neq1: DO m=1,NumEquPlus1
! 
! ----------
! ...... Initialize the block Q_contr(j,j)
! ----------
! 
         Q_contr(1:NumComPlus1,M) = 0.0d0  ! CAREFUL - Whole array operation
! 
! ----------
! ...... Initializations
! ----------
! 
         FFS        = 0.0d0   ! = SUM(mobility*density) in all phases
         SS(n)%enth = 0.0d0   ! Enthalpy rate
! 
! 
!***********************************************************************
!*                                                                     *
!*           COMPUTE FRACTIONAL FLOWS IN THE VARIOUS PHASES            *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase1: DO np=1,NumMobPhases
!      
            SS(n)%PhaseFracFlow(np) = 0.0d0   ! Initialization of fractional flows
! 
! 
            IF(ic == 1 .AND. ElemProp(j,m-1)%viscosity(np) /= 0.0d0) THEN
               SS(n)%PhaseFracFlow(np) = ElemProp(j,m-1)%RelPerm(np) * ElemProp(j,m-1)%density(np)   &
     &                                  /ElemProp(j,m-1)%viscosity(np)
            END IF
!
!
            IF(ic == 2) THEN
               SS(n)%PhaseFracFlow(np) = ElemProp(j,m-1)%satur(np) * ElemProp(j,m-1)%density(np)
            END IF
! 
! -------------
! ......... Augment the sum in FFS
! -------------
! 
            FFS = FFS + SS(n)%PhaseFracFlow(np)
! 
! -------------
! ......... Contribution of components to phase enthalpy     
! -------------
!      
            SS(n)%enth =  SS(n)%enth + SS(n)%PhaseFracFlow(np) * ElemProp(j,m-1)%enthalpy(np)
! 
! -------------
! ......... For Option_Print_SourceSinkInfo >= 5, print info
! -------------
!
            IF(Option_Print_SourceSinkInfo >= 5) WRITE(36, FMT = 6002) m, np, ElemProp(j,m-1)%RelPerm(np),   &
     &                                                                       ElemProp(j,m-1)%viscosity(np), &
     &                                                                       ElemProp(j,m-1)%density(np),   &
     &                                                                       SS(n)%PhaseFracFlow(np), FFS

         END DO DO_NumPhase1
! 
! ----------
! ...... Normalize EG(n)     
! ----------
!      
         IF(FFS /= 0.0d0) SS(n)%enth = SS(n)%enth/FFS
! 
! 
!***********************************************************************
!*                                                                     *
!*          ITERATE ON FLOW RATE TO FIND BOTTOMHOLE PRESSURE           *
!*                                                                     *
!***********************************************************************
! 
! 
         IF(m == 2) THEN
            pj = pj + delx(jlocp+1)
         ELSE
            pj = x(jlocp+1) + dx(jlocp+1)
         END IF
! 
! ...... Initializations 
! 
         gw              = gw0      ! Well flow rate (positive for production)
         NRI_Convergence = .FALSE.  ! Flag indicating convergence of the NR iteration 
! 
! >>>>>>>>>>
! >>>>>>>>>>
! ...... 
! ...... Newton/Raphson iteration
! ...... 
! >>>>>>>>>>
! >>>>>>>>>>
! 
         DO_NewtRaphLoop: DO ig=1,20
! 
            CALL Bottom_Hole_Pressure(n,gw,SS(n)%enth,pwbn,dpdg)  ! Find bottomhole pressure 
! 
            IF(igood /= 0) THEN                                           ! If unable to do so, ...
               WRITE(36,6004) Tot_NumTimeSteps,SS(n)%ElemName,SS(n)%name   ! ... print warning message, and ...
               RETURN                                                     ! ... exit the routine
            END IF
! 
! ......... Set reservoir rate (negative for production)
! 
            gres = -pin*FFS*(pj-pwbn)
            rg   =  gres+gw
! 
! ......... Print info for 'Option_Print_SourceSinkInfo' >= 5
!
            IF(Option_Print_SourceSinkInfo >= 5) WRITE(36, FMT = 6006) ig, pwbn, gw, gres, dpdg, rg
! 
! -------------
! ......... Upon convergence
! -------------
! 
            IF(abs(rg/gw) <= 1.0d-10) THEN
               NRI_Convergence = .TRUE.      ! Reset the convergence flag
               EXIT DO_NewtRaphLoop          ! Exit the NR loop
            END IF
! 
! -------------
! ......... Otherwise, update variables and continue the iteration
! -------------
! 
            drgdg = pin*ffs*dpdg+1.0d0
            gw    = gw-rg/drgdg
! 
         END DO DO_NewtRaphLoop
! 
! 
!***********************************************************************
!*                                                                     *
!*          UPON COMPLETION OF THE NEWTON-RAPHSON ITERATION            *
!*                                                                     *
!***********************************************************************
! 
! 
         IF(NRI_Convergence .EQV. .TRUE.) THEN  ! Upon convergence
! 
! -------------
! ......... Finalize parameter values
! -------------
! 
            Gn = gres
! 
            IF(m == 1) THEN
               SS(n)%BtmHoleP =  pwbn   ! Bottomhole pressure
               SS(n)%rate     =  gres   ! Flow rate
               SS(n)%TempRate = -gres   ! Store current well rate for next initialization
            END IF
! 
         ELSE                   
! 
! -------------
! ......... Upon unsucessful NR iteration
! -------------
! 
            igood = 3                                                                ! Set flag
            WRITE(36,6008) Tot_NumTimeSteps,SS(n)%ElemName,SS(n)%name,gres,gn,pwbn    ! Print warning
            RETURN                                                                   ! Exit the routine
!
         END IF
! 
! 
!***********************************************************************
!*                                                                     *
!*           RENORMALIZE FRACTIONAL FLOWS TO 1, AND COMPUTE            *
!*               CONTRIBUTIONS OF COMPONENTS IN PHASES                 *
!*                                                                     *
!***********************************************************************
! 
! 
         DO_NumPhase2: DO np=1,NumMobPhases
!      
! ......... Normalization of fractional flows     
!      
            IF(FFS /= 0.0d0) SS(n)%PhaseFracFlow(np) = SS(n)%PhaseFracFlow(np)/FFS
!      
! ......... Adjustment of the Jacobian submatrices:  CAREFUL! Whole array operation     
!      
            Q_contr(1:NumCom , m) = Q_contr(1:NumCom , m) - FAC*GN* SS(n)%PhaseFracFlow(np) * ElemProp(j,m-1)%MassFrac(1:NumCom,np)
!
         END DO DO_NumPhase2
! 
! ----------
! ...... Adjust the heat-related Jacobian submatrices
! ----------
! 
         Q_contr(NumComPlus1,m) = Q_contr(NumComPlus1,m) - FAC*GN*SS(n)%enth
!
! <<<                      
! <<< End of the NEQ1 LOOP         
! <<<
!
      END DO DO_Neq1
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'BottomHole_PXH_for_WellheadP',T50,'[v1.0,  26 April     2004]',6X,   &
     &         ':::::   Determine bottomhole pressure, phase composition and enthalpy for production with specified ',   &
     &         'wellhead pressure')
! 
 6002 FORMAT(' M=',I2,' NP=',I1,' K=',1pE13.6,' VIS=',1pE13.6,' D=',1pE13.6,' FF=',1pE13.6,' FFS=',1pE13.6)
 6004 FORMAT(' At <Tot_NumTimeSteps> = ',I5,' exceed WFLO table data at element ',A5,' (source ',A5,') --> will reduce <TimeStep>')
! 
 6006 FORMAT(' IG=',I2,'   Pwb=',E12.6,'   Gwell=',E12.6,'   Gres=',1pE13.6,'   dPwb/dG =',1pE13.6,'   RG =',1pE13.6)
 6008 format(' At <Tot_NumTimeSteps> = ',I4,', element ',A5,' (source ',A5,') no',   &
     &       ' convergence for Pwb = ',1pE13.6,'   Gres = ',1pE13.6,'   Gwel = ',1pE13.6)
!
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of BottomHole_PXH_for_WellheadP
!
!
      RETURN
! 
!
      END SUBROUTINE BottomHole_PXH_for_WellheadP
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Bottom_Hole_Pressure(n,G,H,pp,dpdg)
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units
!
         USE General_Control_Parameters
! 
         USE Sources_and_Sinks, ONLY: WDel
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE DETERMINING BOTTOMHOLE PRESSURE THROUGH BIVARIATE       *
!*         INTERPOLATION FROM FLOWING WELLBORE PRESSURE TABLE          *
!*                                                                     *
!*                   Version 1.00 - April 27, 2004                     *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(IN)  :: g,h
      REAL(KIND = 8), INTENT(OUT) :: pp,dpdg
! 
      REAL(KIND = 8) :: QQ,RR,DQDg,DpDQ
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: n
! 
      INTEGER :: nn,nG,nH,KL,KR,LL,LR
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  Main body of Bottom_Hole_Pressure
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
! 
! -------
! ... Assignment of parameter values
! -------
! 
      nn  = WDel(n)%idTab
      nG  = WDel(nn)%NumG
      nH  = WDel(nn)%NumH
! 
! 
!***********************************************************************
!*                                                                     *
!*   Determine from the table the interval for the production rate     *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_RangeG: IF( (G < WDel(nn)%p_bhp(1)) .OR. (G > WDel(nn)%p_bhp(nG)) ) THEN   ! When outside the table range, ...
!   
         WRITE(36,6002) G,WDel(nn)%p_bhp(1),WDel(nn)%p_bhp(nG)  ! ... print warning
         IF(NumTimeSteps == 0) CALL STOP_ALL                   ! ... abort on first timestep
         IGOOD = 3                                             ! ... set the completion flag 
         RETURN                                                ! ... Done ! Exit routine
      ELSE
         KL = COUNT(G - WDel(nn)%p_bhp(1:nG) <= 0.0d0)  ! Determine the # of the left interval boundary
         KR = MIN(KL+1,ng)                              ! Determine the # of the right interval boundary
      END IF IF_RangeG
! 
! 
!***********************************************************************
!*                                                                     *
!*       Determine from the table the interval for the enthalpy        *
!*                                                                     *
!***********************************************************************
! 
! 
      IF_RangeH: IF( (H < WDel(nn)%p_bhp(nG+1)) .OR.   &  
     &               (H > WDel(nn)%p_bhp(nG+nH)) )     & 
     &THEN                                                           ! When outside the table range, ...
         WRITE(36,6004) H,WDel(nn)%p_bhp(nG+1),WDel(nn)%p_bhp(nG+nH)  ! ... print warning
         IF(NumTimeSteps == 0) CALL STOP_ALL                         ! ... abort on first timestep
         IGOOD = 3                                                   ! ... set the completion flag
         RETURN                                                      ! ... Done ! Exit routine
      ELSE
         LL = COUNT(H - WDel(nn)%p_bhp(nG+1:nG+nH) <= 0.0d0)  ! Determine the # of the left interval boundary
         LR = MIN(LL+1,nH)                                    ! Determine the # of the right interval boundary
      END IF IF_RangeH
! 
! 
!***********************************************************************
!*                                                                     *
!*                    COMPUTE BOTTOMHOLE PRESSURE                      *
!*                                                                     *
!***********************************************************************
! 
! 
      QQ =  (G                  - WDel(nn)%p_bhp(KL))   & 
     &     /(WDel(nn)%p_bhp(KR) - WDel(nn)%p_bhp(KL))
!
      RR =  (H                  - WDel(nn)%p_bhp(LL))   & 
     &     /(WDel(nn)%p_bhp(LR) - WDel(nn)%p_bhp(LL))
!
      pp =  (1.0d0 - QQ)*(1.0d0 - RR)*WDel(nn)%p_bhp(nG+nH+(KL-1)*nH+LL)      &
     &     +             (1.0d0 - RR)*QQ*WDel(nn)%p_bhp(nG+nH+(KR-1)*nH+LL)   &
     &     +             (1.0d0 - QQ)*RR*WDel(nn)%p_bhp(nG+nH+(KL-1)*nH+LR)   &
     &     +                          QQ*RR*WDel(nn)%p_bhp(nG+nH+(KR-1)*nH+LR)
! 
! 
!***********************************************************************
!*                                                                     *
!*      COMPUTE DERIVATIVE OF PRESSURE WITH RESPECT TO FLOW RATE       *
!*                                                                     *
!***********************************************************************
! 
! 
      DQDg = 1.0d0/( WDel(nn)%p_bhp(KR) - WDel(nn)%p_bhp(KL) )       ! dQ/dG  
!
      DpDQ = (1.0d0-RR)*(  WDel(nn)%p_bhp(nG+nH+(KR-1)*nH+LL)     &  ! dP/dQ
     &                   - WDel(nn)%p_bhp(nG+nH+(KL-1)*nH+LL) )   &
     &      +        RR*(  WDel(nn)%p_bhp(nG+nH+(KR-1)*nH+LR)     &
     &                   - WDel(nn)%p_bhp(nG+nH+(KL-1)*nH+LR) )
!
      DpDg = DpDQ*DQDg       ! dP/dG = (dP/dQ)*(dQ/dG)
!
!
!
!kzhang
      IF(NumTimeSteps == 0) CALL STOP_ALL
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Bottom_Hole_Pressure',T50,'[v1.0,  17 April     2006]',6X,   &
     &         ':::::   Determine bottomhole pressure through bivariate interpolation from flowing wellbore pressure table')
! 
 6002 FORMAT('===> In subroutine "Bottom_Hole_Pressure", the rate G = ',1pE15.8,       &
     &       ' is outside the table range (', 1pE15.8,',',1pE15.8,')   <=== ')
! 
 6004 FORMAT('===> In subroutine "Bottom_Hole_Pressure", the enthalpy H = ',1pE15.8,   &
     &       ' is outside the table range (', 1pE15.8,',',1pE15.8,')   <=== ')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  End of Bottom_Hole_Pressure
!
!
      RETURN
! 
!
      END SUBROUTINE Bottom_Hole_Pressure
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE CPU_Elapsed_Time(IFLAG,DT,TOTAL)  
! 
! ...... Modules to be used 
! 
         USE General_External_File_Units
         USE MPICOM
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*                 ROUTINE COMPUTING ELAPSED CPU TIME                  *
!*                                                                     *
!*                  Version 1.00 - February 15, 2004                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8), INTENT(OUT) :: DT,TOTAL
! 
      REAL(KIND = 8) :: T1,T2
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER, INTENT(IN) :: IFLAG
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call,T1
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of CPU_Elapsed_Time
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         IF(myid==iMaster) WRITE(VERS_Unit,6000)
      END IF
! 
! -------:q
! ... Call the CPU timing routine
! -------
! 
      CALL CPU_Timing_Routine(T2)
! 
! -------
! ... Compute elapsed time
! -------
! 
      IF(IFLAG /= 0) THEN
         DT    = T2-T1
         TOTAL = T2
      ELSE         
         T1 = T2
      END IF        
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'CPU_Elapsed_Time',T50,'[v1.0,  15 February  2006]',6X,/,   &
     &         ':::::   Compute elapsed CPU time')
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of CPU_Elapsed_Time
!
!
      RETURN
      END SUBROUTINE CPU_Elapsed_Time
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Tabulate_TimeSeries_Data
! 
! ...... Modules to be used 
! 
         USE Basic_Parameters
         USE General_External_File_Units

         USE General_Control_Parameters
         USE Time_Series_Parameters
! 
         USE Grid_Geometry
         USE Element_Attributes
         USE Connection_Attributes
         USE Sources_and_Sinks
! 
         USE Solution_Matrix_Arrays
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*     ROUTINE TABULATING ELEMENT, CONNECTION AND GENERATION TIME      *
!*                SERIES DATA FOR SUBSEQUENT PLOTTING                  *
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      IMPLICIT NONE
! 
! -------
! ... Double precision arrays
! -------
! 
      REAL(KIND = 8), DIMENSION(100) :: xd
! 
! -------
! ... Double precision variables
! -------
! 
      REAL(KIND = 8) :: cgn,fft,SF_1,SF_2,SF_H
! 
! -------
! ... Integer arrays
! -------
! 
      INTEGER, DIMENSION(100) :: ind
! 
! -------
! ... Integer variables
! -------
! 
      INTEGER :: nogu = 0
! 
      INTEGER :: i,j,k,l,n
      INTEGER :: Np_obs_conx
! 
! -------
! ... Logical variables
! -------
! 
      LOGICAL :: First_call = .TRUE.
! 
! -------
! ... Saving variables
! -------
! 
      SAVE First_call,nogu
!
!  This subroutie has not been used by TOUGH+HYDRATE, not parallelized.
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   Main body of Tabulate_TimeSeries_Data
!
!
      IF(First_call) THEN
         First_call = .FALSE.
         WRITE(UNIT = VERS_Unit, FMT = 6000)
      END IF
! 
! 
! 
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6000 FORMAT(/,'Tabulate_TimeSeries_Data',T50,'[v1.0,  12 May       2006]',/,    &
     &         ':::::   Tabulate element, connection, and generation data vs. time for plotting (generic)')
! 
 6002 FORMAT(I5,' , ',1pE12.5,100(' , ',I5,5(' , ',1pE12.5)))
 6004 FORMAT(I5,' , ',1pE12.5,100(' , ',I5,3(' , ',1pE12.5)))
 6006 FORMAT(I5,' , ',1pE12.5,100(' , ',I5,5(' , ',1pE12.5)))
! 
 6010 FORMAT(1pE12.5,2x,7(1pE14.7,1x))
!
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>   End of Tabulate_TimeSeries_Data
!
!
      RETURN
! 
!
      END SUBROUTINE Tabulate_TimeSeries_Data
!
!
!
!
!
!***********************************************************************
!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
!***********************************************************************
!
!
!
      SUBROUTINE Warning(ELEM,XX,NK1,KC)
!
!***********************************************************************
!***********************************************************************
!*                                                                     *
!*       ROUTINE FOR PRINTING WARNINGS AND RELATED INFORMATION         *
!*   WHEN THE RANGE IS EXCEEDED AND/OR INITILIALIZATION IS INCORRECT   *
!*                                                                     *
!*                   Version 1.00, September 4, 2003                   *     
!*                                                                     *
!***********************************************************************
!***********************************************************************
!
      USE MPICOM
      IMPLICIT NONE
! 
! ----------
! ... Integer variables
! ----------
! 
      INTEGER, INTENT(IN) :: NK1,KC
! 
      INTEGER :: m
! 
! ----------
! ... Double precision arrays
! ----------
! 
      REAL(KIND=8), DIMENSION(NK1), INTENT(IN) :: XX
! 
! ----------
! ... Character variables
! ----------
! 
      CHARACTER(LEN=5), INTENT(IN) :: ELEM
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  BEGIN Warning
!
!
      WRITE(36,6100) ELEM,(XX(m), m=1,NK1)                  
!
      IF(KC == 0) THEN
         WRITE(36,6101) 
         CALL STOP_ALL                                             
      END IF
!
!
!***********************************************************************
!*                                                                     *
!*                        F  O  R  M  A  T  S                          *
!*                                                                     *
!***********************************************************************
!
!
 6100 FORMAT(' !!!!!!!!! Cannot find parameters at element "',A8,'" with primary variables  XX(M) =',10(1X,1pE12.5))                               
!
 6101 FORMAT(' !!!!!!!!!!!  Erroneous Data Initialization  !!!!!!!!!!',11X,'STOP EXECUTION  <=========')                                 
!
!
!  =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>=>  END Warning
!
!
      RETURN
!
      END SUBROUTINE Warning
!
!
!
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
